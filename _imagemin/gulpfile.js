var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var pngToJpeg = require('png-to-jpeg');
var ext_replace = require('gulp-ext-replace');

gulp.task('convert-pngs', function() {
    return gulp
        .src(['src/*.png', 'src/*.PNG'])
        .pipe(
            imagemin({
                use: [
                    pngToJpeg({
                        quality: 90
                    })
                ]
            })
        )
        .pipe(ext_replace('jpg'))
        .pipe(gulp.dest('dest'));
});

gulp.task('imagemin-jpgs', function() {
    return gulp
        .src(['src/*.jpg', 'src/*.JPG'])
        .pipe(imagemin())
        .pipe(gulp.dest('dest'));
});

gulp.task('default', ['convert-pngs', 'imagemin-jpgs']);
