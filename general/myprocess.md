Trello board
    Explore: ideas I think are talk worthy.
        Can acquire the "CFP-worthy" tag if I like them a lot and I write an abstract about them.
    Open CFPS: CFPs I want to submit to. This is time-sensitive!
    Doing: Ideas I am building out or need to build out because it got accepted!
    Ready: Talks that I've written already.
    Waiting: CFPs I've submitted to and am waiting to hear back from
    Conferences: Conferences that I would want to submit to. Most are not open CFPs but have CFP dates from the previous year on them, so that I know when to start looking for an open CFP.

Idea
The idea for a talk bubbles around in my head for months ahead of time.

"Explore": notice things that I read about in blogs/on Twitter that I am interested in learning more about
    Add them to trello

"Doing": think about here and there.
    make notes to myself in a pocket notebook
        happens at random times. I see something in real life and it feels like a story I could tell in a talk.

    Start to build out an outline
        happens both breadth first & depth first
            breadth first: I start to lay out a high level flow. Things move around a lot.
            depth first: I start to build out details of a portion of the high level flow.
            I don't do one before the other. I work on whichever method I'm feeling at the time.

    Start to build slides

    Add images & fine tune slides

