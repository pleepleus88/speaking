---
layout: false
template: cover-art

# THANK YOU!

.about.right[
## Steven Hicks
### [@pepopowitz](http://twitter.com/pepopowitz)  <i class="el el-twitter"></i>
### steven.j.hicks@gmail.com  <i class="el el-envelope"></i>
### [bit.ly/!!](http://bit.ly/!!)  <i class="el el-globe"></i>
]

???

I want to thank you for your time

I really appreciate it.

Survey

Questions after

Thank you!

---
template: level-1
class: double-wide, resources

# Resources

* [Jest](https://facebook.github.io/jest/)

---
template: level-1
class: double-wide, resources

# Images

* [url] - Title
* The rest - Me
