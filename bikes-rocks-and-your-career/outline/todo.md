neurolinguistic programming - 
    ask for what you want, not what you don't want
    it shifts your view of the situation, & anchors you and others
    this is "find your line"
