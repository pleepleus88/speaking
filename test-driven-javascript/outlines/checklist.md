* mirror shortcut: cmd-f1

* Start up slides
  * sync one presenter (p) with one full-screen

* Open command line to C:\_sjh\dev\workshops\tdd-workshop.git
  * Zoom in (ctrl+mouse)
  * `npm run test-module-2`

* VS Code to workshop.git
* Left panel
  * Preview of module 2
  * module-2/index.spec.js
* Right panel
  * module-2/index.js

