# Yes

## Add a star student slide for my intro

## Add an image for "each cell has 8 neighbors"

## Verify syntax for mocking is up to date

---

Updated timings after music city code -

0:00 - 0:15 - Intro & Setup
0:15 - 0:30 - Jest (instruction)
0:30 - 0:55 - Jest (hands-on exercise)
0:55 - 1:15 - TDD (instruction)
break? (10 min)
1:15 - 1:30 - TDD (collaborative exercise)
break? (10 min)

1:40 - 1:45 - Code Katas (instruction)
1:45 - 2:15 - Paired Code Kata (hands-on exercise)
2:15 - 2:25 - Real-life TDD (instruction)
break? (10 min)
2:25 - 2:55 - Real-life TDD (hands-on exercise)
break? (10 min)

3:05 - 3:10 - Advanced Code Kata (instruction)
3:10 - 3:50 - Paired Advanced Code Kata (hands-on exercise)
3:50 - 4:00 - Good Practices & Summary (instruction)
