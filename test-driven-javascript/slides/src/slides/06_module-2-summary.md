---
template: module-title

# Recap: Module 2
## Intro To Test-Driven Development

???
summary: 1:25 - 1:30

**9:25 - 9:30**

---
layout: true
template: module-2-section

## Recap

---

### How did it go?
* What made sense?
* What didn't?
* What stuck with you?

---

### Suggestions

---
layout: true
template: level-3
name: module-2-suggestions

# 2: TDD
## Recap
### Suggestions

---

#### Stay Green

???

Open-water swimming & sighting

---

#### Handle validation first

???

easy wins

---

#### Design your interfaces

???

Make it simple

Make it intuitive

Make it readable

---

#### Triangulate

???

Add a second similar test

Make both tests pass at the same time

---

#### Tests aren't permanent

???

Use them to navigate to the solution

Delete them when they no longer serve you

You can write tests that aren't production worthy.

---

#### Refactor

???

Sometimes it's tempting to move on as soon as you make it pass

It's the easiest step to forget

Also - refactoring means refactoring TEST CODE, too - not just tested code.
