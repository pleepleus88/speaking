---
template: module-title

# Module 1
## Jest

???

instruction: 0:15 - 0:30

**8:15 - 8:30**

---

layout: true
template: module
name: module-1

# 1: Jest

---

## Delightful JavaScript Testing

### [facebook.github.io/jest](http://facebook.github.io/jest)

???

self-proclaimed

Jasmine/mocha/chai?

---

## Why?

---

layout: true
template: module-section
name: module-1-section

# 1: Jest

---

layout: true
template: module-1-section
name: why

## Why?

---

### Easy to set up

???

Generally only a couple of steps

All are really well documented

---

### Zero configuration

???

If you **follow the conventions** for naming your test files,

Jest will just find them.

---

### Fast

???

Especially given a large test suite.

Slower by comparison for a small test suite.

(Parallelization)

Others scale linearly; Jest scales logarithmically.

---

### Helpful error messages

???

This is one of my favorite things happening in dev right now

Error messages that tell you **what you did wrong**

instead of just **you did something wrong**

---

layout: true

---

class: bg-contain
background-image: url(images/jest-error-1.jpg)

---

class: bg-contain
background-image: url(images/jest-error-2.jpg)

---

class: bg-contain
background-image: url(images/jest-error-3.jpg)

---

layout: true
template: why

---

### Interactive watch mode

???

By default,

runs **only tests affected by changes** since your most recent commit.

(Great for TDD)

Other options:

1. run all tests

2. filter tests by name or file path

---

layout: false
template: module-1

## Writing tests

---

layout: true
template: module-1-section

## Writing tests

---

### Where to put them?

???

I mentioned that Jest has conventions for naming/locating your tests

These are your options

--

- \_\_tests\_\_ folder

--

- \*.spec.js

--

- \*.test.js

???

In all cases, the best place to put those test files

is right along side of the code being tested.

---

layout: false
template: module-1

## Jest API

---

layout: true
template: module-1-section

## Jest API

---

### describe()

???

describe is how you define a **set of tests**

--

.dim-2[

```javascript
describe("first-name-translator", () => {
  // tests go in here
});
```

]

???

## It's kind of like **namespacing** your tests

### describe()

.dim-3[

```javascript
describe("first-name-translator", () => {
  describe("invalid requests", () => {
    // ....invalid request tests....
  });
});
```

]

???

can be nested

---

### it() or test()

???

use it or test to define an individual test.

--

.dim-1.dim-3.dim-5[

```javascript
describe("first-name-translator", () => {
  it("translates my name", () => {
    // test goes here
  });
});
```

]

???

give it a name

and a function to execute.

---

### expect()

???

assertions

--

.dim-1.dim-2.dim-3.dim-6.dim-7[

```javascript
describe("first-name-translator", () => {
  it("translates my name", () => {
    const result = translateFirstName("Steve");

    expect(result).toEqual("Sassy");
  });
});
```

]

---

### expect()

```javascript
expect(a).toEqual(b);

expect(a).not.toEqual(b);

expect(() => a()).toThrowError(b);
```

???

they aren't much different than other JS test frameworks

and if you're new to JS testing, they read very fluently.

---

### Putting it all together

```javascript
describe("first-name-translator", () => {
  it("translates my name", () => {
    const result = translateFirstName("Steve");

    expect(result).toEqual("Sassy");
  });
});
```

---

layout: false
template: module-1

## Running tests

---

layout: true
template: module-1-section

## Running tests

--

.centered-image[
![Interactive watch mode](images/interactive-watch-mode.png)
]

---

template: module-title

# Exercise: Module 1

## Jest

### `module-1/README.md`

???

get comfortable with Jest

JS Unicorn Name Generator

! show them how to preview markdown in vscode!

exercise: 0:30 - 0:50

**8:30 - 8:50**
