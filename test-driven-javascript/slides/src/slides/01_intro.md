---
template: cover-art

# Building Quality JavaScript With Test-Driven Development

.about.left[
## Steven Hicks
### <i class="el el-twitter"></i> [@pepopowitz](https://twitter.com/pepopowitz)
### <i class="el el-envelope"></i> steven.j.hicks@gmail.com
### <i class="el el-globe"></i> [stevenhicks.me/tdd](https://stevenhicks.me/tdd)
]

.while-waiting[
## While we're waiting: 
### [stevenhicks.me/tdd-setup](https://stevenhicks.me/tdd-setup)

### WIFI pw: ????
]

???

**Contact Info**
**Thanks:**

* conference
* organizers
* ~~sponsors~~
* you!

timekillers:

* stickers!
* where from?
* favorite talks
* favorite speakers
* stand up & stretch
* high fives

---

layout: false
template: about-title

# About Me

???

intro & setup: 0:00 - 0:15 (or 0:20 if not a lot people did setup)
(1:00 - 1:15 or 1:20)

---

layout: false
template: full-screen
background-image: url(images/star-student.jpg)
class: no-footer

---

layout: false
background-color: black
background-image: url(images/artsy.svg)
class: inverse, bg-center, bg-40

---

layout: false
template: about-title

# About This Workshop

---

layout: true
template: level-1
name: about

# About This Workshop

---

class: collaborative

## Collaborative

--

- You + me

--

- You + your neighbor

???

Teaching is one of the best ways to learn

& We're going to pair!

---

## Framework-agnostic

---

class: schedule

## Agenda

- .highlight[Module 0:] Intro & Setup
- .highlight[Module 1:] Jest
- .highlight[Module 2:] Intro to TDD
- .highlight[Module 3:] Code Katas
- .highlight[Module 4:] Real-life TDD
- .highlight[Module 5:] Solving Problems Together
- .highlight[Wrapping Up:] Good Practices

???

Now is your chance to break!

1:20 if people didn't do the setup

---

template: module-title

# Feedback

## Green = Good; Yellow = Okay; Pink = Bad

???

explain the system

red, yellow, green cards

put in bag

leave comments if you'd like

---

layout: false
template: about-title

# About You

???

JS devs?

unit testing experience?

solid tdd experience?

have tried tdd but it hasn't stuck

---

template: level-1

# About You

## Meet your neighbors!

--

- Name
- Where you're from
- What you do
- What you're excited about
