---
template: module-title

# Recap: Module 4
## Real-life TDD

???

3:00 - 3:05

**11:00 - 11:05**

Should have taken break 2 by now!

---

layout: true
template: module-4-section

## Recap

---

### How did it go?

- What went well?
- What didn't?
- What stuck with you?

---

### Suggestions

---

layout: true
template: level-3
name: module-4-suggestions

# 4: Real-life TDD

## Recap

### Suggestions

---

#### Introduce TDD in small doses

???

Bug fixes

A function that calculates something

A simple, small, defined set of business rules

---

#### Procrastinate Complexity

???

It's okay to not know how your code is going to look

keep chipping away the easy things, and testing those

---

#### Be Pragmatic

???

Some things are just too impure/nondeterministic for TDD

Identify the impure, unpredictable, & things out of your control

i.e. current date/time, api's

Extract & mock them

Test your thing in isolation
