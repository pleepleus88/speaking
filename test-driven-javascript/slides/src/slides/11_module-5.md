---
template: module-title

# Module 5
## Solving Problems Together

???

instruction: 3:05 - 3:10

**11:05 - 11:10**

---

layout: true
template: module
name: module-5

# 5: Solving Problems Together

---

layout: true
template: module-section
name: module-5-section

# 5: Solving Problems Together

---

template: module-5
layout: true

---

## Conway's Game Of Life

---

template: full-screen
background-image: url(images/game-of-life.gif)
animated-background: images/game-of-life.gif
class: no-footer
name: game-of-life

???

Cells die, stay alive, or revive based on the states of their neighbors

4 rules

They seem simple...but they are challenging.

You're going to build the logic that makes the cells follow the 4 simple rules.

---

## Pair Up!

???

Choose a laptop

---

## Workflow

1. Person 1: Write a failing test
2. Person 2: Make the test pass
3. Person 1&2: Refactor together
4. Person 2: Write a failing test
5. Person 1: Make the test pass
6. Person 1&2: Refactor together
7. Repeat

---

## Rules

- No code without tests
- Collaborate to define the test cases
- If you're typing, you write the code
- Keep all methods below 5 lines
- Do one task at a time

???

3 - You may ask for the other’s opinion

3 - But the other may not force you to write the code a certain way

5 - Try not to read ahead

---

## Things To Note

---

layout: true
template: module-5-section

## Notes

---

### This problem is hard

???

I don't expect you to finish

---

### Each cell has 8 neighbors

.centered-image[
![Each cell has 8 neighbors](images/all-8-neighbors.png)
]

???

Not 4

Diagonals are included!

---

### Start with specs that won't ship

???

Remember that tests don't have to be permanent

Use them to guide you on the right path,

and delete them when they no longer serve you

--

* The next value of a cell depends on its current value 

--

* The next value of a cell depends on the cell directly to the left


---

### The Blinker

.centered-image[
![Blinker pattern](images/blinker.gif)
]

???

Once you support "the blinker" you're probably done

oscillator

---

### Refactor to make space

???

Sometimes you can see a change coming up

And you can identify that your system can make room for it

In these situations, it's helpful to refactor BEFORE writing your failing test

---

### You're amazing and I believe in you 💪🏼
---

template: module-title

# Exercise: Module 5

## Solving Problems Together

### `module-5/README.md`

???

3:10 - 3:45

**11:10 - 11:45**
