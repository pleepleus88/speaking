---
template: module-title

# Module 2
## Intro To Test-Driven Development

???
instruction: 0:55 - 1:15

**8:55 - 9:15**

---
layout: true
template: module

# 2: Test-Driven Development (TDD)

---

> A discipline in which any system change requires a failing test to be written first

???

also called Test-Driven Design

emphasis on design

it helps you design the system


---
layout: true
template: module
name: module-2

# 2: TDD

---
## TDD tightens your feedback loop.

???

- **definition**: self-correction in a system to maintain a desired state
- **simple example**: thermostat
  - means to measure (thermometer)
  - means to adjust (furnace)

- **loose**: make changes, wait til prod release to find out if it worked
- **tight**: make changes, see within seconds if you broke something

Q: what else tightens development feedback loop?

A: CI/CD

---
class: all-changes-require

## All Changes Require:

--

1. Write a failing unit test

--

2. Write just enough code to pass the test

--

3. Refactor the code just enough

???

to meet the desired level of quality

Hardest part: writing a failing test first.

---

## Red/Green/Refactor

???

aka r/g/r

Red & Green are pretty obvious - failing test, passing test

But refactoring needs some explanation.

---
layout: true
template: module-section
name: module-2-section

# 2: TDD

---
layout: true
template: module-2-section
name: module-2-refactoring

## Refactoring

---

> Refactoring is the process of restructuring code without affecting the external behavior
    
---

### Goals:
* Improve readability and maintainability
* Remove “code smells”
* Remove duplication

???

---

### Refactoring works best when you have unit tests covering the code you are refactoring

???

How else will you know if you alter the external behavior?

Imagine you have a complex system with good unit test coverage

You make changes and break something, you get errors!

Without unit tests, you'd have to manually test, and you'd assuredly miss something.

---
template: module-2

## Why TDD?

---
layout: true
template: module-2-section

## Benefits
---

### TDD may lead to...

* Simpler designs
* Smaller units of code
* Looser coupling
* Cleaner interfaces
* Improved code coverage

???

emphasis on _may_

---

### TDD may also...

* Question your understanding of the requirements
* Document the requirements
* Guide code reviews
* Give you code confidence
* Reduce mental clutter

???

Also some effects beyond the code

---
template: module-2

## How Do I TDD?

---
layout: true
template: module-2-section

## The Process

---
### .highlight[Step 0:] Accept that TDD is hard

???

It will probably not feel natural at first

For me, it took two years of failed attempts before it felt natural

---

### .highlight[Step 1:] Identify the test

???

You can identify tests one at a time, as you write them

I usually prefer to brainstorm all my tests for a class before I start

I write them in my test file as comments

This is a great time to decide what you do and don’t know about the requirements

And discuss this with your team!

Also a great time to define the API to the thing you're building

---

### .highlight[Step 2:] Write the failing test

???

But you're not just writing the test!!!!

You're also writing the interface to what you're testing!!!!

**THIS** is what makes it test-driven DESIGN

This is the time to decide what the interface to your function will look like

---

### .highlight[Step 3:] Write as little code as possible to make the test pass

???

The easiest way to do this is to “fake it”

i.e. hardcode the expected result

It doesn’t matter that this code is clearly wrong. 

The important thing is that you now have a passing test, and you can refactor the code under the safety net of that test, until the code is right.

---

### .highlight[Step 4:] Refactor the code until you're content

???

That means making small changes, while keeping your tests passing

---
template: module-2

## Tips

---
layout: true
template: module-2-section

## Tips

---

### Stay “green” while refactoring

???

If you leave the “green” state for a long time, it can be tough to get back

---

### Work in small iterations

???

You are looking for a tight feedback loop

---

### Unexpected passing tests are 
### bad luck

???

* Find out why they're passing.
* Make them fail to prove they're working.

---
template: module-title

# Exercise: Module 2
## Intro To Test-Driven Development

### `module-2/README.md`

???
exercise: 1:15 - 1:25

**9:15 - 9:25**

FizzBuzz Demo - instructor-led

(next slide for preview)

---
template: module-2-section

## FizzBuzz

```
> FizzBuzz(15)

1 2 Fizz 4 Buzz Fizz 7 8 Fizz Buzz 11 Fizz 13 14 FizzBuzz
```

???

