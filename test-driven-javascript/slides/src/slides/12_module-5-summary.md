---
template: module-title

# Recap: Module 5
## Solving Problems Together

???

3:45 - 3:50

**11:45 - 11:50**

---

layout: true
template: module-5-section

## Recap

---

### How did it go?

- What went well?
- What didn't?
- What stuck with you?

---

### Suggestions

---

layout: true
template: level-3
name: module-5-suggestions

# 5: Solving Problems...

## Recap

### Suggestions

---

#### Test the interface, not the implementation

???

Only test things that this thing's "client" would care about.

i.e. not "it puts this value in state"

but instead "when I do this thing to it, this other thing shows up."

---

#### Tests are not forever

???

They don't have to be permanent

You can write tests that you know aren't going to ship

And then delete them as soon as they become wrong

---

#### Refactor to make space

.centered-image[
![Kent Beck Tweet](images/kent-beck.png)
]

???
