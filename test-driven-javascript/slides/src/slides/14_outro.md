---
template: module-title
# Feedback
## Green = Good; Yellow = Okay; Pink = Bad
---

layout: false
template: cover-art

# THANK YOU!

.about.left[

## Steven Hicks

### <i class="el el-twitter"></i> [@pepopowitz](https://twitter.com/pepopowitz)

### <i class="el el-envelope"></i> steven.j.hicks@gmail.com

### <i class="el el-globe"></i> [stevenhicks.me/tdd](https://stevenhicks.me/tdd)

]

???

I want to thank you for your time

I really appreciate it.

Survey

Questions after

Thank you!

---

template: level-1
class: double-wide, resources

# Resources

- [Workshop materials](https://stevenhicks.me/tdd)
- [Jest](https://facebook.github.io/jest/)
- [Code katas](https://codekatas.com)

---

template: level-1
class: double-wide, resources

# Images

- [Ryan Lange](https://unsplash.com/photos/mAcDHok1t8k) - Title
