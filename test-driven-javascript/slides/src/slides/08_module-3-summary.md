---
template: module-title

# Recap: Module 3
## Code Katas

???

2:10-2:15 

**10:10 - 10:15**

---

layout: true
template: module-3-section

## Recap

---

### How did it go?

- What went well?
- What didn't?
- What stuck with you?

---

### Suggestions

---

layout: true
template: level-3
name: module-3-suggestions

# 3: Code Katas

## Recap

### Suggestions

---

#### Write small methods

???

Break things into small pieces

It's easier to understand & manage your code

---

#### Don't look too far ahead

???

Leads to overarchitecting

Building things you don't need

---

#### Use katas to build habits

???

You can practice them on your own

some people like to start their day with a kata

if you've read about habits:

- power of habit - charles duhigg
- atomic habits - james clear
- any modern non-fiction on habits

you're familiar with the habit loop

cue/routine/reward

**cue**: logic-intensive problem

**routine**: tdd

**reward**: code I'm proud of, well-tested, small methods, ...

---

#### [codekata.com](http://codekata.com/)

???

more katas
