---
template: module-title

# Wrapping Up
## Good Practices

???

3:50 - 4:00 

**11:50 - 12:00**

Let's wrap up with a few good practices for keeping your code friendly to TDD


---

layout: true
template: module
name: good-practices

# Good Practices

---

## Treat test code like tested code

???

Focus on maintainability & readability

Name appropriately

with intention & clarity

---

## Tests are not forever

### Delete them when they no longer serve you

???

They aren't eternal

Use them to drive you to a design/solution

Delete them or modify them when they aren't serving you anymore

---

## Make things small

???

tests!

test files!

tested code!

---

## Optimize tests for time of failure, not time of writing

???

Assert in ways that the error will be helpful

Use clear test describes & names

Keep things organized!

Keep things small!

You have context when writing. You don't when the test fails.
