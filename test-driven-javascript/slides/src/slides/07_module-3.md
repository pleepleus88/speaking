---
template: module-title

# Module 3
## Code Katas

???
Should have taken 1st break by now!

instruction: 1:40 - 1:45

**9:40 - 9:45**

---

layout: true
template: module
name: module-3

# 3: Code Katas

---

layout: true
template: module-section
name: module-3-section

# 3: Code Katas

---

template: module-3
layout: true

---

> A code kata is an exercise in programming which helps programmers hone their skills through practice and repetition.

---

## Usually not a real-world problem

???

A canned problem, a specific scenario with specific rules

---

## Katas are about practice

???

Not the problem itself.

---

## Build a testing habit

???

The more you do it, the more it becomes your default response to a problem

Under pressure, habits remain

I'm pretty good about eating a salad with my lunch when things are going smoothly

But when things get busy & hectic, I fall back to my habits - fast food, soda, no salad.

---

template: module-title

# Exercise: Module 3

## Code Katas

### `module-3/README.md`

???

String calculator kata

1:45-2:10

**9:45 - 10:10**

Rules:

- No code without tests

- Don't expect to finish! There's a lot here.
