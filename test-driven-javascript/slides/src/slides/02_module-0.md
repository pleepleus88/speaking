---
template: module-title

# Module 0
## Setup

---
template: module
layout: true

# 0: Setup

---

## [https://stevenhicks.me/tdd-setup](https://stevenhicks.me/tdd-setup)

???

intro & setup: 0:00 - 0:15 (or 0:20 if not a lot people did setup)

**8:00 - 8:15/8:20**

If you cloned before this morning - pull latest!


If you've not gotten things set up yet,

I'm going to ask that you do this on your own time.

Go to this url, and follow the instructions to get set up.

*If lots didn't do it:*

We'll take 5 minutes to do this now. 

I'll walk around and help where I can, but please don't hesitate to ask your neighbors if you're having problems.

TIMEBOXED TO 5 MINUTES.

