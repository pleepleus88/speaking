---
template: module-title

# Module 4
## Real-life TDD

???

instruction: 2:15 - 2:25

**10:15 - 10:25**

---
layout: true
template: module
name: module-4

# 4: Real-life TDD

---

layout: true
template: module-section
name: module-4-section

# 4: Real-life TDD

---

template: module-4

## Tests Aren't Always Easy

???

It's nice to write these simple tests that we've been writing

but sometimes life & code aren't so pleasant

---

template: module-4

## Mocking

---

template: module-4-section

## Mocking

### Replacing **actual** dependencies with **fake** ones

???

---

layout: true
template: module-4-section
name: module-4-mocking

## Mocking

---

### When?

???

When would we do this?

---

layout: true
template: level-3
name: module-4-mocking-when

# 4: Real-life TDD

## Mocking

### When?

---

#### The actual dependency has side-effects

???

API endpoint that deletes a user

---

#### The actual dependency is not deterministic.

???

A deterministic function returns the same results given the same input, every single time.

Non-deterministic:

(e.g. calls against a **database**, looking for specific records).

Intrinsically non-deterministic:

(e.g. a function that gets the **current time**).

It is much easier to write tests against these dependencies when they are mocked - we can easily control the values they are returning, and verify our system is handling all the possible values properly.

---

#### The actual dependency is expensive to test.

???

DOM manipulation (our app)

Isolate the things that are difficult to test,

TDD all of the things that are easier to test, separately.

---

#### You aren't sure what the dependency does yet.

???

When you have a large problem that you need to break into smaller problems.

Mock the "difficult" stuff,

TDD the orchestration of those dependencies,

Then apply TDD to the "difficult" functions in isolation.

I call this strategy **"procrastination."**

---

layout: false
template: module-4-mocking

### Jest mocks

???

Several pieces involved in mocking with Jest

---

layout: true
template: level-3
name: module-4-mocking-jest

# 4: Real-life TDD

## Mocking

### Jest

---

#### jest.fn()

???

jest.fn helper returns a new mock function

that we can substitute for a real function

--

```javascript
const e = {
  preventDefault = jest.fn();
}

// act...

expect(e.preventDefault).toHaveBeenCalledTimes(1)
```

---

#### jest.mock()

???

another helper - jest.mock -

replaces an imported dependency with a fully/automatically mocked one

--

```javascript
jest.mock("axios");
```

---

#### beforeEach() & afterEach()

???

Global functions we can use to execute code before or after each test runs

--

```javascript
jest.mock("axios");

beforeEach(() => {
  axios.get.mockReset();
});
```

---

#### Faking Implementation

--

```javascript
jest.mock("axios");

axios.get.mockResolvedValue({
  data: [
    {
      name: "first item",
      hills: "Easy"
    }
  ]
});
```

---

#### Assertions

--

```javascript
jest.mock("axios");

it("calls axios", () => {
  // act...

  expect(axios.get).toHaveBeenCalledTimes(1);
  expect(axios.get).toHaveBeenLastCalledWith("/api/trails?hills=Easy");
});
```

---

1. Import the dependency
2. Mock the dependency
3. Reset the dependency before each test
4. Provide a fake implementation
5. Assert against the fake dependency

???

Within a test file...

1 - as you normally would for calling the dependency

2 - using jest.mock()

3 - in your beforeEach

4 - if you need to

5 - optional

---

class: medium

```javascript
import axios from "axios";

jest.mock("axios");

describe("search-form/call-api", () => {
  beforeEach(() => {
    axios.get.mockReset();
  });

  it("calls the api", async () => {
    axios.get.mockResolvedValue(aFakeResponse());

    const form = aFakeForm();

    await callApi(form);

    expect(axios.get).toHaveBeenCalledTimes(1);
    expect(axios.get).toHaveBeenLastCalledWith("/api/trails?hills=Easy");
  });
});
```

---

layout: false
template: module-4

## Brownfield Development

???

TDD seems easy to introduce to a new codebase

But there are also some great places to introduce TDD in an existing codebase

---

layout: true
template: module-4-section
name: module-4-brownfield

## Brownfield

---

### Business Logic

???

One great place to introduce TDD into your app is business logic.

Business logic is generally not related to UI,

so it's easier to test.

Also easily isolated to small functions,

which makes it easier to write, read, and maintain tests.

---

template: level-3

# 4: Real-life TDD

## Brownfield

### Business Logic


```javascript
get: function(req, response) {
  let results = trailData;

  if (req.query){
    results = maybeFilterBySport(results, req.query.sport);
    results = maybeFilterByHills(results, req.query.hills);
  }

  response.json(results);
},
```

???

You can imagine a function like this

Which does some filtering of data, based on a request

Would be a pretty great place to test-drive your code

---

layout: false
template: module-4-brownfield

### Bug Fixes

???

Another great place to introduce TDD into a project is by fixing bugs.

When you get a bug, you write a test proving that the bug exists - and therefore a failing test

And then make the test pass, to fix the bug

---

template: level-3

# 4: Real-life TDD

## Brownfield

### Bug Fixes

```javascript
it("translates Zeke's name", () => {
  const result = translateFirstName("Zeke");

  expect(result).toEqual("Dashing");
});
```

???

In the exercise you're going to work through, you're going to get a handful of bug reports

Like this one

These are tailor-made for TDD

---

template: module-title

# Exercise: Module 4

## Real-life TDD

### `module-4ab/README.md` & `module-4c/README.md`

???

2:25 - 2:50

**10:25 - 10:50**

10 minute break before, during, or after
