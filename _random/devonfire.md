?s for Dave - 
    Which mic?
        jabra filters more noise, but sounds more tingy
        logitech has fuller sound but picks up background noise

Bio - 

    Steven Hicks is a full-stack web developer with nearly 20 years experience. He believes in clean, readable, and maintainable code. Steve likes to use the right tool for the job, especially if the right tool is JavaScript. He strongly believes that if you ain't falling, you ain't learning. 

    Steve embraces continuous improvement and believes that a developer's job is to solve problems, not just write code.

    When he isn't talking to the duck or playing with his kids, you can find Steve at a triathlon, on his mountain bike, or in a climbing gym. 

What lights you up about technology and what you are currently doing?
    not tech itself - 
    things that go along with the tech

    helping people/making people's lives easier
        users
            sw that helps them
            simplifies their day-to-day tasks
        devs
            code they can read 3, 6, 12 months from now
                and know my intentions        
    collaboration/teamwork
        high fives
    learning new things
    solving problems
    quality

How did you get started writing software?
    tale of resignation, not excitement/inspiration

    as a kid, computers weren't prevalent (41 yo)
    we had one at home
        games (lode runner, death sword)
        print shop (banners, clip art, font, dot matrix)
    hs 
        programming class - meh
    college
        bio major - too much memorization
        math classes (missed it from hs)
        required a programming class
            wasn't wowed by it
            it came easy, i thought i could do it
            i knew i could make money doing it
        graduated with math & cis
    first job as a developer
        coasted
        didn't think i'd do it forever
        professor - didn't pull the trigger
        art school - technical illustrator
            part time - would take forever
            full time - choose between dev salary & starting over
            i realized i actually really liked development
    quit art school
    got a new job (more inspiring, i was stagnating)
    growing as a dev & person ever since
    getting more active in the dev community - local, state, regional
        speaking at meetups, code camps, conferences

Please share a story about time you failed.
    my most recent
    started a new job 2 months ago 
    part of the reason - my last project at my previous job
    variety of reasons, I just was not interested in it.
    reluctantly consulting at a company 1.5 hours away
        only had to commute 1x/week
        just wasn't interested
    I knew when I started that I wasn't going to be there for very long
    started looking for a job immediately
    
    **Didn't go all in.**

    Not a failure in terms of money
    But I don't think that I accomplished what I was asked
    Because I didn't try.

    Lesson: if your heart isn't in your work, you're going to fail.
    1. Find a way to get your heart in it
        Wish I'd done this
    2. Find work that you can put your heart in
        I did this
    Neither is right or wrong
        I just wish I'd done more of the former.
    
Please share the story of your greatest success.  Make sure you share how 
this experience delivered value.

    Becoming a conference speaker
    Didn't know I wanted to do it - 
        That Conference, Cory House
        command of the room, inspiring, funny, informative
        I can do that!
    Pushed by Chris Hayes
        Wanted to get our name well known in the community
    First couple meetups - terrifying
        but I was hooked.
        Adrenyline rush.
    Last year - 6 talks at 5 conferences in midwest
        Submitting to more national/int'l ones
    Proud of how I fought through rejection
        self-rejection 
            will anyone come? 
            will they listen to me? 
            what if I say something dumb?
        rejection from conferences
            they don't know who you are
            you aren't good at writing abstracts
    Lots of help getting through it
        support from wife
        coworkers who were doing the same thing
            hive mind?
        incredibly supportive coworkers giving amazing feedback
        yoga instructor - really good at guided meditation - helped build confidence
        getting into climbing - failure training
    But it's the most I've ever pushed through rejection & fear
    & delivers value to me personally, 
        and I feel like I've found a way to give back.

How do you stay current with what you need to know?
    Used to be feedly (google reader, RIP)
    That stopped working when I became a creator
    Now it's Twitter
        the builders of the internet
    Newsletters
        even those I fall behind on
    Podcasts
        Dev on fire
        Tech Done Right
    Going to conferences
    Scrimba

Book Recommendation
    I really want to like business books, self-improvement, pop psychology
    I struggle to finish them
    3 blog posts
        vs 35 different versions of the same story
    Clif notes would be awesome
    Confidence gap - Russ harris
        I struggle with self-confidence
        3/4 of the way through before stopping
        Helped me realize specifically what causes me to lack confidence, especially in social situations
            lack of skill in starting a conversation & keeping it going
    JD Maier - Getting results the agile way
        Applying agile to your life
        1/2 of the way through
        Focus less on big goals
            more on smaller actionable tasks that lead there
    Clean Code - Uncle Bob
        Changed the way I write code
        1/2 of the way through :(
    I should get better at pulling the plug

What has you the most excited about your current projects and future?

    New team
        Clicked in interview
    Opportunity to share my love of speaking with new job
        Get people out there speaking
        hive mind?
    Not as much speaking, so I'm looking into other mediums to provide value
        writing
        videos? 
    Rebranding of MKE DOT NET
        Cream City Code
    Submitting workshop to conferences

What are the greatest sources of pain in your life and work?

    self-doubt

What do you geek out about other than software that really lights you up?

    outdoors
        wisconsin - 3 months great, 3 months hot, 6 months darkness/despair
        camping
        triathlons with steph
        mountain biking, trail running (summer)
        snowboarding, indoor rock climbing (winter)
    
        analogical thinking between this & my work 

Can you make one prediction for the future of software?
    It's not going to take much longer for tech giants on the coasts
    to discover talent in flyover states
    and not require them to move
    more coworking spaces
    wishful thinking


3 tips for delivering more value.
    1. Tighten the feedback loop
        applicable in so many places
        Dev
            Not doing testing - 
                not getting feedback until your code hits test env
                Start writing tests, see if your changes are working.
            testing after - 
                not getting feedback until after you made all your changes
                TDD
            deployments are manual - 
                you're avoiding deployment, because they take so long
                so it's a longer cycle between building a feature & release
                CI/CD will help tighten that loop
        Product
            Just ship it
                gets product in front of customer, quicker feedback
            5 hops between you and customers
                Takes longer to get their feedback
                Game of telephone
                Eliminate some of those hops, talk to them directly if you can
        Personal development
            Waiting for review to see how you're doing
                Ask for 1 on 1s
                Check in more often
        Writing/speaking
            Don't wait to deliver to the audience
            Get feedback from coworkers, friends
            Refine it before you bring it to the audience
    2. Find the way that works for you, do it unapologetically
        Scott Hanselman, video on getting started in speaking
        You'll get tons of advice 
            it comes from a good place
            But it is often more about the person giving it, than you.
        Kent Beck - feedback sorting
        Ask for all the feedback you can get
        Listen to it
        But don't act on all of it.
        Pick & choose the things that 
            are about you
            are actionable
            you understand.
        Don't be afraid to stray from advice
            if you feel strongly that it's going to work.
            Example: I script all my talks.
            It works for me.
            I don't read them like a robot
                But having words figured out ahead of time gives me confidence
    3. Do Scary Things
        Push yourself out of your comfort zone.
        Don't just do what is easy & comfy
        We tell ourselves stories about all the things that could go wrong
            And they're often exaggerations
        We ignore the incredible payoff that comes with doing new & scary things
            It opens up a whole new world of possibilities
        We're so much more capable than we let ourselves believes
            But you have to push past fear to find out.





