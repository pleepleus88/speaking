Dynamic back-ends run much of the web, but they come with a price - complexity, performance, and security. 

The current tooling for building static websites is remarkable. This talk will cover static generation tools that can help you eliminate unnecessary architecture and complexity.  