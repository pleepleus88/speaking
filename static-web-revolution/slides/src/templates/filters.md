<svg width="800px" height="600px" >
   <defs>

     <filter id="posterize2" color-interpolation-filters="sRGB">
        <feColorMatrix
            type="matrix"
            values="1 1 0 0 0
                    1 0 0 0 0
                    1 0 0 0 0
                    1 0 0 1 0 "/>

        <feComponentTransfer>
    <feFuncR type="discrete"
        tableValues="0 0.3 0.5 0.9 1"/>
    <feFuncG type="discrete"
        tableValues="0 0.3 0.5 0.9 1"/>
    <feFuncB type="discrete"
        tableValues="0 0.3 0.5 0.9 1"/>
        </feComponentTransfer>
     </filter>
     <filter id="posterize" color-interpolation-filters="sRGB">
        <feComponentTransfer>
    <feFuncR type="discrete"
        tableValues="0 0.25 0.5 0.75 1"/>
    <feFuncG type="discrete"
        tableValues="0 0.25 0.5 0.75 1"/>
    <feFuncB type="discrete"
        tableValues="0 0.25 0.5 0.75 1"/>
        </feComponentTransfer>
         <feColorMatrix
            type="matrix"
            values="1 0.4 0 0 0
                    1 0 0 0 0
                    1 0 0 0 0
                    1 0 0 1 0 "/>
     </filter>
   </defs>
</svg>
