---
layout: true
class: sub-section
name: root

# History Of The Web

---

???

it is stupid to have to double-fence these
but layout slides don't show up by themselves.

---


## The before time

---

## Later

---
layout: true
template: root

## The Now Time

---

---

### Is this nested?

---

### Cuz it should be

---
layout: true
class: sub-section
name: chapter-2

# Chapter 2

{{content}}

^^^ the content goes there

---

---

## Some things are happening

---

## I don't know what

---

layout: true
template: chapter-2

## We should find out

{{content}}

???

Without that {{content}}, our next sub-content would be appended

instead of above the "content goes there".

---

---

### Is it purple?

---

### Can it fly?

---

### No.