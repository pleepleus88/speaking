---
template: title

# The Static Web Revolution

---
template: level-2
class: bg-cover
background-image: url(images/metamorphosis.jpg)

???

A revolution is an **inflection point**

A **metamorphosis**

A significant change is made in how we do something.

The **old way** becomes the **new way**

---
template: level-2
class: bg-cover
background-image: url(images/heavy-cms.jpg)

# The Old Way

???

The **old way** of website development looks like this **container ship**.

We used **bulky CMS's** that accomplished **many different things**...

But they were **hard to navigate**.

---

template: level-2
class: bg-cover, inverse-text
background-image: url(images/micro-cms.jpg)

# The New Way

???

The **new way** looks like this.

Instead of putting everything into one giant CMS

We have **lots of specialized boats**, that each do the things they're good at.

A boat for **authentication**

**content mgmt**

**serving content**


---
template: level-2
class: image

# The New Way

![Augmented static](images/drawings/augmented-static.png)

???

This is the future of building websites that I'm excited about.

The Scalability, Speed, and Security of static websites

Augmented with JavaScript and APIs

And managed with a headless CMS

is where I see the future of building websites.

---

template: title

# Thank You!

.col.col-4.about.left.centered[

## Steven Hicks
### <i class="el el-twitter"></i>  [@pepopowitz](http://twitter.com/pepopowitz)
### <i class="el el-envelope"></i>  steven.j.hicks@gmail.com
### <i class="el el-globe"></i>  [bit.ly/static-web-revolution](http://bit.ly/static-web-revolution)

]

???

I'd like to thank you for your time.

I really appreciate it.

I hope you have a great time at That Conference.

---
template: level-2
class: double-wide, hide-footer

# Resources

.leftest[
### Tools
* Geocitiesizer - http://www.wonder-tonic.com/geocitiesizer/
* Jekyll - http://jekyllrb.com
* Hugo - http://gohugo.io
* Eleventy - http://11ty.io
* Gatsby - https://github.com/gatsbyjs/gatsby



### Directories
* http://www.staticgen.com
* http://www.thenewdynamic.org/tools

### Images
* Files - https://www.flickr.com/photos/stevenbley/4174688409
* Speed - https://unsplash.com/photos/8tXukRrs7yk
]


.rightest[

### Images
* Security - https://unsplash.com/photos/8yYAaguVDgY
* Simplicity - https://unsplash.com/photos/gN8vJz4gPh8
* Scalability - https://unsplash.com/photos/mVhd5QVlDWw
* Source Control - https://unsplash.com/photos/xekxE_VR0Ec
* Wiring - https://unsplash.com/photos/qAShc5SV83M
* Lock - https://unsplash.com/photos/8yYAaguVDgY
* Metamorphosis - https://static.pexels.com/photos/39862/cocoon-butterfly-insect-animal-39862.jpeg
* Container Ship - https://static.pexels.com/photos/104346/pexels-photo-104346.jpeg
* Boats - https://unsplash.com/photos/PR_LKkOiaUQ
]