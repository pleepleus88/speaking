---
template: title
# Goals

???

I've said a lot of things about WordPress

And I don't mean to dismiss the value of it

It is a great platform that has worked for millions of people

My goal today isn't to tell you what's wrong with WordPress.

---
template: title

# A compelling alternative
## To the traditional dynamic website

???

My goal today is to offer you a compelling alternative

to the traditional dynamic website

---
template: title

# Dynamic Websites

???

So let's talk about dynamic websites.

---
layout: true
template: level-2
class: statement

# Dynamic Websites

---

## Content is stored in a database

---

## HTML is generated when the user requests it

???

HTML is generated dynamically based on that content

When the user requests it

---

## Content is often managed with a CMS

---
class: image

![Dynamic 1](images/drawings/dynamic-1.png)

???

in a nutshell...

---
class: image

![Dynamic 2](images/drawings/dynamic-2.png)

---
class: image

![Dynamic 3](images/drawings/dynamic-3.png)

???

Contrast this with static websites.

---

layout: true
template: level-2
class: statement

# Static Websites

---

## Content is stored as static files


---

## HTML is generated when content is changed

---

## Content can be managed with a static site generator

---
class: image

![Static 1](images/drawings/static-1.png)

???

in a nutshell...

---
class: image

![Static 2](images/drawings/static-2.png)

---
class: image

![Static 3](images/drawings/static-3.png)

???

If you look back 20 years to the before time

When we were on Geocities

and a marquee tag was the best way to make a statement

It's not much different than the static sites I'm describing here

** the difference is that the tooling is so much better **

