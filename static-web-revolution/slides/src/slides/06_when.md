---
template: title

# When Should You Go Static?

---
layout: true
template: level-2
name: when

# When

---
class: bold-statement

## Blogs

---
layout: true
template: level-3

# When
## Blogs

---
class: image, hide-footer

![personal blog](images/blog.jpg)

???

It might be a personal blog

---
class: image, hide-footer

![Centare.com](images/centaredotcom.jpg)

???

It might be a company website and blog

This is the actual Centare.com website, where I work

It used to be a WordPress site

We rewrote it using Harp

We used to get errors from broken plugins about once a month

Now it's never down.

---
class: image, hide-footer

![SmashingMagazine.com](images/smashing.jpg)

???

It might even be something even bigger

Like smashing magazine

---

layout: true
template: when

---
class: bold-statement

## Portfolios

---
template: level-3
class: image, hide-footer

# When
## Portfolios

![Portfolio](images/portfolio.jpg)


---
class: bold-statement

## Brochures

---
template: level-3
class: image, hide-footer

# When
## Brochures

![Tosa Yoga](images/tosayoga.jpg)

---
class: bold-statement

## Docs

---
template: level-3
class: image, hide-footer

# When
## Docs

![Docs](images/docs.jpg)

???

---
class: bold-statement

## Style Guides

---
template: level-3
class: image, hide-footer

# When
## Style Guides

![Style Guide](images/styleguide.jpg)

---
class: bold-statement

## Events

---
template: level-3
class: image, hide-footer

# When
## Events

![Cream City Code](images/creamcitycode.jpg)

???

This also used to be a Wordpress site

Now we're using gatsby

Updates are really easy to push

And again, we never worry about trying to get the WordPress instance back up after a bad update.


