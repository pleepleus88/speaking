---
template: title

# How Do I Get Started?

---
template: definition

# How

## Static Site Generator
### A command-line tool that takes simple, flat files as input and converts them to a complete website of static HTML files.

???


Choose a static generator

But we need to talk about a few things first.

...

So right off the bat

Know that you're going to be using the command line.

---
layout: true
template: level-3
name: how

# How

---

## How Do I Customize?

--
### CSS/LESS/SASS/PostCSS

???

Most have support for a css precompiler, if not multiple

--
### Themes (if you are lucky)

---
class: image

## How Do I Write Content?

???

It depends on what you mean by content.

--

![Website](images/drawings/website.png)

---
class: image
## How Do I Write Content?

![Templates](images/drawings/templates.png)

---
class: image
## How Do I Write Content?

![Content](images/drawings/content.png)

---

## How Do I Write Templates?

--

### With your favorite templating language

???

Maybe you don't have one!

The truth is - it doesn't much matter.

---
layout: true
template: level-3
class: image-center

# Templates

---

## EJS

![EJS](images/how/ejs.png)

???

Because you're really just deciding what syntax you want to use to inject content

---

## Handlebars

![HBS](images/how/handlebars.png)

---

## Pug (Jade)

![Pug](images/how/pug.png)

---
template: how

## How Do I Write Content?

--

### With a text editor

--

### In MarkDown

---
class: double-image
layout: false
template: level-3

# Content
## Markdown

.leftest[
![Markdown](images/how/markdown.png)
]

--

.rightest[
![HTML](images/how/html.png)
]

???

If you don't like markdown

And I don't blame you

With a lot of generators, you can use the same language you used for templates.

---
class: statement
template: how

## How Do I Add Metadata?

???

Things like "this is a draft"

and "date published"

--
### Frontmatter

---
layout: false
template: level-3

# Metadata
## FrontMatter

.col.col-5.centered.frontmatter[
#### about.md
![FrontMatter](images/how/frontmatter.png)
]

---
template: title

# Choosing A Generator

---
template: title

# [staticgen.com](https://www.staticgen.com)

---
layout: true
template: level-2

# Choosing A Generator

---
class: image

![staticgen.com](images/staticgendotcom.jpg)

???

go to staticgen.com

a directory of most open source static site generators

---
class: image
restart-animation: images/staticgendotcom.gif
name: staticgendotcom

![staticgen.com](images/staticgendotcom.gif)

???

scroll....

we're going to define some criteria.

---

layout: true
template: level-3
name: choosing-a-generator

# Choosing A Generator

---

## Potential Criteria

--

### Engine Language

--

### Templating Language

--

### Simplicity vs Customization

--

### Extensibility

--

### Frontmatter Support

---
template: title

# Suggestions

---
template: level-3
layout: true
class: comparison
name: suggestions

# Suggestions

---
template: suggestions

## Jekyll

--

.pros[
* Support
* Themes
* Features
]

--

.cons[
* Difficult setup
* Not Windows-friendly
]

---
class: image-center

## Jekyll

.col.col-5.centered.profile.posterized[
![Dad](images/bumpa-1.jpg)
]
???

Jekyll is kind of like my dad.

---
class: image-center

## Jekyll

.col.col-5.centered.profile.posterized[
![Dad](images/bumpa-2.jpg)
]
???

He's a retired plumber.

If you give him a problem, and he can rig together a solution.


---
template: full-screen
background-image: url(images/jekyll-mess.jpg)

???

Unlike my dad, though, sometimes when you're solving problems with Jekyll,

it feels like you're just doing it this way because it works,

not because it's the right way to do it.

credit: https://unsplash.com/search/wiring?photo=qAShc5SV83M

---
template: suggestions

## Hugo

--

.pros[
* Fast
* Support
* Themes
]

--

.cons[
* No extensibility
]

---
class: image-center

## Hugo

.col.col-5.centered.profile.posterized[
![Brandon](images/brandon.png)
]

???

Hugo is kind of like my coworker Brandon.

He gets stuff done really fast, and with great quality,

and makes you feel like a snail.

If you have a lot of content to turn into HTML, Hugo is a great option.

---
template: suggestions

## Eleventy

--

.pros[
* Simple
]

--

.cons[
* Not a lot of bells & whistles
]

---
class: image-center

## Eleventy

![Banana slicer](images/banana-slicer.jpg)

???

Eleventy is kind of like a banana slicer.

it doesn't do much.

people aren't going to talk about it a whole lot.

but if you need to slice a banana..or in this case build a really simple static site.....this is how you do it.

---
template: suggestions

## Gatsby

--

.pros[
* React
* PWA
* Momentum
]

--

.cons[
* React
* You might not need all that stuff
]

---
class: image-center

## Gatsby

.col.col-5.centered.profile.posterized[
![Ryan](images/ryan.png)
]
???

Gatsby is kind of like my coworker Ryan.

Ryan has all the coolest, latest gadgets.

Gatsby is pretty cool technology.

---
layout: true
template: full-screen

---
background-image: url(images/cheerios.jpg)

???

But sometimes...you have a bowl of honey nut cheerios...

---
background-image: url(images/banana.jpg)

???

and a banana...

---
background-image: url(images/cheerios-bananas.jpg)

???

and you want to end up here.

---
layout: false
class: image-center

![Banana slicer](images/banana-slicer.jpg)

???

a banana slicer will get you there much more easily than a sapphire garmin watch.

seems  like a joke

but i strongly believe in using the simplest tool that accomplishes what you're trying to do

and that is often eleventy for me.

---

template: how
layout: true

## How Do I Host?

---
class: bold-statement

--

### Anywhere.

???

It’s just static files

So you can host it anywhere.

If you're looking for some specific ideas....

---

--

### Existing Infrastructure

--

### Amazon S3

--

### Dropbox

--

### Github Pages

--

### Dedicated Static Hosts (surge, Aerobatic)

---
class: bold-statement

### Netlify

---
template: how

## Netlify Features

--

### Run builds for most static generators

--

### Free custom domains

--

### Free SSL

--

### Baked-in CDN

--

### Serverless Functions
