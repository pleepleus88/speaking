---
template: title
class: jam-stack

# The JAM Stack

## **J**avaScript

## **A**PIs

## **M**arkup

![JAM stack](images/jam.png)


???

And so with our JAM stack

We've improved the lives of developers.

---
template: full-screen
background-image: url(images/why/security.jpg)

???

Our site is much more secure,

and less work to maintain.

credit - https://unsplash.com/search/security?photo=8yYAaguVDgY

---

template: full-screen
background-image: url(images/why/speed.jpg)

???

We've improved the lives of our users,

Because our site flies like the wind.

---
template: image
restart-animation: images/dinojump.gif
name: dino-jump
class: hide-footer

![Dino Jump](images/dinojump.gif)

???

But we still can't focus on the things we want to focus on

Because we forgot about someone.

---
template: image

.col.col-5.centered.profile.posterized[
![Stephanie](images/stephaniehicks.jpg)
]

???

The content authors.

This is my wife Stephanie. Steph is an English teacher & yoga instructor.

She wants to post to her blog every once in a while.

But she's focused on lots of other things.


---
template: image

![VS Code](images/vscode.jpg)

???

She doesn't have the time or interest to learn MarkDown.

She's not interested in using a text editor to post something to her site.

For Steph we need a more visual tool.

...

This is why we need to talk about

---
template: title

# Authoring Tools

---
template: level-3
layout: true

# Authoring Tools

---
class: image, hide-footer

## Text Editor

![VS Code](images/vscode.jpg)

???

For devs,

we have our favorite text editor.

And we can learn to write in MD.

You also might have a tech-savvy author.

---
template: level-2
class: image

# Authoring Tools

.col.col-5.centered.profile.posterized[
![Stephanie](images/stephaniehicks.jpg)
]

???

For Steph, and the less-savvy author,

we need something more visual.

---
template: level-2
class: bold-statement

# Authoring Tools
## Headless CMS

???

we need...

and a headless cms is basically...

---
template: level-3
class: bold-statement

# Authoring Tools
## Headless CMS

### CMS As A Service

---
class: image

## Without a Headless CMS

![No CMS](images/drawings/no-cms.png)

???

without a headless cms,

(describe slide)

---
class: image

## With a Headless CMS

![Headless CMS](images/drawings/headless-cms.png)

???

a headless cms typically swaps out that git repository

for content as a service.

the content is stored in their db instead of in your repo.

your static site generator will hit an api

and consume the content from the headless cms at build time

**note**: you're introducing more attack vectors.

---
layout: true
template: level-3

# Headless CMS

---
class: image, hide-footer

## Contentful

![Contentful](images/contentful.jpg)

???

Contentful

CMS as a service is young

Many of these have launched pretty recently

There are a lot of features yet to come in this space

---
class: image

## Netlify CMS

![Netlify CMS](images/netlify-admin.jpg)

???

but there's one headless CMS that stands out to me.

Netlify CMS - built by the people at Netlify.

---
class: image

## Netlify CMS

![Netlify CMS](images/drawings/netlify-cms.png)

???

It's a little bit different, in that it integrates with Git

so that the content is still stored in your repo

When you make changes, it pushes to your repository

If you're site is hosted on Netlify, Netlify will see those changes, and regenerate your static site.

---
class: image, hide-footer

## Netlify CMS

![Smashing Magazine](images/smashing.jpg)

???

and if you're wondering about the viability of this

this is what Smashing magazine uses.

---
template: title

# [headlessCMS.org](https://www.headlessCMS.org/)

???

Maybe Netlify CMS isn't right for you.

If you want to find the right headless CMS,

there's a directory for them.
