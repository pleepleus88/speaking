---
template: title
# Why Should You Go Static?

???

I have 5 reasons you should consider static sites

And it is coincidence that they all begin with S

---
layout: true
template: level-2
class: bold-statement

# Why

---
class: bg-cover, inverse-text
background-image: url(images/why/speed.jpg)

## Speed

???

Static html files **serve faster**

They are **CDN ready**

https://unsplash.com/search/cycling?photo=8tXukRrs7yk

---
class: bg-cover, inverse-text
background-image: url(images/why/security.jpg)

## Security

???

Dynamic back-ends are

susceptible to many different types of attacks.

Static sites **remove** so many **moving parts**

And **eliminate** so many **attack vectors**.

https://unsplash.com/search/security?photo=8yYAaguVDgY

---
class: bg-cover, inverse-text
background-image: url(images/why/simplicity.jpg)

## Simplicity

???

Fewer moving parts

= fewer points of failure

You don’t have to worry about your
**database going down**

You don’t have to worry about
when to **clear the server cache**

https://unsplash.com/search/swing?photo=gN8vJz4gPh8

---
class: bg-cover, inverse-text
background-image: url(images/why/scalability.jpg)

## Scalability

???

As a result of the simplicity,

your entire site is CDN ready.

https://unsplash.com/search/people?photo=mVhd5QVlDWw

...

Think about all the effort you’ve invested over the years in

**optimizing database calls** and **caching strategies**,

and trying to **move** as many things **to a CDN** as possible.

...

When your entire site can live on a CDN..

that is a huge scalability win,

and it **saves you a ton of time**.

---
class: bg-cover, inverse-text
background-image: url(images/why/source-control.jpg)

## Source Control

???

Your content is stored in your source control repository

If you want to know what a page looked like on a certain date,

or who changed it and when,

you just browse the commit log.

https://unsplash.com/search/photos/code?photo=xekxE_VR0Ec
