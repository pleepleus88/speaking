---
template: title

# The Static Web Revolution

.col.col-4.about.left.centered[

## Steven Hicks
### <i class="el el-twitter"></i>  [@pepopowitz](http://twitter.com/pepopowitz)
### <i class="el el-envelope"></i>  steven.j.hicks@gmail.com
### <i class="el el-globe"></i>  [bit.ly/static-web-revolution](http://bit.ly/static-web-revolution)

]

???

thank you:
* organizers
* you!

show of hands:
* learned something new
* left a talk inspired
* met someone interesting
* tired of sitting
    if so, stretch


**Contact Info**


---
layout: false
class: bg-cover
background-image: url(images/sponsors.jpg)

???

sponsors

---
layout: true
class: middle
name: intro

.col.col-5.profile.posterized[
![Me](images/steven-hicks-2.jpg)
]

---

.col.col-5[
# Steven Hicks
## JavaScript Engineer????
]

???

I've been building websites for a while.
