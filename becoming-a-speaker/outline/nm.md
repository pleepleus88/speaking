Introductions
  Me
    * Speaking at conferences 1.5 years
    * 4 code camps
    * 7 conferences
    * Selfish goal: get people to come with me
    * Unselfish goal: help people experience the incredible growth of speaking
    * Today: answer questions
Questions
  Why should I speak?
    Giving back to dev community
      * Feels good
    Free conference ticket
      * and sometimes travel
      * so you can learn stuff
      * and visit places
    Career development
      * Become a better dev
      * designer
      * leader
      * person
    Branding (self)
    Branding (NM)
      * As an outsider to NM until recently, 
        I've seen a ton of great talent coming here
        I can't even imagine all the talent that I don't know about.
        We can do a better job of letting people know.
      * Tony: NM Support
  How do I get over the impostor syndrome/self-doubt that prevents me from speaking?
    You don't need to be an expert
      * Misconception
      * Curse of knowledge
        * As soon as you know something,
          you forget what was hard to learn about it.
      * The best person to teach you something you don't know
        is often someone who just learned it.
    You don't need to be a good speaker
      * You haven't done it very much
      * Takes practice
    Speaking is scary for everyone
      * I still get scared
      * People with way more experience still get nervous
      * Doc Norton
  How do I come up with ideas to talk about?
    Things you know really well
      * Something you've been working with for a year
      * You don't need to be an expert in "JavaScript", but maybe you have a lot of experience in working with forms in Angular
          That's more than others have
    Things you recently learned
      * Many others haven't learned them yet
    Things you want to learn but haven't yet
      * Conference-driven learning
    Your experiences 
      * i.e. we had this problem
      * and this is how we solved it
    Personal development
      * my favorite
      * Things that have helped you become better
      * overcoming fear, imposter syndrome, better communication,...
  How do I get to speak at a conference?
    CFP
      * A few months before the event
    Title, Abstract, Bio
      * Title: Clarity first, wittiness second
      * Abstract: Short description that will end up on the schedule
        * It's how people will pick your session over others
      * Bio
        * Description of you
        * Promote yourself
  How do I write a good abstract?
    Know who you're writing it for
      * Eventually, the attendees
      * Initially, the reviewers
    Use proper grammar & punctuation
      * Prove to reviewers that you can communicate to attendees
    Express the intention of the session
      * Be clear
      * Get to the point
    Stand out
      * Reviewers see many sessions
    Tell attendees what they'll get out of the session
      * Describe their problem, offer a solution
      * Your session isn't going to be for everyone
      * Maximize YOUR audience
  Example: Not Enough Information
  Example: Not Clear Or To The point
  Example: Just Right
    ....and then you wait
  I didn't get accepted. Now what?
    Keep trying!
      * Acceptance rate is really low. 
      * maybe 2/10
    Ask for feedback
      * Send an email
  Oh shoot, I got accepted! Now how do I write my talk?
    Content > slides
      * Whatever that means for you
      * Make sure in the end, your content is solid
      * And good slides is gravy
    Why > How > What
      * Those are in order of least likely to find on internet
      * Why will motivate/inspire them
    Tell a story
      * 45 minutes to an hour of listening
      * Best received as a narrative
      * Provide them "hooks" for later
    Focus on the audience
      * not the tech being demonstrated
      * Consider their pains of learning this thing
      * Help them learn it
  How do I make good slides?
    Use fewer words
      * Words distract them from you
    Use fewer bullet points
      * Again, they distract people from your message
    Large text
      * Code samples, make them big
      * If you're going to use an IDE or command-line, zoom in
      * Projectors are often garbage
    High visibility/contrast
      * Projectors are often garbage
  Do I need a live demo?
    No!
  How do I prepare for my talk?
    Practice a ton
      * Not to the point of memorization
      * Know the concepts - not the words
    Give it at work!
      * Fed summit, your team, ...
    Give it at a meetup!
      * MKE JS, MKE WEB PROS, ReactJS, ...
      * Procrastinators: This gives you an earlier due date.
    Get feedback from people
      * So that when you're ready to give it, it's not your first draft.
  What do I need to know before I start talking?
    Things will go wrong. 
      * Don't depend on internet.
      * ...being next to your computer.
      * ...having a projector.
      * ...sound
      * ...your computer
    Do a tech-check
      * Night before, early morning
    Everyone gets nervous.
      * Remember: they want you to succeed.
      * Find a way to calm yourself before
  How do I handle audience questions?
    You choose! 
      * During? After? Never?
    Don't be afraid to say "I don't know".
      * Don't dig yourself a hole.
      * It doesn't discredit you.
  How do I handle annoying attendees?
    There aren't very many of them.
      * I've seen 2
    Ask them to talk after.
      * Let them know you hear them
      * Suggest that there are others who want to hear you
  What are some open CFPs?
Next steps
  CFP Jam/Abstract Party?