---
layout: false
template: cover-art

# THANK YOU!

.about.left[
## Steven Hicks
### <i class="el el-twitter"></i> [@pepopowitz](http://twitter.com/pepopowitz)
### <i class="el el-envelope"></i> steven.j.hicks@gmail.com
### <i class="el el-globe"></i> [bit.ly/pragmatic-react-patterns](http://bit.ly/pragmatic-react-patterns)
]

???

I want to thank you for your time

I really appreciate it.

Thank you!

---
template: level-1
class: double-wide, resources

# Resources

* [Babel](https://babeljs.io/)
* [Thinking In React](https://reactjs.org/docs/thinking-in-react.html)
* [Ducks](https://github.com/erikras/ducks-modular-redux)
* [Prettier](https://prettier.io/)
* [Code samples](https://github.com/pepopowitz/maintainable-react-samples)

---
template: level-1
class: double-wide, resources

# Images

* Cover Art - Karly Santiago, https://unsplash.com/photos/Nmi-xhZ-JrkF
* The rest - Me

