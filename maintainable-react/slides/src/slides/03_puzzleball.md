---
template: next-pattern
background-image: url(images/puzzleball-pattern.jpg)

# 2. "Puzzleball" Pattern

???

The namesake for this pattern: 

13 years ago, at my bachelor party

Hung over after camping in New Glarus Woods state park

Sandeep put 25c into a kids vending machine

it was garbage! like gobots. but it stuck with me. 

---
template: level-1
layout: true
# Puzzleball

---

> Let's decompose and enjoy assembling

???

because of the instructions.

Which is just such a good translation

So good that I once made a t-shirt about it

And who knew that 15 years later, 

it would be a perfect mantra for React JS.

Because one of the most important things to do in React...

---
template: level-2
layout: true
# Puzzleball
## Let's Decompose...!

---

### Break your components into 
### appropriately-sized pieces

???

And assemble them

---

### https://reactjs.org/docs/thinking-in-react.html


???

Thinking in react - the best page in the docs

---

> #### **Step 1:** Break The UI Into A Component Hierarchy
> #### **Step 2:** Build A Static Version in React
> #### **Step 3:** Identify The Minimal (but complete) Representation Of UI State
> #### ...

.footnote[
https://reactjs.org/docs/thinking-in-react.html
]
???

Build your components without any actual state

So you can find the proper boundaries

Don't worry about state until way later

In doing this, you'll be able to break your components down!

---
template: level-3
layout: false

# Puzzleball
## Let's Decompose...!

### Why?

* Things are easier to test when they are cohesive

???

Edges are easier to find

...

--
* Things are easier to read when they are cohesive

???

Like an outline. 

Less cognitive load/context-switching

So break components down by concern

Break functions down by concern

---

### Make things meaningfully small

???

It's not unheard of for me to have many functions that are only a few lines long

And a component that assembles a bunch of them.

But I also have components that have 80 lines of code, because they all pertain to the same concern

---

### Components are about ~~reuse~~ isolation

???

It's also important to know WHY we're breaking them down

It's no about reuse

It's about isolation. 

The ability to abstract something into a black box, 

and not have to worry about the internals of that black box

unless you need it

...

So to break things down, don't necessarily look for things that can be reused

Look for logical **boundaries of responsibility**
