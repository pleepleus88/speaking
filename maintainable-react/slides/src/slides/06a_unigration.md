---
template: next-pattern
layout: false
background-image: url(images/pyramids.jpg)

# 4. "Unigration" Pattern

???

Testing

---
template: level-1
layout: true

# Unigration

---
class: bg-contain
background-image: url('images/test-pyramid.jpg')

???

testing pyramid - describe it

mike cohn

...

UNIT: sometimes too small/isolated to add confidence

& people can be overly dogmatic about what defines a "unit"

INTEGRATION: slow

& we can tell a lot from our app without pulling in db/api's

UI: /shrug awful to maintain

---
class: bg-contain
background-image: url('images/test-pyramid-unigration.jpg')

???


include anything that is fast-running & below in the tree

mock anything that is slow-running (api, db, fs)


---
template: level-2

# Unigration
## Why?

* Enables refactoring

???

tiny unit tests make refactoring brittle

often too focused on implementation

--

* Adds confidence

???

tests are closer to realistic

without making your tests super slow
