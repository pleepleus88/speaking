---
template: next-pattern
background-image: url(images/pacifist.jpg)

# 8. "Pacifist" Pattern

???

How many people have had an argument over someone about **code format**?

Had someone on your team commit format changes?

Have BEEN the person that committed format changes?

More importantly...

---
layout: true
template: level-2
name: pacifist
# Pacifist

---
class: bold-statement

## Poll

### Tabs vs Spaces?

---
class: bold-statement

## Poll

### Semicolons vs Not semicolons?

---

## Solution

???

I'm proud to tell you I have found the answer.

---
layout: true
class: title, inverse
name: who-cares

---

# WHO F%@$ING CARES?????!!!

???

who cares?!?!?!

Why are we wasting our time arguing about this?

Years ago, it mattered, because we had to all manually format things to look the same.

But now we can automate it!

---
layout: true
template: pacifist
---

## Solution

### Prettier
### [prettier.io](https://prettier.io/)

???

An opinionated code formatter

sensible opinions

with plugins for all major code editors


---
force-gif-restart: true
name: prettier
class: no-footer

## Prettier

.content-image[
![Prettier formatting ugly code](images/prettier.gif)
]

???

You can write garbage code, and let prettier clean it up for you

---
## Prettier

### I've got opinions!

???

I know what you're thinking

I heard the word "opinion". 

I have opinions. What if Prettier doesn't match my opinions?

---
template: who-cares

# WHO F%@$ING CARES?????!!!

---
template: who-cares
class: ftfy-semi-colon

# WHO F%@$ING CARES?????!!!

???

ftfy

...

but really, It is configurable.

And you can connect it to your eslint rules really easily, if you're using that.

Eslint + eslint-prettier + prettier

CI/CD

---

## Why?
* Ends senseless arguments

???

ends arguments about how your code should be formatted

Prettier is opinionated, but the opinions are sensible

--
* Never worry about formatting your code again!

???

So you'll never have to worry about formatting code again!

...

And now you can worry about things that matter.

---
class: bg-contain
background-image: url('images/bikeshed.jpg')
???

like what color we should paint the bikeshed
