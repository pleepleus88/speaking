---
template: next-pattern
background-image: url(images/suitcase.jpg)

# 5. "Suitcase" Pattern

---
template: level-1
layout: true

# Suitcase

---

???

Background: packing a suitcase.

---
class: bg-contain
background-image: url('images/shirts-with-shirts.jpg')

???

I've always packed like items together.

shirts with shirts, pants with pants

...

Overhead: calculate "what I need to wear each day" in my head;

pack items into suitcase;

unpack into drawers;

---
class: bg-contain
background-image: url('images/shirts-with-shirts-highlighted.jpg')

???

and then every morning, I have to rip through all those piles and assemble an outfit.

...

One day I realized I didn't have to do this.

---
class: bg-contain
background-image: url('images/outfits.jpg')


???

I could pack my suitcase as outfits.

Determine what I'll need fo reach day, and pack each day as a unit.

Then they could go into the drawer just like this

---
class: bg-contain
background-image: url('images/outfits-highlighted.jpg')

???

And I can grab a day's-worth of clothes every morning.

---
class: no-footer

```javascript
components
  kittenLovesMeCheckbox.jsx
  kittenName.jsx
  layout.jsx
  login.jsx
  register.jsx
pages
  feedKitten.jsx
  kittenForm.jsx
  login.jsx
  register.jsx
api
  api-client.js
styles
  feedKitten.css
  kittenLovesMeCheckbox.css
  kittenName.css
  layout.css
  login.css
  register.css
```

???

We have a long history of sorting our code by what type of garment it is.

Shirts with shirts; reducers with reducers; components with components.

---
class: no-footer

.dim-1.dim-2.dim-3.dim-4.dim-6.dim-7.dim-8.dim-9.dim-11.dim-12.dim-14.dim-15.dim-16.dim-17.dim-18.dim-20[
```javascript
components
  kittenLovesMeCheckbox.jsx
  kittenName.jsx
  layout.jsx
  login.jsx
  register.jsx
pages
  feedKitten.jsx
  kittenForm.jsx
  login.jsx
  register.jsx
api
  api-client.js
styles
  feedKitten.css
  kittenLovesMeCheckbox.css
  kittenName.css
  layout.css
  login.css
  register.css
```
]

???

when we get a bug, it's not like it's all the reducers are broken, a feature is broken.

To fix it, we need to find the "authentication" shirt, "authentication" pants, "auth" socks

---
class:no-footer

```javascript
user
  login
    index.jsx
    index.css
    api-client.js
  register
    index.jsx
    index.css
    api-client.js
kittenForm
  index.jsx
  api-client.js
  kittenLovesMeCheckbox.jsx
  kittenLovesMeCheckbox.css
  kittenName.jsx
  kittenName.css
feedKitten
  index.jsx
  api-client.js
```

???

Or...we can organize our code by feature

Mix the different garments together, but have them related by what they're actually used for.

All the auth things together; all the dashboard things together; etc

---
class:no-footer

.dim-1.dim-2.dim-6.dim-7.dim-8.dim-9.dim-10.dim-11.dim-12.dim-13.dim-14.dim-15.dim-16.dim-17.dim-18.dim-19[
```javascript
user
  login
    index.jsx
    index.css
    api-client.js
  register
    index.jsx
    index.css
    api-client.js
kittenForm
  index.jsx
  api-client.js
  kittenLovesMeCheckbox.jsx
  kittenLovesMeCheckbox.css
  kittenName.jsx
  kittenName.css
feedKitten
  index.jsx
  api-client.js
```
]

???

and when we get a bug with the login form, 

everything is in one place.

---
template: level-2

# Suitcase
## Why?

* Less time looking for the different pieces that are related to a feature

--
* Put code together that changes together

???

And this is important because it makes our code more cohesive
