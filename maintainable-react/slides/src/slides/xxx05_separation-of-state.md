---
template: next-pattern
layout: false

# 3. "Separation Of Presentation & State" Pattern

pattern: ????

???

I MIGHT SKIP THIS ONE.

---
template: level-1
layout: true

# Separation Of Presentation & State

---

???

Embrace the separation of complicated state management & presentation

Most often associated with Redux

But the origin is not at all redux-y

https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0

---

Problem Code: Stateful + presentational component

---

Solution Code: Stateful component, presentational component

---

Why?
    Helps me separate code, obey SRP & SOC

???

Caveat
    Note the word "complicated"
    This is a pragmatic pattern.
    It's not a hard rule.
    If you have a component that doesn't do a lot - leave it as one.
    Code example: toggle component.

