---
template: cover-art

# Pragmatic Patterns For Maintainable React Apps

.about.left[
## Steven Hicks
### <i class="el el-twitter"></i> [@pepopowitz](http://twitter.com/pepopowitz)
### <i class="el el-envelope"></i> steven.j.hicks@gmail.com
### <i class="el el-globe"></i> [bit.ly/pragmatic-react-patterns](http://bit.ly/pragmatic-react-patterns)
]

???

**Contact Info**
**Thanks:**

* conference
* organizers
* you!

timekillers:

* favorite talks
* favorite speakers
* where from?
* stand up & stretch
* high fives

---
layout: false
class: bg-cover, no-footer
background-image: url(images/sponsors.jpg)

???

sponsors

---
layout: true
template: level-1
name: bio

# Steven Hicks
---

.profile.bio[
![Me](images/steve-by-olivia-square.jpg)
![Me](images/steve-by-lila-square.jpg)
]

???

I am standing right in front of you so you can tell what I look like

But on the left is what my 7 year old daughter Olivia thinks I look like

On the right is what my 10 year old daughter Lila thinks I look like

and if you think this is just an excuse to talk about my kids

and show you their artwork

You are correct.

---

## JavaScript Engineer???

???

I'm actually not sure what I am

new job next Monday

Milwaukee

---
template: title
class: inverse

# Patterns

???

This talk is about patterns.

Patterns is almost a "reserved word" in development

People think about the Gang of Four, Singletons, and Factories

That's not what I mean by patterns.

I'm talking about simple things that you can do to your code to make the experience better for someone reading it.

...

**Habits** might even be a better word.
