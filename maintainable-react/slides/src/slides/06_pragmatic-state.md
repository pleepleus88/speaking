---
template: next-pattern
layout: false
background-image: url(images/pragmatic-state.jpg)

# 3. "Pragmatic State" Pattern

???

How many people are using Redux?

Those of you using Redux, what percentage of the state in your app is managed by redux?

---
template: level-1
layout: true

# Pragmatic State

---

> In general, use Redux when you have reasonable amounts of data changing over time, you need a single source of truth, and you find that approaches like keeping everything in a top-level React component's state are no longer sufficient.

.footnote[
[redux.js.org](https://redux.js.org/faq/general#general-when-to-use)
]

???

This is directly from the redux FAQ

---

```javascript
// actions
const TOGGLE_LOVES_ME = "TOGGLE_LOVES_ME";

function toggleLovesMeActionCreator() {
  return {
    type: TOGGLE_LOVES_ME,
  };
}
```

???

So how do we end up with code like this to manage state for something as simple as a checkbox?

---

```javascript
// reducer
const initialState = {
  lovesMe: true,
};

function kittenLovesMeReducer(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_LOVES_ME:
      return {
        ...state,
        lovesMe: !state.lovesMe,
      };
    default:
      return state;
  }
}
```

---

```javascript
// presentational
function KittenLovesMeCheckbox({ lovesMe, onChange }) {
  return (
    <div>
      <label htmlFor="kittenLovesMe">Loves me?</label>
      <input type="checkbox" checked={lovesMe} onChange={onChange} />
      <div>{lovesMe ? "loves me!" : "hates me."}</div>
    </div>
  );
}
```

---
class: no-footer

```javascript
// container
const mapStateToProps = state => {
  return {
    lovesMe: state.kittenLovesMeReducer.lovesMe,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onChange: () => {
      dispatch(toggleLovesMeActionCreator());
    },
  };
};

const KittenLovesMeCheckboxContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(KittenLovesMeCheckbox);
```

---
class: medium, no-footer

```javascript
class KittenLovesMeCheckbox extends React.Component {
  state = {
    lovesMe: true,
  };

  handleChange = () => {
    this.setState(oldState => ({
      lovesMe: !oldState.lovesMe,
    }));
  }

  render() {
    return (
      <div>
        <label htmlFor="kittenLovesMe">Loves me?</label>
        <input
          type="checkbox"
          checked={this.state.lovesMe}
          onChange={this.handleChange}
        />
      </div>
    );
  }
}
```

???

I'll remind you - the example for this component that uses setState

is all right here. 

All of it.

---

> In the end, Redux is just a tool.  It's a great tool, and there are some great reasons to use it, but there are also reasons you might not want to use it.   Make informed decisions about your tools, and understand the tradeoffs involved in each decision.

???

here's some other things the redux faq says about when to use redux

Redux is a tool. 

You don't use a screwdriver to drive nails, and you don't use a hammer to drive screws. 

Developers love dogma. They love guiding rules, so that they don't have to think.

Oh, you have state? Use redux. 

The problem is, there is no tool that is going to solve every scenario. 

Redux has its uses. But it's not to manage ALL of your application's state.

---
class: bg-contain
background-image: url('images/state-setstate.jpg')

???

Use the built-in React state for state that only matters to one component.

---
class: bg-contain
background-image: url('images/state-elevated.jpg')

???

Use built-in React state and lift it up to the highest component, 

if you've only got a couple components that care about the state.

---
class: bg-contain
background-image: url('images/state-context.jpg')

???

If you have components that are a little further apart, or application-level state, 

Use the new **Context API**

---
class: bg-contain
background-image: url('images/state-redux.jpg')

???

You could also use redux for app-level state,

or if you want things like time-travel debugging.

---
class: bg-contain
background-image: url('images/dogmatic-state.jpg')

???

But don't use a hammer to drive screws. 

Use the appropriate state management approach for the job.


Further reading:

* https://spin.atomicobject.com/2017/06/07/react-state-vs-redux-state/
* https://medium.com/@dan_abramov/you-might-not-need-redux-be46360cf367
* https://github.com/jamiebuilds/unstated#faq

---
template: level-2

# Pragmatic State
## Why?

* Eliminate unnecessary boilerplate code

???

Redux leads to boilerplate code, and you want to make sure it's worth it.

--
* Simple components are easier to understand when you can see the state

--
* Redux actions imply distant side-effects
