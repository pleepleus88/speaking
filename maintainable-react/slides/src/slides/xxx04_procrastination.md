---
template: next-pattern
layout: false

# 3. "Procrastination" Pattern

pattern: ????

MIGHT SKIP THIS FOR TIME

???

Comes from an approach I like to take while TDD'ing

---
template: level-2
layout: true
name: procrastination


---
template: level-2
layout: true
# Procrastination
## Problem Description

---

### describe a problem space with lots of pieces
maybe a list of things that this feature will do?

---

### describe a hodge-podge solution where everything is smushed together

---

### describe a problem that breaks it down into individual things

#### including a really difficult thing

???

I think this is junk vvvv

Breadth-first instead of depth-first

    drawing: tree of decisions
    breadth-first: push off the complicated stuff; attack the happy paths
    depth-first: attack the complicated stuff/edge cases

---

### Why?

* Leads to a manageable test suite

???

Complicated decisions are tested in isolation

And it's easier for me to identify where they are

--

* Leads to a cohesive codebase

???

...

Boundaries are more clearly defined

Responsibilities are isolated

Complicated things often become difficult things to read

And if I have them isolated, it's less pollution in most of my code.
