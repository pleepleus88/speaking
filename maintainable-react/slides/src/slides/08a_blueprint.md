---
template: next-pattern
layout: false
background-image: url(images/blueprint.jpg)

# 7. "Blueprint" Pattern??

???


---
template: level-1
layout: true

# Blueprint

---
background-image: url(images/pragmatic-state.jpg)
class: bg-contain

???

be pragmatic & thoughtful about every tool you use

don't mail it in on anything

be intentional

...

problem: this takes so much time & thought.

---

## Solution: README.md

???

solution: document your decision-making process in your README

---

```markdown
## Choosing a state management mechanism

### Does only this component care?

Use setState.

### Do only a few closely-related components need it? 

Use setState, & elevate it to the lowest common ancestor.

### Is it state that the entire app needs?

Use React.Context.

```

???

example - choosing state

...

I'm not looking for coding standards - 

just guidance to make a decision. 

What does this project prioritize, because they're all different?

---
template: level-2
# Blueprint
## Things to put in a README

--

* FAQ

???

a. you probably have some, and the answers are tribal knowledge

--

* Project Goal

???

b. this will help me understand why we make certain decisions

--

* Guiding Principles

???

c. Help me make a decision on my own

--

* Architectural Priorities

???

d. what do we prioritize? performance? readability? extensibility?

you can't say all of them. you can maybe say 3. just like your life priorities.

--

* Reasons For Decisions

???

e. "why did we choose XXX???"

---

## Maintainability:
## Making it easier for people in the future

---
class: bg-contain
background-image: url('images/future-me-cyborg.jpg')

???

not this person from the future

---
class: bg-contain
background-image: url('images/future-me-moustache.jpg')

???
this person from the future

this person from the future doesn't know why you made these decisions,

and you might not remember why you made these decisions

if you can provide them a blueprint that shows why you made it, and how you make other decisions on this app, that's really helpful.

& it might turn out that this person three months from now that needs your blueprint..is you.
