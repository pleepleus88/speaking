---
template: next-pattern
background-image: url(images/platform.jpg)

# 1. "Use The Platform" Pattern

???

....the background for this pattern, 

is this - 

---
template: level-1
layout: true

# Use The Platform

---

## React is (mostly) just JavaScript.

???

This is one of the reasons we all chose React!

It would behoove us to use the delightful tools that JS gives us!

---
template: level-2
layout: false
# Use The Platform
## Array Methods

???

The first feature we get from the platform is some really helpful Array methods.

---
template: level-3
layout: true
# Use The Platform
## Array Methods

---
class: hide-footer

### Scenario 


```javascript
const kittens = [
  {
    name: 'Turtle',
    color: 'gray',
    lovesMe: false,
  },
  {
    name: 'Flower',
    color: 'black',
    lovesMe: true,
    dead: true,
  },
  {
    name: 'Potatoes',
    color: 'gray',
    lovesMe: true,
    dead: true,
  }
]
```

???

The examples we'll use for these methods are based on a scenario

---

### Old

```javascript
let myKittenNames = [];
for (let i = 0; i < kittens.length; i++) {
  const kitten = kittens[i];
  if (kitten.color === 'gray') {
    myKittenNames.push(kitten.name);
  }
}
let myFavoriteKitten;
for (let i = 0; i < myKittenNames.length; i++) {
  const kittenName = myKittenNames[i];
  if (kittenName.startsWith('P')) {
    myFavoriteKitten = kittenName;
  }
}
```

---
 
### New

```javascript
const myFavoriteKitten = kittens
  .filter(kitten => kitten.color === 'gray')
  .map(kitten => kitten.name)
  .find(kittenName => kittenName.startsWith('P'));
```

???

We could also...

Why is this better?

It's declarative code, not imperative.

There's no need to focus on the details of HOW you're mappin through the array

Just that you are.

...

You could do this by writing your own functions

You could use something like lodash or ramda to lots of these things


---

### ES2015 (IE9+)

```javascript
array.map
array.filter
array.reduce
array.every
array.some
array.find
```

???

But you can also just use JavaScript. 

These are all from the **ES2015** spec

Almost all of them are supported in **IE9+**

**Find** is the exception

If you're using modern browsers, you can use these methods.

If you're not, you can still use them (babel, later.)

---

### Why?

* They encourage immutable code

???

and immutable code is more predictable

--
* Declarative code is more readable

---
template: level-2
layout: false
# Use The Platform
## Cohesive prop validation

---
template: level-3
layout: true
# Use The Platform
## Cohesive prop validation

???

class fields

---
class: hide-footer, medium

### Old

```javascript
class Kitten extends React.Component {
  render() {
    const { name, color, lovesMe, isDead } = this.props;
    return (
      <div>
        ...
      </div>
    );
  }
}

Kitten.propTypes = {
  name: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  lovesMe: PropTypes.bool.isRequired,
  isDead: PropTypes.bool,
};

Kitten.defaultProps = {
  lovesMe: true,
  isDead: false,
};

```

???

Problem: feels like we are bolting things on 

that are really part of the original class definition.

Reduces readability because this is a specification on our class, 

but outside of the class.

---
class: hide-footer, medium

### New

```javascript
class Kitten extends React.Component {
  render() {
    const { name, color, lovesMe, isDead } = this.props;
    return (
      <div>
        ...
      </div>
    );
  }

  static defaultProps = {
    lovesMe: true,
    isDead: false,
  };

  static propTypes = {
    name: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    lovesMe: PropTypes.bool.isRequired,
    isDead: PropTypes.bool,
  };
};
```
---

### How

* Class fields
* Stage 3 proposal
* https://github.com/tc39/proposal-class-fields

---

### Why? 

* Makes our components more cohesive

---
template: level-2
layout: false
# Use The Platform
## Eliminate Constructors

---
template: level-3
layout: true
# Use The Platform
## Eliminate Constructors

???

http://egorsmirnov.me/2015/06/14/react-and-es6-part2.html

---
class: medium

### Old

```javascript
class KittenLovesMeCheckbox extends React.Component {
  constructor() {
    super();

    this.handleChange = this.handleChange.bind(this);

    this.state = {
      lovesMe: true,
    };
  }

  handleChange() {
    this.setState(
      //...
    );
  }

  render() {
    // ...
  }
}
```

???

Problem: Noise in the constructor. 

Constructors that do a lot of stuff make it easy for a dev to say "oh, we put stuff in here"

Which leads to lots of state, 

which might not be necessary

---

### New

```javascript
class KittenLovesMeCheckbox extends React.Component {
  state = {
    lovesMe: true,
  };

  handleChange = () => {
    this.setState(
      //...
    );
  }

  render() {
    // ...
  }
}
```

???

How?

We're using class fields again

This time with arrow functions

---

### Why?

* Minimizes an anti-pattern vector

---
template: level-2
layout: false
# Use The Platform
## Readable Asynchrony

---
template: level-3
layout: true
# Use The Platform
## Readable Asynchrony


---
class: medium

### Old
```
const feedKitten = dispatch => {
  callKitten()
    .then(kitten => {
      dispatch(kittenArrived(kitten));
      openFood()
        .then(food => {
          dispatch(foodOpened(food));
          return fillBowl(food);
        })
        .then(bowl => {
          dispatch(bowlFilled(bowl));
          return giveBowlToKitten(bowl, kitten);
        })
        .then(() => {
          petKitten(kitten);
        })
        .then(() => {
          dispatch(happyKitten(kitten));
        });
  });
};
```

???

problem: promises are almost as bad as callbacks for cognitive load,

especially if they aren't directly chained.

Each of those indentations is a "scope" in the code,

*and* a scope in your brain, as you read it.

https://hackernoon.com/6-reasons-why-javascripts-async-await-blows-promises-away-tutorial-c7ec10518dd9

---

### New

```javascript
const feedKitten = kitten => {
  return async dispatch => {
    const kitten = await callKitten();
    dispatch(kittenArrived(kitten));

    const food = await openFood();
    dispatch(foodOpened(food));

    const bowl = await fillBowl(food);
    dispatch(bowlFilled(bowl));

    await petKitten(kitten);
    dispatch(happyKitten(kitten));
  };
};
```

???

Modern browsers (i.e. not IE)

---

### Why?

* Reduces nesting, and therefore cognitive load 


???

---
template: level-2
layout: true
# Use The Platform
## Babel

---

### https://babeljs.io/

???

This is how you'll get access to all of these things, if you don't already.

CRA has support for all.

Babel **plugins** to allow features;

Babel preset "env" to support older browsers.
