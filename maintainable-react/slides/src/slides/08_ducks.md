
---
template: next-pattern
background-image: url(images/duck.jpg)

# 6. "Ducks" Pattern

???

This is actually the only pattern with a website, 

and also with not a crappy made-up name

...

"Suitcase" pattern applied to Redux

---
template: level-1
layout: true

# Ducks

---

```javascript
export const ADD_KITTEN = 'ADD_KITTEN';
export const DELETE_KITTEN = 'DELETE_KITTEN';
export const UPDATE_KITTEN = 'UPDATE_KITTEN';
```
.footnote[
actions/kittens.js
]

???

The typical way of arranging your redux code looks like this.

Again, shirts with shirts, etc.

---
class: no-footer

```javascript
export function addKitten(kitten) {
  return {
    type: ADD_KITTEN,
    kitten
  }
};

export function deleteKitten(kitten) {
  return {
    type: DELETE_KITTEN,
    kitten
  }
};

export function updateKitten(kitten) {
  // ...
};
```
.footnote[
action-creators/kittens.js
]

---

```javascript
export default function reducer(state = initialState, action) {
  switch (action.type){
    case ADD_KITTEN:
      // ...
    case DELETE_KITTEN:
      // ...
    case UPDATE_KITTEN:
      // ...
  }
}
```
.footnote[
reducers/kittens.js
]

???

...

The Ducks pattern applies the suitcase lifehack principle

with a few operating rules.

---

> ### A module...
> #### MUST export default a function called reducer()
> #### MUST export its action creators as functions
> #### MUST have action types in the form module/reducer/ACTION_TYPE

.footnote[
[https://github.com/erikras/ducks-modular-redux](https://github.com/erikras/ducks-modular-redux)
]

---

```javascript
// ...actions
const ADD = 'kittens/ADD';
const DELETE = 'kittens/DELETE';
const UPDATE = 'kittens/UPDATE';

// ...action-creators

// ...reducer
```
.footnote[
kittens/state.js
]

???

And so your code ends up looking like this:

...

actions aren't exported (usually), because they are all contained within this file.

---
class: no-footer

```javascript
// ...actions

// ...action-creators
export function addKitten(kitten) {
  return {
    type: ADD,
    kitten
  }
};

export function deleteKitten(kitten) {
  return {
    type: DELETE,
    kitten
  }
};

export function updateKitten(kitten) {
  // ...
};

// ...reducer
```
.footnote[
kittens/state.js
]

???

action-creators are exported as named

---

```javascript
// ...actions

// ...action-creators

// ...reducer
export default function reducer(state = initialState, action) {
  switch (action.type){
    case ADD:
      // ...
    case DELETE:
      // ...
    case UPDATE:
      // ...
  }
}
```
.footnote[
kittens/state.js
]

???

reducer is exported as default

---
template: level-2

# Ducks
## Why?

* Less time looking for the different pieces that are related to a reducer

--
* Put code together that changes together
