Intro

    Hi! I'm Steve. I want to talk about making your code more maintainable.

    What does it mean for code to be maintainable?

    Something about tidying up?

    I have a handful of simple patterns & practices that can help tidy up your code.


// can't really use this. Livable code means somewhere between staged & shitty.
//  I'm striving more for the staged.
    I can't take credit for the phrase "livable code." That's Sarah Mei.
        https://twitter.com/i/moments/843392359903649792
    But what she means by "livable code" is this - if you think about your codebase like a house...
        Does it look like this? (shit everywhere)
        Like this? (slightly less shit)
        Or like this? (beautiful and pristine)

        When you think about making changes to your code,
            do you feel like you have to look all over the house?

            Or do you feel like you can find everything you need in one place?

"Use the platform" pattern
    pattern: Super mario bros, donkey kong, double dragon!
    https://www.touchtapplay.com/wp-content/uploads/2013/12/DDT.png

    React is (mostly) just JS. 
        Use the delightful tools that JS gives us!

    Clean up .propTypes & .defaultProps ()
        http://egorsmirnov.me/2015/06/14/react-and-es6-part2.html
        
        Old: ClassName.proptypes = {}; Classname.defaultProps = {};
        
        Problem: feels like we are bolting things on that are really part of the original class definition. Reduces readability because this is a specification on our class, but outside of the class.
        
        how: Class fields
            stage 3 proposal
            https://github.com/tc39/proposal-class-fields
        
        New: static propTypes = {}; static defaultProps = {};

    Clean up state initialization
        http://egorsmirnov.me/2015/06/14/react-and-es6-part2.html

        old: constructor(){this.state = ...}

        Problem: Noise in the constructor. 
        Constructors that do a lot of stuff make it easy for a dev to say "oh, we put stuff in here"
            Which leads to lots of state, which might not be necessary

        how: Class fiels (again)

        New: state = {};
        
    Async/await
        old: promises
        ```
        return function (dispatch) {
            return fetchSecretSauce().then(
                sauce => dispatch(makeASandwich(forPerson, sauce)),
                error => dispatch(apologize('The Sandwich Shop', forPerson, error))
            );
        };
        ```

        problem: promises are almost as bad as callbacks for context-switching, if they aren't directly chained.
        
        new: async/await
            https://tc39.github.io/ecmascript-asyncawait/
            stage 3???

        ```
        return function (dispatch) {
            try {
                const sauce = await fetchSecretSauce();
                dispatch(makeASandwich);
            } catch (error) {
                dispatch(apologize....)
            }
        };
        ```

        I might want an example without errors to better prove the simplicity?

"Poorly translated puzzleball" pattern
    pattern: puzzleball 
    The namesake for this pattern: 
        13 years ago, at my bachelor party
        Hung over after camping in New Glarus Woods state park
        Sandeep put 25c into a kids vending machine
        Received this: (http://www.engrish.com//wp-content/uploads/2008/08/puzzle-ball1.jpg)

        (describe it)
        (zoom in)
            Let's decompose and enjoy assembling

    Break your components into tiny pieces
        And assemble them

        Thinking in react - the best page in the docs
            https://reactjs.org/docs/thinking-in-react.html
            Build your components without any actual state
                So you can find the proper boundaries
        
        Why?
            Smaller things are easier to test
                Edges are easier to find
            Smaller things are easier to read
                Like an outline. 
                Less cognitive load/context-switching
        
        So break your components down.
        Break your helper functions down into smaller pieces. 
        Break your reducers down, and use combineReducers.


        When you think they are small
            Make them even smaller

        // show code example of small things

        In the end: components are not about reuse
            They are about isolation.

"Procrastination" pattern
    pattern: ???

    Comes from an approach I like to take while TDD'ing

    Breadth-first instead of depth-first
        drawing: tree of decisions
        breadth-first: push off the complicated stuff; attack the happy paths
        depth-first: attack the complicated stuff/edge cases

    Why?
        Leads to a manageable test suite
            Complicated decisions are tested in isolation
                And it's easier for me to identify where they are
            With depth-first, I have a harder time identifying boundaries
                Complicated decisions tend to bleed into happy paths

"Separation of presentation & state" pattern
    pattern: ???

    Embrace the separation of complicated state management & presentation

    Problem Code: Stateful + presentational component

    Solution Code: Stateful component, presentational component

    Why?
        Helps me separate code, obey SRP & SOC
    
    Caveat
        Note the word "complicated"
        This is a pragmatic pattern.
        It's not a hard rule.
        If you have a component that doesn't do a lot - leave it as one.
        Code example: toggle component.

"Pragmatic state management" pattern
    pattern: ??? a scale???

    https://redux.js.org/faq/general#general-when-to-use

        In general, use Redux when you have reasonable amounts of data changing over time, you need a single source of truth, and you find that approaches like keeping everything in a top-level React component's state are no longer sufficient.

    Define: component state
    Example: setState

    Define: application state
    Example: redux

    https://spin.atomicobject.com/2017/06/07/react-state-vs-redux-state/
    https://medium.com/@dan_abramov/you-might-not-need-redux-be46360cf367

    When to use Redux/application state?
        * The state is shared across the entire app
            ex: current user
        * The state is shared between distant components
            ex: ???
    
    When not to use Redux/application state?
        * The state is contained within a component/several components
            ex: toggle component

    State mgmt recommendations from unstated:
        https://github.com/jamiebuilds/unstated#faq

    Why?
        Unnecessary redux leads to boilerplate code
        Simple components are easier to understand when you can see the state
        Redux actions imply distant side-effects

"Suitcase lifehack" pattern
    pattern: suitcase

    Background: packing a suitcase.
        I've always packed like items together.
            shirts with shirts, pants with pants
        Overhead: convert "what I need to wear each day" into items;
            pack items;
            unpack into drawers;
            find all those items every morning.

        One day I realized I didn't have to do this.
            Determine what I need to wear each day;
            pack days together;
            grab a day of clothes every morning.

    Code: 
        we put shirts + shirts; knives + knives if it's a kitchen

        when we get a bug, it's not like it's all the shirts that are broken

        we need to find the "authentication" shirt, "authentication" pants, "auth" socks

    So what if we organized our code like we (should) pack our suitcase?

    All the auth things together; all the dashboard things together; etc

    Why?
        Less time looking for the different pieces that are related to a feature

        Put code together that changes together

"Ducks" pattern
    pattern: ducks

    Background: last two React meetups

    "Suitcase lifehack" pattern applied to Redux

    old: 
        reducers/feature/index.js
        actions/feature/index.js
        action-creators/feature/index.js

        (show code)
    new: 
        reducers/feature/index.js
            exports reducer and action-creators
                actions aren't needed (usually), because they are all contained in this file.

        https://github.com/erikras/ducks-modular-redux
        Rules:        
            A module...
                MUST export default a function called reducer()
                MUST export its action creators as functions
                MUST have action types in the form npm-module-or-app/reducer/ACTION_TYPE

        (show code)

    Why?
        Less time looking for the different pieces that are related to a reducer
        
        Put code together that changes together

"Pacifist" pattern
    pattern: person dying on a hill

    Background: How many people have had an argument over someone about code format?
        Had someone on your team commit format changes?
        Have BEEN the person that committed format changes?

    Poll: 
        tabs vs spaces
        semicolons vs not semicolons

    Solution:
        WHO F@^@#$ING CARES!
        
        Stop caring about code formatting & automate it!

    Prettier
        An opinionated code formatter
            sensible opinions
            with plugins for all major code editors
        You can write garbage code, and let prettier clean it up for you
            Show it!
    
    Opinions
        I know what you're thinking
        I heard the word "opinion". 
            I have opinions. What if Prettier doesn't match my opinions?
    
    WHO F@#$#@#@$ING CARES?????!!!
    WHO F@#$#@#@$ING CARES?????!!!; <-ftfy

    If you must: Eslint + eslint-prettier + prettier

    Why?
        Ends senseless arguments about how your code should be formatted.
            Prettier is opinionated, but the opinions are sensible
        
        Never worry about formatting your code again!

    So now you can worry about things that matter.
        Where do the socks go?
        What color should the bikeshed be?
            monopoly guy: $$$$Green$$$$
            neckbeard: aliceblue
            Rachel: #db011c
            J: Pantone 23432234234
        ???

