---
layout: false
template: our-journey
class: stage-3
journey-stage: 3
name: journey-3

???

We've got tools

Know how to use them

Good practices?

---
template: level-1

# Guidance
## Better Components

???

It starts with writing better components.

We can write them in a way

that makes writing tests easier.

---
template: level-2
layout: true

# Guidance
## Better Components

---

### Extract Complicated Things & Business Logic

???

Test them in isolation!

This leaves your components mostly dumb

And way easier to test

---

### Decompose Components By Responsibility

???

Small components do fewer things

And are therefore easier to test

but you don't want to make them so small that they don't have meaning

break up by responsibility

allows you to shallow render components for tests, instead of deep

list+list item example

---
template: level-1

# Guidance
## Better Tests

???

With better components, we can focus on writing better tests.

---
template: level-2
layout: true
name: better-tests

# Guidance
## Better Tests

---

### Embrace describe()

???

Describe **functions** under test

And **scenarios** that require multiple tests within them.

Keep tests **organized** & easy to find things

Don't go more than 2 or 3 describes deep

or it means your function is doing too much

...

---
template: full-screen
background-image: url(images/junk-drawer.jpg)
class: inverse

???

But don't be **monsters**, and write all of your tests outside of describe blocks. 

(story about just adding tests wherever)

---

### Minimize Setup

???

When you have common setup, 

extract it into composable functions

Tests are easier to read when we can focus on what makes them unique.

Signal:noise

---
template: level-3
layout: true

# Guidance
## Better Tests
### Minimize Setup

---

#### Chekhov's Gun

---


>Remove everything that has no relevance to the story. If you say in the first chapter that there is a rifle hanging on the wall, in the second or third chapter it absolutely must go off. If it's not going to be fired, it shouldn't be hanging there.

.footnote[
Anton Chekhov
]

???

same rule applies to setup.

If the test isn't using it...don't set it up.

allows reader to focus on *what makes this test unique*

---
class: no-footer

```javascript
describe('validateBeer', () => {
    it('returns invalid for beers with no abv', () => {
        const beer = {
            id: 87983,
            brewery: {
                id: 12332,
                name: 'Central Waters',
                location: 'Amherst, WI',
                overallRating: 5
            },
            name: 'Mudpuppy Porter',
            beerStyle: 'Porter',
            abv: undefined
        },

        const result = validateBeer(beer);

        expect(result).toEqual(false);
    });
})
```
.footnote[
validate-beer.spec.js
]

---
class: no-footer

.dim-1.dim-2.dim-3.dim-4.dim-5.dim-6.dim-7.dim-8.dim-9.dim-10.dim-11.dim-12.dim-14.dim-15.dim-16.dim-17.dim-18.dim-19.dim-20[
```javascript
describe('validateBeer', () => {
    it('returns invalid for beers with no abv', () => {
        const beer = {
            id: 87983,
            brewery: {
                id: 12332,
                name: 'Central Waters',
                location: 'Amherst, WI',
                overallRating: 5
            },
            name: 'Mudpuppy Porter',
            beerStyle: 'Porter',
            abv: undefined     // <---------
        },

        const result = validateBeer(beer);

        expect(result).toEqual(false);
    });
})
```
]
.footnote[
validate-beer.spec.js
]

---

```javascript
describe("validateBeer", () => {
    it("returns invalid for beers with no abv (less setup)", () => {
        const beer = makeMeABeer();
        beer.abv = undefined;

        const result = validateBeer(beer);

        expect(result).toEqual(false);
    });
});
```

.footnote[
validate-beer.spec.js
]

???

makeMeABeer would be another function in this test file

that just makes a beer with sensible defaults

And then you can override what you want

---
template: better-tests
layout: true

---

### Keep Snapshots Small

???

Snapshotting big things is cumbersome

hard to track what changed

...

and along that note...

---

### Use [enzyme-to-json ](https://github.com/adriantoine/enzyme-to-json)

---
template: level-3
layout: false

# Guidance
## Better Tests
### enzyme-to-json

```html
<!-- shallow -->
<article
  className="beer"
>
  <h1>
    Mudpuppy Porter
  </h1>
  <Brewery
    brewery="Central Waters"
  />
</article>
```

???

otherwise you'll get noise from the enzyme wrapper

---
template: better-tests

### Extend Jest with .toLookLike() sparingly

---
template: better-tests

### Consider Your Audience

.content-image.context[
![Stefan](images/context-stefan.jpg)
]

---
template: title

# Optimize for fixing tests
## instead of writing tests

???

Write in a way that when they fail in several months,

you can tell immediately why the failed

and how to fix them.

