---
layout: false
template: cover-art

# THANK YOU!

.about.right[
## Steven Hicks
### [@pepopowitz](http://twitter.com/pepopowitz)  <i class="el el-twitter"></i>
### steven.j.hicks@gmail.com  <i class="el el-envelope"></i>
### [stevenhicks.me/react-testing](https://stevenhicks.me/react-testing)  <i class="el el-globe"></i>
]

???

I want to thank you for your time

I really appreciate it.

Survey

Questions after

...

---
template: full-screen
background-image: url(images/that2019.jpg)

???

I'll be here next year.

I hope you will too!

Thank you!

---
template: level-1
class: double-wide, resources

# Resources

* [This talk](https://stevenhicks.me/react-testing)
* [Code Samples](https://github.com/pepopowitz/unit-testing-your-react-app)
* [Jest](https://facebook.github.io/jest/)
* [Enzyme](http://airbnb.io/enzyme)
* [enzyme-to-json](https://github.com/adriantoine/enzyme-to-json)
* [Additional matchers (assertions)](https://github.com/blainekasten/enzyme-matchers)
* [Pain points of jest/enzyme](https://medium.com/@Tetheta/lessons-learned-testing-react-redux-apps-with-jest-and-enzyme-eb581d6d167b)

---
template: level-1
class: double-wide, resources

# Images

* [Loic Djim](https://unsplash.com/photos/Sq7WPOjHGDs) - Title
* [mgstanton](https://www.flickr.com/photos/marirn/6131270109/) - Junk Drawer
* The rest - Me



