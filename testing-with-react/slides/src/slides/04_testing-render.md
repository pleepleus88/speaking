---
template: title

# Testing Component Render

???

To demonstrate how you'd test component render,

I've got a scenario of components for you.

---
template: level-3
layout: true
name: testing-render
class: code

# Testing React
## Component Render


---
template: testing-render
layout: true
name: our-scenario

### Our Scenario

---

.content-image[
![Snapshot](images/beer-tree-1.jpg)
]

---

.content-image[
![Snapshot](images/beer-tree-2.jpg)
]

---

.content-image[
![Snapshot](images/beer-tree-3.jpg)
]

---

.content-image[
![Snapshot](images/beer-tree-4.jpg)
]

???

render, but to the real actual DOM

as close to the real browser as you'll get

---

```javascript
import Brewery from './Brewery';
import BeerStyle from './BeerStyle';

export default function Beer({ beer }) {
    return (
        <article className="beer">
            <h1>
                {beer.name}
            </h1>

            <Brewery brewery={beer.brewery} />
            <BeerStyle beerStyle={beer.beerStyle} />
        </article>
    );
}
  
```
.footnote[
Beer.jsx
]

???

Note: className="beer"

---

```javascript
export default function Brewery({ brewery }) {
    return (
        <h2 className="brewery">
            {brewery}
        </h2>
    );
}
```

.footnote[
Brewery.jsx
]

???

Note: className="brewery"

---
template: testing-render
layout: true

### shallow()

---

--

```javascript
import { shallow } from 'enzyme';

it('renders a beer', () => {
   
    const beer = {
        name: 'Mudpuppy Porter'
    };

    const wrapper = shallow(<Beer beer={beer} />);
   
    expect(wrapper.find('.beer')).toHaveLength(1);

    expect(
        wrapper.contains(<h1>Mudpuppy Porter</h1>)
    ).toBe(true);

});
```

.footnote[
Beer.spec.jsx
]

---

```javascript
it('doesn`t shallow render brewery', () => {
   
    const beer = {
        name: 'Mudpuppy Porter',
        brewery: 'Central Waters'
    };

    const wrapper = shallow(<Beer beer={beer} />);

    expect(wrapper.find('.brewery')).toHaveLength(0);

    expect(
        wrapper.contains(<h2 className="brewery">Central Waters</h2>)
    ).toBe(false);

});
```

.footnote[
Beer.spec.jsx
]

???

on the other hand...

I wouldn't write a test like this for my code

But this illustrates what shallow does.

---
template: testing-render
layout: false

### render()

???

renders the whole tree

"integration testing" components

--

```javascript
import { render } from 'enzyme';

it('renders brewery', () => {

    const beer = {
        name: 'Mudpuppy Porter',
        brewery: 'Central Waters'
    };

    const wrapper = render(<Beer beer={beer} />);

    expect(wrapper.find('.brewery')).toHaveLength(1);

});
```

.footnote[
Beer.spec.jsx
]

???

Note we don't have access to wrapper.contains

It uses a library called Cheerio

provides some traversal, but it's not as nice to navigate.

---
template: testing-render
layout: false

### mount()

???

almost identical

--

```javascript
import { mount } from 'enzyme';

it('mounts brewery', () => {

    const beer = {
        name: 'Mudpuppy Porter',
        brewery: 'Central Waters'
    };

    const wrapper = mount(<Beer beer={beer} />);

    expect(wrapper.find('.brewery')).toHaveLength(1);

    expect(
        wrapper.contains(<h2 className="brewery">Central Waters</h2>)
    ).toBe(true);

});
```

.footnote[
Beer.spec.jsx
]

???

deep renders

but it renders into the full DOM - so it's a better representation of what will actually happen

AND I get the ability to do .contains

---
template: title

# Snapshot Tests

???

There's **another method** to testing render.

Comes from **Jest**

Takes a snapshot of the rendered component

---
layout: true
template: testing-render

### Snapshot Tests

---

.content-image.snapshots[
![Snapshot](images/snapshot-1.jpg)
]

???

The first time you run the test, 

it'll put the rendered contents of your component in a file.

Every time after, it'll **regenerate** the snapshot

and **compare** it to the one in the file

---

.content-image.snapshots[
![Snapshot passing](images/snapshot-passing.jpg)
]
???

If it's the same, the test passes.

---

.content-image.snapshots[
![Snapshot failing](images/snapshot-failing.jpg)
]

???

If it's different, the test fails.

It might have failed for good reason

In which case you go fix the issue

But it might just be a harmless change.

---

.content-image.snapshots[
![Snapshot updated](images/snapshot-updated.jpg)
]

???

In this case, you can tell Jest to **update the snapshot.**

And you're back to a passing test.

Jest relies on you to say

**"yes, it's different, but it's still good."**

---
class: full-width, no-footer

![Snapshot error message](images/snapshot-error.jpg)

???

* Error diff

* Option to update

---
class: code

```javascript
it('snapshots a beer with shallow', () => {

    const beer = {
        name: 'Mudpuppy Porter',
        brewery: 'Central Waters'
    };

    const wrapper = shallow(<Beer beer={beer} />);

    expect(wrapper).toMatchSnapshot();

});
```

.footnote[
Beer.spec.jsx
]

???

Here's an example of a snapshot test

...

and this will generate a snapshot that looks like this...

---

class: code

```html
<!-- shallow -->
<article
  className="beer"
>
  <h1>
    Mudpuppy Porter
  </h1>
  <Brewery
    brewery="Central Waters"
  />
</article>
```

.footnote[
Beer.spec.jsx.snap
]

???

of particular note is the <Brewery> component - a placeholder, really, for a child component.

...

if I use render, instead of shallow, the snapshot will look like this

---
class: code

.dim-1.dim-2.dim-3.dim-4.dim-5.dim-6.dim-7.dim-13[
```html
<!-- render -->
<article
  class="beer"
>
  <h1>
    Mudpuppy Porter
  </h1>
  <h2
    class="brewery"
  >
    Central Waters
  </h2>
</article>
```
]

.footnote[
Beer.spec.jsx.snap
]

???

you can see that now I have the rendered markup from the brewery, not just a component placeholder.

...

and if I use mount...

---
class: code, no-footer

.dim-1.dim-2.dim-3.dim-4.dim-5.dim-6.dim-7.dim-8.dim-9.dim-10.dim-20.dim-21.dim-22[
```html
<!-- mount -->
<Beer
  beer={...}
>
  <article
    className="beer"
  >
    <h1>
      Mudpuppy Porter
    </h1>
    <Brewery
      brewery="Central Waters"
    >
      <h2
        className="brewery"
      >
        Central Waters
      </h2>
    </Brewery>
  </article>
</Beer>
```
]

---
layout: false
template: testing-render
class: list

### Pros of Snapshot Tests

--

#### Simple to write

???

render component

then expect something **toMatchSnapshot()**.

...

--

#### Simple to fix

???

When one breaks, you just hit 'u' and it fixes the test

maybe TOO easy...

...

--

#### Speeds up development

???

I waver on this, because there are cons.

---
layout: true
template: testing-render
class: list

### Cons of Snapshot Tests

---

#### Break easily

???

They break really easily. 

Literally anything in your component changes

and you've got a broken test.

...

but the biggest downside...

--

#### Optimized for **writing** tests over **fixing** tests

???

Snapshot tests make it easy to WRITE tests

But **they make it harder to fix broken tests**.

---

#### Who do we write tests for?

???

Consider.

--

.content-image.context[
![Steve](images/context-steve.jpg)
]

???

Is it you, right now?

In some sense, if you're doing TDD, yes.

But they're just as useful, if not more, for...

---
#### Who do we write tests for?


.content-image.context[
![Stefan](images/context-stefan.jpg)
]

???

Your coworker with the sweet moustache

3 mos from now

No context

...

Snapshot tests are really cool - 

but I think they **make fixing tests harder**, because they

**require** you to have **more knowledge** about the component being rendered

To know if something is breaking or not.



---
template: title

# Html-looks-like

.footnote[
[github.com/staltz/html-looks-like](https://github.com/staltz/html-looks-like)
]

???

A third method for testing render

Uses a library called html-looks-like

...

Html looks like allows you to specify a template

that your markup should look like


---
layout: true
template: testing-render

### Html-looks-like

---
class: code

```javascript
it('looks like a beer', () => {
  const beer = {
    name: 'Mudpuppy Porter',
    brewery: 'Central Waters',
  };

  const wrapper = render(<Beer beer={beer} />);

  expect(wrapper).toLookLike(`
    <article class="beer">
      <h1>Mudpuppy Porter</h1>
      {{ brewery placeholder }}
    </article>
  `);
});
```

.footnote[
Beer.spec.jsx
]

???

And we can write a custom Jest matcher that uses html-looks-like

to allows to expect something toLookLike a string of markup.

...

You can't tell by these slides, because the syntax highlighting is too good - 

but that's actually a string that I'm comparing it to

Which kind of stinks, and can be hard to maintain. 

---
class: code

```javascript
it('looks like a beer', () => {
  const beer = {
    name: 'Mudpuppy Porter',
    brewery: 'Central Waters',
  };

  const wrapper = render(<Beer beer={beer} />);

  expect(wrapper).toLookLike(
    <article className="beer">
      <h1>Mudpuppy Porter</h1>
      <Placeholder for="brewery" />
    </article>
  );
});
```

.footnote[
Beer.spec.jsx
]

???

But it doesn't take much to write a toLookLike matcher

that takes JSX

and a Placeholder component where we don't care

And now this test is **easier to read & maintain**.

---
layout: false
template: testing-render
class: list

### Pros of Html-looks-like

--

#### Clear tests

???

The only thing in our assertion is the markup we care about for the sake of the test

---
layout: true
template: testing-render
class: list

### Cons of Html-looks-like

---

#### It's very heavy

???

And the way you extend Jest to use a new matcher, 

your extension is added at the beginning of every single test.

So it slows down your tests, noticeably.

---
template: level-3

# Testing React
## Component Render
### Guidance

#### Find the right balance

???

But the key -

Find the balance of shallow vs deep vs snapshots vs html-looks-like

Try them all.

Me: move as much out of my components as possible

Run simple tests of components, usually shallow rendered, to make sure things are shown/hidden correctly

Using Enzyme if the assertion is simple to write

Html-looks-like if it is not
