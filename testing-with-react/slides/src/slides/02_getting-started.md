---
layout: false
template: our-journey
class: stage-0

???

This is what our journey looks like today

Arm you with tools

Show you how to use those tools

Tell you when to use a hammer, when to use a wrench

...

Price is right?

---
layout: false
template: our-journey
class: stage-1
journey-stage: 1
name: journey-1

---
template: level-2
layout: true
name: getting-started

# Getting Started

---
template: title

# Jest
## http://facebook.github.io/jest

???

The test framework I'm interested in showing today is Jest

Can be used for any JS testing, not just React

---

## Why Jest?

???

several reasons

---
layout: true
template: getting-started

## Why Jest?

---

### Easy to set up

???

If you're using create-react-app, it's already set up!

If not, it's probably less than three steps to set up.

---

### Zero configuration

???

If you **follow the conventions** for naming your test files,

Jest will just find them.

---

### Interactive watch mode

???

By default, 

runs **only tests affected by changes** since your most recent commit.

So as I'm making changes, the only tests running are the ones I'm working on.

(Great for TDD)

Other options:

1. run all tests

2. filter tests by name or file path

---

### Helpful error messages

???

This is one of my favorite things happening in dev right now

Error messages that tell you **what you did wrong**

instead of just **you did something wrong**

...

These help you **pinpoint** what's wrong with your test

--

.content-image.friendly-error[
![Friendly error message from Jest](images/friendly-error.jpg)
]
---

### Snapshot testing

???

Allows you to compare **what a component looks like** now

to **what it looked like before** you made changes

We'll look more later.

But this is a huge feature for a lot of people doing React dev.

---
layout: false
template: title

# Installing Jest

???

If you're using create-react-app, it's already set up!

---
layout: true
template: getting-started

---

## Installing Jest

### `npm install --save-dev jest`

???

For the most part...it's this simple.

There might be a couple extra steps depending on your setup, 

--

.footnote[
### http://facebook.github.io/jest
]

???

but the docs are thorough and helpful.

---
class: list

## Jest Conventions

--

### \_\_tests\_\_ folders

--

### *.spec.js

--

### *.test.js

---
template: title

# Jest API

---
template: getting-started
layout: true
name: jest-api-2

## Jest API

---

### describe()

???

describe is how you define a **set of tests**

---
template: level-3
layout: true
name: jest-api-3
class: code

# Getting Started
## Jest API

---

### describe()

```
describe('getLocalBeers', () => {
    // tests for getLocalBeers go here
});
```
.footnote[
get-local-beers.spec.js
]

???

It's kind of like **namespacing** your tests.

---

### describe()

```
describe('getLocalBeers', () => {
    describe('when there are lots of beers', () => {
        // tests for getLocalBeers/lots of beers go here
    });
});
```
.footnote[
get-local-beers.spec.js
]

???

You can nest describes

Great for scenarios

that need multiple tests

---
template: jest-api-2

### it() or test()

???

use it or test to define an individual test.

---
template: jest-api-3

### it() or test()

.dim-1.dim-7[
```
describe('getLocalBeers', () => {

    it('calls the api', () => {
        // your test goes here
    });

});
```
]
.footnote[
get-local-beers.spec.js
]

???

give it a name

and a function to execute.

---
template: jest-api-2

### Assertions

???

and then you'll want to make assertions.

---
template: jest-api-3

### Assertions

```javascript
expect(result.location).toEqual('Amherst, WI');

expect(result.name).not.toEqual('High Life');

expect(result.beers).toHaveLength(3);

expect(result.beers).toEqual(
    expect.arrayContaining([{ name: 'Mudpuppy Porter' }])
);
```

???

expect()

matchers

(read em)

they aren't much different than other JS test frameworks

and if you're new to JS testing, they read very fluently.

---
template: getting-started
layout: false
class: title-footnote

## Converting To Jest

### Jest Codemods

.footnote[[github.com/skovhus/jest-codemods](https://github.com/skovhus/jest-codemods)]

???

If you're convinced...

Jest Codemods will help you convert

It's not trivial - there might still be things to do

But this will help you get started
