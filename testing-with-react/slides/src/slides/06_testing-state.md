---
template: title

# Testing Component State

???

Now you might think there is a third thing in your react app you want to test

In this case, I'm really talking about components that are using setState to store component-level state.

---
template: level-2
layout: true
name: testing-state
class: code

# Testing React
## Component State
---

--

### Usually covered by interaction tests


???

Think back to our collapsible panel example

We don't care so much about the fact that it's setting state

What we care about is that when we click this button on a collapsible panel,

it expands the content.

And we can accomplish that test by .simulating a click

And then verifying that an element appears.

---

### Testing state is testing implementation

???

You can do it...but I advise against it.
