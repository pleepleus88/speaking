---
layout: false
template: our-journey
class: stage-2
journey-stage: 2
name: journey-2

???

How do we test our react app?

---
template: level-1

# Testing React

## Business Logic

???

One of the most important things to test

Also one of the easiest

---
template: level-2
class: code, no-footer

# Testing React
## Business Logic


```javascript
it('filters out beers that aren`t porters', () => {
    const beers = [
        {
            brewery: 'Central Waters',
            name: 'Mudpuppy Porter',
            beerStyle: 'Porter'
        },
        {
            brewery: 'New Glarus',
            name: 'Spotted Cow',
            beerStyle: 'Farmhouse'
        }
    ];

    const result = filterBeers(beers);

    expect(result).toHaveLength(1);
    expect(result[0].name).toEqual('Mudpuppy Porter');
});
```

---
template: level-1

# Testing React

## Components

???

And this moment is probably what you're **most looking forward to** in this talk

...

---
template: level-2
class: code, hide-footer

# Testing React
## Components
### Are you sure it can't be extracted?

???

Except I'm going to make you **wait** even longer.

Because I want you to ask yourself - does this logic I'm about to test need to live in this component?

Or can I **extract it to a function**, and test that function in isolation?

---
template: level-2
class: code

# Testing React

## Extracted Logic

```javascript
it('maps api beers to ui beers', () => {
    const apiBeer = {
        beerName: 'Mudpuppy Porter',
        beerId: '123456'
    };

    const expectedUiBeer = {
        name: 'Mudpuppy Porter',
        id: '123456'
    };

    const result = mapApiBeerToUiBeer(apiBeer);

    expect(result).toEqual(expectedUiBeer);
});
```

.footnote[
map-beers.spec.js
]
???

Especially useful with **mapping functions**

reduce

filter

changing shape

---
template: level-2
class: list

# Testing React
## Components

???

And all of the things we want to test can be

**categorized** as one of **two** things.

...

--

### Render

???

Does it render correctly?

...

--

### Interactions

???

Does it interact properly?

"when I click this button, this div shows up"

...

and for both those things, we'll want to pull in one more tool.

---
template: level-2

# Testing React
## Components


### Enzyme

---
template: level-3
# Testing React
## Components
### Enzyme

> Enzyme is a Javascript testing utility for React that makes it easier to assert, manipulate, and traverse your React Components' output

.footnote[
http://airbnb.io/enzyme/
]

???

allows you to render your component into memory

execute tests against it

---
template: level-3
class: list

# Testing React
## Components
### Why Enzyme?

--

#### Intuitive API

???

Similar to jQuery's api for finding elements & interacting

...

--

#### Shallow or deep rendering

???

It offers a variety of options for rendering - 

Which means you can test components how you want to test them - 

in full isolation, or in full integration.

---
template: level-3
class: list

# Testing React
## Components
### Installing Enzyme

#### http://airbnb.io/enzyme/docs/installation/

???

To install Enzyme....head to the docs.

There are a couple of important details - 

installing an adapter

that you don't want to miss

