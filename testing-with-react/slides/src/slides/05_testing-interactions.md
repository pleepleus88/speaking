---
template: title

# Testing Component Interactions

???

The key to testing interactions is ...

---
template: level-3
layout: true
name: testing-interactions-3
class: code

# Testing React
## Component Interactions


---

### .simulate()
???

Enzyme's simulate function.

--


```javascript
it('simulates a click event', () => {

    //arrange
    const wrapper = shallow(...);

    //act
    wrapper.find('button').simulate('click', parameters);

    //assert
    // ...

});
```

???

.simulate lets you simulate an event on an element.


So it's easy to test things like

**"when the user clicks the button, this other element gets rendered."**

---
layout: true
template: testing-interactions-3

### Our Scenario

---

--

```html
<CollapsePanel>
    <span>This is the content that shows up when expanded.</span>
</CollapsePanel>
```

???

Example of usage

---

```javascript
export default class CollapsePanel extends React.Component {
    render() {
        return (
            <div className="collapse-panel">
                <h1>
                    <a
                        onClick={this.toggleCollapsed}
                        className="collapse-panel-toggle"
                    >
                        {this.props.title}
                    </a>
                </h1>
                {this.renderChildren()}
            </div>
        );
    }
    //...
}
```

.footnote[
CollapsePanel.jsx
]

???

The component might look like this

---

.dim-1.dim-2.dim-15.dim-16.dim-17[
```javascript
export default class CollapsePanel extends React.Component {
    render() { ... }

    renderChildren() {
        if (this.state.isCollapsed) {
            return null;
        }

        return (
            <div className="collapse-panel-content">
                {this.props.children}
            </div>
        );
    }

    //...
}
```
]

.footnote[
CollapsePanel.jsx
]

???

The renderChildren function looks like this

---


.dim-1.dim-2.dim-3.dim-4.dim-5.dim-15[
```javascript
export default class CollapsePanel extends React.Component {
    render() { ... }

    renderChildren() { ... }

    state = {
        isCollapsed: true
    };

    toggleCollapsed = () => {
        this.setState(prevState => {
            return { isCollapsed: !prevState.isCollapsed };
        });
    };
}
```
]

.footnote[
CollapsePanel.jsx
]

???

And state mgmt like this

---
template: level-2
layout: true
name: testing-interactions-2
class: code

# Testing React
## Component Interactions

---

```javascript
it('is collapsed by default', () => {

    const wrapper = shallow(
        <CollapsePanel>
            <span>Some contents here</span>
        </CollapsePanel>
    );

    expect(wrapper.find('.collapse-panel')).toHaveLength(1);
    expect(wrapper.find('.collapse-panel-content')).toHaveLength(0);

});
```

.footnote[
CollapsePanel.spec.jsx
]

???

We might write one test to **verify the default state**

---

```javascript
it('uncollapses after clicking', () => {

    const wrapper = shallow(
        <CollapsePanel>
            <span>Some contents here</span>
        </CollapsePanel>
    );

    wrapper.find('.collapse-panel-toggle').simulate('click');

    expect(wrapper.find('.collapse-panel-content')).toHaveLength(1);

});
```
.footnote[
CollapsePanel.spec.jsx
]

???

And another test to **verify the expanded state.**
