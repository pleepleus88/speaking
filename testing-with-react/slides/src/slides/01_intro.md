template: cover-art

# Unit Testing Your React App

.about.right[
## Steven Hicks
### [@pepopowitz](http://twitter.com/pepopowitz)  <i class="el el-twitter"></i>
### steven.j.hicks@gmail.com  <i class="el el-envelope"></i>
### [stevenhicks.me/react-testing](https://stevenhicks.me/react-testing)  <i class="el el-globe"></i>
]

???

**Contact Info**
**Thanks:**

* you!


timekillers:

* STICKERS!
* advice for new people
* favorite talks
* favorite speakers
* stand up & stretch
* high fives

checklist: 
* slides sync'ed
* audio works
* recording audio 
* recording video
* **CLAP!**






* favorite kinds of talks
  * technical
  * experiential
  * soft skills
* where from?
* poll about intro


---
template: full-screen
background-image: url(images/thatorganizers.jpg)
class: no-footer

???

organizers!

---
template: full-screen
background-image: url(images/thatsponsors.jpg)
class: no-footer

???

sponsors!

---
layout: true
template: level-1
name: bio

# Steven Hicks
---

.profile.bio[
![Me](images/steve-by-olivia-square.jpg)
![Me](images/steve-by-lila-square.jpg)
]

???

I am standing right in front of you so you can tell what I look like

But on the left is what my 7 year old daughter Olivia thinks I look like

On the right is what my 10 year old daughter Lila thinks I look like

and if you think this is just an excuse to talk about my kids

and show you their artwork

You are correct.

---

## JavaScript Engineer

???

for a company in MKE called NM

---
layout: false
template: full-screen
class: inverse
background-image: url(images/nm-view.jpg)

<svg class="nm-logo"><symbol id="svg$he" viewBox="0 0 260 32"><path fill=#FFF d="M9.2 14.5c2.7 0 5 4.3 5.4 11.1h.1c.4-1.4.5-2.8.5-4.2 0-4.9-2.3-9.3-5.4-9.3-2.9 0-3.2 2.4-2.5 3.5.3-.4.8-1.1 1.9-1.1zm3 12.2c.1-.6.2-1.1.2-2 0-3.1-1.9-8.1-5.4-8.1-2.7 0-3.1 2.6-2.2 3.4.2-.6.8-1.3 1.9-1.3 2.9 0 5 4.5 5.5 8zm17.7-11.1c.3-.4.4-.9.4-1.2 0-1.2-1.1-2.3-2.7-2.3-4.7 0-6.4 8.2-5.1 13.5h.1c.5-7.2 2.8-11.1 5.4-11.1 1.3-.1 1.8.8 1.9 1.1zm.2 1.1c-4.4 0-6.1 7.5-5.2 10h.1c.5-3.8 2.6-8 5.3-8 1.5 0 1.9.9 1.9 1.3 1-.9.4-3.3-2.1-3.3zM19.4 29.3c-2.7 0-5.4.6-7.7 2.1v.1c1.1-.2 3.8-.5 7.1-.5 3.6 0 4.9.5 5.5.5.4 0 .6-.2.6-.6 0-1.2-2.6-1.6-5.5-1.6zm0-2.8c-2.3 0-5.4.6-7.7 2.1v.1c.9-.1 3.9-.5 7.2-.5 3.1 0 4.9.5 5.4.5.4 0 .6-.2.6-.6 0-1.2-2.6-1.6-5.5-1.6zM29.9 8.6c-.3.1-.8.6-.8 1.3 0 .9.6 2.6 3.1 2.6 2.6 0 4.3-1.9 4.3-4.5s-1.9-4.9-6-4.9c-4.9 0-11.2 4.4-11.2 15 0 2 .2 4.2.7 6.6h.1c0-13 4.9-19.2 10.2-19.2 2.3 0 3.5 1.4 3.5 2.9 0 1.4-.8 2.4-2.1 2.4-1.5 0-2-1.3-1.8-2.2zM7 10.2c-.2.3-.8.8-1.8.8C4 11 3 10 3 8.6 3 7.3 4.1 6 6.1 6c5 0 10.9 7.2 10.9 16.3 0 .6 0 1.6-.1 2.4h.1c.5-1.6 1-4 1-5.7 0-8-5.5-15.7-11.9-15.7C2.3 3.3.4 5.8.4 8.1c0 2.5 1.5 4.4 3.8 4.4C6 12.5 7 11.2 7 10.2zM6.3.6c-.5 0-.8.3-.8.6 0 .5.3.7.9.9 1.8.7 5.2 1.2 9 1.2 6.8 0 13.8-1 16.8-2.2V1c-2.5.2-6.8.5-13.7.5C9.1 1.5 7.2.6 6.3.6zm5.4 3.6c.8.4 4.5 2 6.2 8.2.1.4.3.5.5.5.4 0 .6-.3.6-.8 0-2.9-1.6-5.4-2.9-6.5-1.5-1.2-3.3-1.5-4.4-1.4zm9.2.9c1.2-1.2 2.9-1.7 4.7-1.5v.1c-.6.2-2 .7-3.5 2.2-1.2 1.3-2 2.8-2.4 4.1-.4-2.6.4-4.1 1.2-4.9z"/><path fill=#fff d="M43.8 15.5h-2v-1.4h4.6v1.4l8.1 8.8v-8.8h-2v-1.4h5.6v1.4h-2v12.9h-1.5l-9.2-10v8.7h1.9v1.3h-5.5v-1.3h2V15.5zm14.6 7.8c0 2.2 1.5 5.4 5.7 5.4 4 0 5.7-2.8 5.7-5.4 0-2.7-1.9-5.4-5.7-5.4-4 0-5.7 3-5.7 5.4zm3.1 0c0-2.4.8-4.2 2.7-4.2 1.7 0 2.6 1.6 2.6 4.4 0 2.6-1 4.2-2.6 4.2-1.7-.1-2.7-1.7-2.7-4.4zm11-4h-1.8v-1.1h4.5v1.7c.4-.6.8-1.1 1.2-1.5.4-.3.8-.5 1.3-.5s1.2.4 1.6 1c-.1.9-.5 2.1-1 2.9-.6-1.1-1-1.5-1.7-1.5-.6 0-.9.3-1.5 1v6H77v1.1h-6.3v-1.1h1.8v-8zm7.6.3l4.3-4.1v2.8h2.9v1.3h-2.9v5.8c0 1.3.5 2.2 1.4 2.2.8 0 1.3-.8 1.7-.8.2 0 .3.1.3.3 0 .5-.9 1.6-3 1.6s-3.1-1.1-3.1-3.4v-5.7h-1.6zm13.1-.2c1.3-.9 2.6-1.5 3.6-1.5 1.6 0 2.8 1.3 2.8 3v6.4h1.8v1.1h-6v-1.1h1.5v-5.8c0-1.2-.6-1.8-1.6-1.8-.6 0-1.3.3-2.2.8v6.8h1.5v1.1h-6v-1.1h1.8V13.8h-1.8v-1.1h4.6v6.7zm7.5-.1v-1.1h5.6v1.1h-1.4l2.3 5.1 1.9-5.1h-1.4v-1.1h5.2v1.1h-1.3l2.3 5.1 1.9-5.1h-1.6v-1.1h4.2v1.1H117l-3.5 9.3h-.6l-3.4-7.2-2.7 7.2h-.5l-4.3-9.3h-1.3zm27.4 3.4c-.3-2.9-2.1-4.7-4.7-4.7-2.8 0-4.7 2.1-4.7 5.2 0 2.4 1.3 5.6 5.5 5.6 1.5 0 2.5-.3 3.9-1.1l.2-1.3c-.8.6-2.2 1.1-3.2 1.1-2 0-3.4-1.9-3.4-4.7h6.4v-.1zm-6.4-1.1c0-1.3.6-2.5 1.7-2.5 1 0 1.7.9 1.9 2.5h-3.6zm8.2 3.8h.7c.4 1.4 1.4 2.2 2.5 2.2.8 0 1.4-.4 1.4-1.2 0-1.7-4.7-2.1-4.7-5.3 0-1.8 1.5-3.1 3.5-3.1.7 0 1.3.2 2 .6l.2-.5h.8v2.7h-.8c-.4-1.2-.9-1.7-1.9-1.7-.9 0-1.4.4-1.4 1 0 2.1 4.8 2 4.8 5.5 0 1.9-1.5 3.2-3.6 3.2-.9 0-1.7-.2-2.5-.6l-.1.5h-.8v-3.3h-.1zm7.8-5.8l4.3-4.1v2.8h2.9v1.3H142v5.8c0 1.3.5 2.2 1.4 2.2.8 0 1.3-.8 1.7-.8.2 0 .3.1.3.3 0 .5-.9 1.6-3 1.6s-3.1-1.1-3.1-3.4v-5.7h-1.6zm17.4 3.1c-.3-2.9-2.1-4.7-4.7-4.7-2.8 0-4.7 2.1-4.7 5.2 0 2.4 1.3 5.6 5.5 5.6 1.5 0 2.5-.3 3.8-1.1l.2-1.3c-.8.6-2.2 1.1-3.3 1.1-2 0-3.4-1.9-3.4-4.7h6.6v-.1zm-6.4-1.1c0-1.3.6-2.5 1.7-2.5 1 0 1.7.9 1.9 2.5h-3.6zm9.5-2.3h-1.8v-1.1h4.5v1.7c.4-.6.8-1.1 1.3-1.5.4-.3.8-.5 1.3-.5s1.2.4 1.6 1c-.1.9-.5 2.1-1 2.9-.6-1.1-1-1.5-1.7-1.5-.6 0-.9.3-1.5 1v6h1.8v1.1h-6.3v-1.1h1.8v-8zm9.8 0h-1.8v-1.1h4.5v1.2c1.4-.9 2.3-1.5 3.6-1.5 1.8 0 2.8 1.2 2.8 3.1v6.3h1.8v1.1h-6v-1.1h1.5v-6.1c0-1-.7-1.6-1.5-1.6-.7 0-1.3.2-2.2.8v6.9h1.4v1.1h-6v-1.1h1.8v-8h.1zm19-3.8h-1.9v-1.4h5.2v1.4l4.1 8.4 4-8.4v-1.4h5.4v1.4h-1.9v11.6h1.9v1.3H197v-1.3h2v-9h-.1L194 28.4h-.5l-4.8-9.9v8.6h1.9v1.3h-5.5v-1.3h1.9V15.5zm29.8 11.8v1.1h-4.5v-1c-1.3.9-2.5 1.3-3.5 1.3-2.2 0-2.9-1.4-2.9-2.9v-6.5H204v-1.1h4.5v7.4c0 1 .5 1.6 1.4 1.6.6 0 1.3-.3 2.3-.9v-7.1h-1.8v-1.1h4.5v9.1h1.9v.1zm.2-7.7l4.3-4.1v2.8h2.9v1.3h-2.9v5.8c0 1.3.5 2.2 1.4 2.2.8 0 1.3-.8 1.7-.8.2 0 .3.1.3.3 0 .5-.9 1.6-3 1.6s-3.1-1.1-3.1-3.4v-5.7H217zm20.8 7.7v1.1h-4.5v-1c-1.3.9-2.5 1.3-3.5 1.3-2.2 0-2.9-1.4-2.9-2.9v-6.5h-1.8v-1.1h4.5v7.4c0 1 .5 1.6 1.4 1.6.6 0 1.3-.3 2.3-.9v-7.1h-1.8v-1.1h4.5v9.1h1.8v.1zm10.6-.3c-.3.1-.5.2-.8.2-.6 0-1.2-.5-1.2-1.3v-4.5c0-2-1.5-3.4-3.4-3.4-1.3 0-2.9.7-4.1 1.8l.2.8c1.2-.7 2.1-1 2.8-1 1.1 0 1.8.7 1.8 2v1.2l-2.5.6c-1.9.5-2.7 1.3-2.7 2.6 0 1.6 1 2.8 2.4 2.8.9 0 1.8-.4 3-1.3.3.8 1.1 1.3 2.1 1.3.8 0 1.6-.2 2.4-.7V27zm-4.7-3.1v2.5c-.5.4-1.1.7-1.5.7-.6 0-1.1-.6-1.1-1.2 0-1.1.5-1.6 2.6-2zm7.2-10.1h-1.7v-1.1h4.5v14.6h1.7v1.1h-6.2v-1.1h1.7V13.8zm6.6.6h.4c.1 0 .4 0 .4-.3 0-.1-.1-.3-.3-.3h-.5v.6zm0 1.1h-.4v-1.9h.9c.1 0 .7 0 .7.6 0 .4-.3.5-.4.5l.4.8h-.4l-.3-.8h-.5v.8zm.4.5c.8 0 1.5-.7 1.5-1.5s-.7-1.5-1.5-1.5-1.5.7-1.5 1.5c0 .9.6 1.5 1.5 1.5zm0-3.3c1 0 1.8.8 1.8 1.8s-.8 1.8-1.8 1.8-1.8-.8-1.8-1.8.8-1.8 1.8-1.8z"/></symbol>

<use href="#svg$he"
     x="150" y="0" width="800" height="200"/>
</svg>

???

view from 10 ft from my desk

big-company perks

small-company tech

...

one thing you should know about me:

---
template: bio

## I really really enjoy writing tests

???

When I'm in a standup, and someone mentions they still need to write tests,

I'll often volunteer for it

---
layout: true
template: full-screen
class: square

---
background-image: url(images/you.jpg)
class: no-footer

???

so that's me

but I want to know about you.

* react developers?
* little testing experience?
* familiar with testing but not written react tests?
* writing react tests?


---
template: full-screen
class: testing-pyramid, no-footer

background-image: url(images/testing-pyramid-1.jpg)

???

Testing pyramid.

If you haven't seen it before....

suggests types of tests (unit, integration,...)


the shape suggests a balance - there's a tradeoff



---
template: full-screen
class: testing-pyramid, no-footer

background-image: url(images/testing-pyramid-2.jpg)

???

We're going to focus on this area

Not to say that the others aren't important!

But the tooling & strategies are going to be different
