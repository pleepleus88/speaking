'use strict';

// Necessary Plugins
var gulp = require('gulp');
var paths = require('../paths');

// copy fonts
module.exports = gulp.task('media', function () {
    return gulp.src(paths.source.media)
        .pipe(gulp.dest(paths.build.media));
});
