Congratulations! You're building a sweet new web application with React. But what are you doing for unit testing?

In this session, we'll look at writing unit tests for a React app with Jest and Enzyme. We'll talk about what to test and how to test it, from basics to best practices.


##old
You're building a web application with React. But you're feeling a bit guilty. You know you should be writing unit tests for your sweet new app...you just aren't sure how to get started.

This session will cover unit testing your React application, from basics to tools to best practices.



