class: new-section

# Speak My Language

???

If you want to communicate with me, speak my language.

And this made me sad

---
background-image: url(images/kids/liv_sad3.jpg)
class: bg-cover

???

But I realized, using the right language is also

 one of my rules for writing readable code.

---
class: double-wide, tertiary

# Speak My Language
## Names
### Language Constructs

???

Especially when it comes to naming things

It's important to use appropriate language constructs.

...

--

```javascript
var stats = statisticsForPlayers;
```

???

This code seems all fantastic.

Just assigning one variable to another.

...

--

```javascript
function statisticsForPlayers() {
    me.loading = true;

    var statsRequest =
        buildStatisticsRequest();

    playerService.loadStatistics(
        statisticsRequest,
        onStatisticsLoaded);
}
```

???

Until you discover that statisticsForPlayers is a method, not an object.


Code is easier to read when you

use **nouns for classes**,

and **verbs for functions**.

When you use the wrong language constructs,

you have to think harder about what something is.

---
class: double-wide, tertiary

# Speak My Language
## Names
### Spell Correctly

???

Misspelled names are confusing

When you mis-spell a word, you are in the minority

(except accommodation. no one knows how to spell that. we should get rid of it.)

Everything else...You can be certain that a majority of those reading your code can spell correctly.

...

--

.leftest[
```javascript
function geenrateRandomNumber(){

}
```
]

???

So when I misspell the word generate,

whether it's a typo or I think that's how it's spelled,

It makes things harder for others to read.

...

--

.rightest[
```javascript
function generateRandomNumber(){

}
```
]

???

It also has the unfortunate side-effect

of preventing my IDE from auto-completing when I type "gen".

---
class: double-wide, tertiary

# Speak My Language
## Names
### Be Consistent

???

Inconsistent language also forces the reader to think when they read your code.

...

--

.leftest[
```javascript
//inconsistent names for lists

var playerList;

var teams;

var listOfSeasons;
```
]

???

If I am inconsistent with naming lists of things,

I spend more time trying to identify what something is.

...

--

.rightest[
```javascript
//consistent names for lists

var playerList;

var teamList;

var seasonList;
```
]

???

When you use consistent language,

reading your code requires less thought.

In this case, I can quickly identify if something is a list of things.

---
class: double-wide, tertiary

# Speak My Language
## Names
### Be Symmetrical

???

There's also something to be said for symmetry in naming

...

--

.leftest[
```csharp
//asymmetrical names

public void FileOpen(...)

public void CloseFile(...)
```
]

???

Asymmetry can show itself a couple ways.

1- Ordering of terms in the name

We're naturally drawn to symmetry so this throws us off.

...

---
class: double-wide, tertiary

# Speak My Language
## Names
### Be Symmetrical

.leftest[
```csharp
//asymmetrical names

public void FileOpen(...)

public void CloseFile(...)
```

```csharp
//asymmetrical names

public void UnpackFile(...)

public void CloseFile(...)

```
]

???

There's also something to be said for symmetry in naming

...

Asymmetry can show itself a couple ways.

1- Ordering of terms in the name

...

2- unnatural opposites

There is a specific book I remember reading to my kids

A tab-flappy book, and on the front of each flap was one word,

behind the flap was the opposite.

Up, down. In, out. Open, Close.

These words are natural opposites, and we understand them from a very young age.

...

--

.rightest[
```csharp
//symmetrical names

public void OpenFile(...)

public void CloseFile(...)

```
]

???

Symmetrical names are easier to read

because you understand them without thinking.

---
class: double-wide, tertiary
# Speak My Language
## Names
### Metaphors Are Tricky

???

And the idea of unnatural opposites

brings me to metaphors.

Metaphors are high-risk high-reward.

A good metaphor can be really helpful for readability.

It provides the reader a shortcut to understanding the code.

But you really have to nail the metaphor.

...

--
.leftest[
```csharp
// cuz metronomes "keep time".
public interface IMetronome {
    DateTime Now { get; }
}
```
]

???

If you use one that isn't quite right, it can make things more confusing.

And in this case, you can tell it's not right, because I have to add a cute comment.

I got too cute with this.

...

--
.leftest[
```csharp
public interface IClock {
    DateTime Now { get; }
}
```
]

???

My Clock interface doesn't need a cute comment

because it is the right metaphor.

---

background-image: url(images/kids/lila_happy1.jpg)
class: bg-cover, inverse-text

# Speak My Language

???

I get it, I told her.

And we looked back at the list of things we'd talked about

---
class: summary-list, center

# Be Direct
# Be Clear
# Be Concise
# Stop Repeating Yourself
# Pace Yourself
# Speak My Language

???

To be better at communication

(read em)

And looking at this, and comparing it to all the ways I try to do all these things in code,

I said think I can sum it all up by saying, the key to good communication is...

---
