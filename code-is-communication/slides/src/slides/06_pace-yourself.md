class: new-section

# Pace Yourself

???

Sometimes you talk too fast, and I can't keep up.

---
background-image: url(images/coping-2.jpg)
class: bg-cover, square

???

I had to remind myself of some coping strategies I've learned

(from booklets my daughter has brought home from school)

...

About the pacing -

I know I talk fast when I am trying to make a point

So I think I see where she's coming from

And then I thought about the ways I pace my code,

to make it more readable.

---
class: sub-section

# Pace Yourself
## Use Whitespace

???

The first thing I like to do for pacing
is use whitespace effectively

---
class: double-wide, medium, tertiary

# Pace Yourself
## Whitespace
### Separate Concepts Visually

???

I find that vertical whitespace helps keep concepts separate,
and therefore more readable.

...

--

.leftest[
```javascript
function loadStatisticsForPlayers() {
    me.loading = true;
    var statsRequest =
        buildStatisticsRequest();
    playerService.loadStatistics(
        statisticsRequest,
        onStatisticsLoaded);
}
```
]

???

On the left, I have no vertical whitespace, so my code appears cluttered.

...

--

.rightest[
```javascript
function loadStatisticsForPlayers() {

    me.loading = true;

    var statsRequest =
        buildStatisticsRequest();

    playerService.loadStatistics(
        statisticsRequest,
        onStatisticsLoaded);
}
```
]


???

On the right, I’ve separated concepts with vertical whitespace.

This allows me to quickly identify the different things happening in the method.

---
class: double-stack, medium, tertiary

# Pace Yourself
## Whitespace
### Reduce Concepts Per Line

???

Another way we can use vertical spacing
is to give concepts their own line.

...

--

```javascript
function getSecondBaseman(players) {
    return _.chain(players).where({ position: '2B'}).pluck('lastname').value();
}
```

???

Here again, I’ve got a line of code that chains together several clauses,
and it reads like a __runon sentence__.

...

--

```javascript
function getSecondBaseman(players) {
    return _.chain(players)
        .where({ position: '2B'})
        .pluck('lastname')
        .value();
}
```

???

By giving each clause its own line, I can more easily identify important concepts.

---
class: double-stack, tertiary

# Pace Yourself
## Whitespace
### Reduce Concepts Per Line

???

Aside from readability, another great reason to reduce the concepts per line

is how easy it is to see your changes in Git,

or whatever other source control you're using.

--


![Me](images/codesamples/git-diff-bad.png)

--


![Me](images/codesamples/git-diff-good.png)

---

background-image: url(images/kids/liv_happy3.jpg)
class: bg-cover

# Pace Yourself

???

So at this point,

I got really excited and started telling her all about

whitespace and

 Git and

 beep boop beep boop

And she said UGH, this is what I'm talking about.

...

Sometimes it just seems like you are speaking a different language.

---
