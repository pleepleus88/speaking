class: new-section

# Be Concise

???

And again, I was a little hurt by this

...

---
background-image: url(images/kids/lila_sad2.jpg)
class: bg-cover

???

But then I started to bring it back to code again.

I can think of some ways my code is concise,

especially when it comes to functions

I have this theory that...

---
background-image: url(images/war-and-peace.jpg)
class: bg-cover

???

Functions should read less like war & peace

---
background-image: url(images/cyoa.jpg)
class: bg-cover square

???

And more like a choose your own adventure book

---
class: double-wide, secondary

# Be Concise
## Small Units

???

And we can accomplish this with small units of code.

--

.leftest.small[
```javascript
function loadSomeData(){

    me.loading = true;

    var dataRequest = {};
    dataRequest.ids = _.pluck(me.items, 'id');
    dataRequest.startDate =
        formatDate(me.startDate);
    dataRequest.endDate =
        formatDate(me.endDate);

    dataService.loadData(
        dataRequest,
        function (result) {
            me.fieldA = result.fieldA;
            me.fieldB = result.fieldB;

            notify('Data loaded!');
        }
    );
}
```
]

???

This is my example from before

Where I identified all of the different things happening

And extracted them

...

--

.rightest.small[
```javascript
function loadSomeData(){

    setLoadingState();

    var dataRequest = buildDataRequest();

    dataService.loadData(
        dataRequest,
        handleResponse
    );
}
```

```javascript
function setLoadingState(){
    me.loading = true;
}

function buildDataRequest(){
    //...
}

function handleResponse(data){
    //...
}
```
]

???


I want to read a small excerpt, and

from its contents, identify where I need to read next.

Through this process, I can understand the code one small bit at a time.

---
class: double-wide, secondary

# Be Concise
## Eliminate Indentation

???

Here's a hint that your units of code are too large -

You have a lot of indentation.

--

.small[
```javascript
function searchPlayers(team, position, searchText) {
    var results = [];

    players.forEach(function(player) {
        if (player.team == team) {
            player.positions.forEach(function(playerPosition) {
                if (playerPosition == position) {
                    if (player.firstName.startsWith(searchText)) {
                        results.push({
                            playerId: player.playerId,
                            fullname: `${player.firstName} ${player.lastName}`
                        });
                    } else if (player.lastName.startsWith(searchText)) {
                        results.push({
                            playerId: player.playerId,
                            fullname: `${player.firstName} ${player.lastName}`
                        });
                    }
                }
            });
        }
    });

    return results;
}
```
]

???

...

Indented code is confusing.

---
class: double-wide, secondary

# Be Concise
## Eliminate Indentation

.small[
```javascript
function searchPlayers(team, position, searchText) {
    return players
        .filter(p => p.team == team)
        .filter(p => positionMatches(p, position))
        .filter(p => playerNameMatches(p, searchText))
        .map(mapMatchingPlayers);
}

function positionMatches(player, position) {
    return player.positions
        .filter(pos => pos == position).length > 0;
}

function playerNameMatches(player, searchText) {
    return (
        player.firstName.startsWith(searchText) ||
        player.lastName.startsWith(searchText)
    );
}

function mapMatchingPlayers(player) {
    return {
        playerId: player.playerId,
        fullname: `${player.firstName} ${player.lastName}`
    };
}
```
]

???

When possible, you can avoid it by extracting functions.

---
class: double-wide, small, secondary

# Be Concise
## Avoid Regions

???

Another way to identify if your function could be more concise

is if it has regions

...

--

.leftest[
```csharp
public void GetCurrentGameScenario(
    RequestModel request)
{

#region Get Player Info

    var playerFromDb = GetPlayerById(
        request.PlayerId);

    var playerResult = MapPlayer(playerFromDb);

#endregion

#region Get Game Info

    var gameFromDb = GetGameById(
        request.GameId);

    var gameResult = MapGame(gameFromDb);

#endregion

}

```
]

???

In this example, you can see I’ve got sections of code indicated by regions.

...

--

.rightest[
```csharp
public void GetCurrentGameScenario(
    RequestModel request)
{

    var playerResult = GetPlayerInfo(
        request.PlayerId);

    var gameResult = GetGameInfo(
        request.GameId);

}
```
]

.rightest[
```csharp
public PlayerViewModel GetPlayerInfo(
    int playerId)
{
    //...
}

public GameViewModel GetGameInfo(
    int gameId)
{
    //...
}

```
]

???

If I extract those blocks into separate methods,

my top level method becomes a nice concise outline.

...

This rule for regions also extends to classes.

If you can find commonality between a bunch of functions in a class,

enough to create a region,

that region could probably be its own class.

---
class: double-wide, secondary

# Be Concise
## Pass Few Arguments

???

The more arguments you have,

the more mental mapping the reader has to do,

to keep the sequence straight.

...

--
.leftest[
```javascript
function buildTeam(
    sp, c,
    _1b, _2b, _3b, ss,
    lf, cf, rf) {

        //...

    }
}
```
]

???

There are no hard limits, but fewer is better, 3 is a lot, and that is way too many.

There are a couple of ways out of this.

First, we can look at our method and decide if it is doing too much.

If so, break it up.

...

--

.rightest[
```javascript
function buildTeam(
    battery, infield, outfield) {

        //...

    }
}
```
]

???

Second, it's possible that the arguments you're passing

are conceptually related.

In this case, you can combine arguments into objects.


---
class: double-wide, secondary

# Be Concise
## But Be Complete

```javascript
(w,g,c,m)=>(b=(`G`[r=`repeat`](w)+`
`)[r](g))+(`C`[r](w)+`
`)[r](c)+(`M`[r](w)+`
`)[r](m)+b
```

???

there is a balancing act between being concise and being complete.

you don't want to be so concise that you've written code golf

But just like communicating verbally

Unnecessary words are noise.

...

So after all of this thinking about how to make my code more concise,
I told my wife

---

background-image: url(images/kids/liv_happy2.jpg)
class: bg-cover

# Be Concise

???

Yeah. I recognize the value of being concise.

What did she have for me next?

She said,

---
