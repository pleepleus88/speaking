
class: title

# THANK YOU!

.col.col-4.about.left.centered[
## Steven Hicks
### <i class="el el-twitter"></i>  [@pepopowitz](http://twitter.com/pepopowitz)
### <i class="el el-envelope"></i>  steven.hicks@centare.com
### <i class="el el-globe"></i>  [bit.ly/code-is-communication](http://bit.ly/code-is-communication)
]

---
# Resources

## Further Reading

* [_Clean Code_](https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882)
* [_Refactoring_](https://www.amazon.com/Refactoring-Improving-Design-Existing-Code/dp/0201485672)
* [_Code Complete_](https://www.amazon.com/Code-Complete-Practical-Handbook-Construction/dp/0735619670)
