class: new-section

# Stop repeating yourself

???

When you repeat yourself, I stop listening.

And this made me kind of mad...

---
background-image: url(images/kids/liv_sad1.jpg)
class: bg-cover

???

But again I brought it back in my head to code.

One of my favorite rules to follow is the

---
class: sub-section

# Don't repeat yourself
## DRY Principle

???

Repetition in code is a bug in waiting.

You change things in 4 places, but forget about the 5th.

---
class: double-wide, small, tertiary

# Don't repeat yourself
## Stay DRY
### Extract Method

???

One of the best ways to eliminate duplication is by extracting methods.

...

--

.leftest[
```javascript
function loadPlayerStats(players) {

    var _1b = players['cecil cooper'];
    var _1bStatsRequest = {
        playerId: _1b.id
    };
    statsService
        .loadStats(_1bStatsRequest)
        .then(function (result){
            _1b.stats = result;
        });


    var _2b = players['jim gantner'];
    var _2bStatsRequest = {
        playerId: _2b.id
    };
    statsService
        .loadStats(_2bStatsRequest)
        .then(function (result){
            _2b.stats = result;
        });


    var _3b = players['paul molitor'];
    var _3bStatsRequest = {
        playerId: _3b.id
    };
    statsService
        .loadStats(_3bStatsRequest)
        .then(function (result){
            _3b.stats = result;
        });
}
```
]

???

Here, you can see I’m repeating about 9 lines of code for several different players.

---
class: double-wide, small, tertiary

# Don't repeat yourself
## Stay DRY
### Extract Method

.leftest.dim-1.dim-2.dim-12.dim-13.dim-14.dim-15.dim-16.dim-17.dim-18.dim-19.dim-20.dim-21.dim-22.dim-23.dim-24.dim-25.dim-26.dim-27.dim-28.dim-29.dim-30[
```javascript
function loadPlayerStats(players) {

    var _1b = players['cecil cooper'];
    var _1bStatsRequest = {
        playerId: _1b.id
    };
    statsService
        .loadStats(_1bStatsRequest)
        .then(function (result){
            _1b.stats = result;
        });


    var _2b = players['jim gantner'];
    var _2bStatsRequest = {
        playerId: _2b.id
    };
    statsService
        .loadStats(_2bStatsRequest)
        .then(function (result){
            _2b.stats = result;
        });


    var _3b = players['paul molitor'];
    var _3bStatsRequest = {
        playerId: _3b.id
    };
    statsService
        .loadStats(_3bStatsRequest)
        .then(function (result){
            _3b.stats = result;
        });
}
```
]

???

One thing that's interesting about repeated code is often you can see it in the shape of the code.

---
class: double-wide, small, tertiary

# Don't repeat yourself
## Stay DRY
### Extract Method

.leftest.dim-1.dim-2.dim-3.dim-4.dim-5.dim-6.dim-7.dim-8.dim-9.dim-10.dim-11.dim-12.dim-23.dim-24.dim-25.dim-26.dim-27.dim-28.dim-29.dim-30[
```javascript
function loadPlayerStats(players) {

    var _1b = players['cecil cooper'];
    var _1bStatsRequest = {
        playerId: _1b.id
    };
    statsService
        .loadStats(_1bStatsRequest)
        .then(function (result){
            _1b.stats = result;
        });


    var _2b = players['jim gantner'];
    var _2bStatsRequest = {
        playerId: _2b.id
    };
    statsService
        .loadStats(_2bStatsRequest)
        .then(function (result){
            _2b.stats = result;
        });


    var _3b = players['paul molitor'];
    var _3bStatsRequest = {
        playerId: _3b.id
    };
    statsService
        .loadStats(_3bStatsRequest)
        .then(function (result){
            _3b.stats = result;
        });
}
```
]

???

One thing that's interesting about repeated code is often you can see it in the shape of the code.

---
class: double-wide, small, tertiary

# Don't repeat yourself
## Stay DRY
### Extract Method

.leftest[
```javascript
function loadPlayerStats(players) {

    var _1b = players['cecil cooper'];
    var _1bStatsRequest = {
        playerId: _1b.id
    };
    statsService
        .loadStats(_1bStatsRequest)
        .then(function (result){
            _1b.stats = result;
        });


    var _2b = players['jim gantner'];
    var _2bStatsRequest = {
        playerId: _2b.id
    };
    statsService
        .loadStats(_2bStatsRequest)
        .then(function (result){
            _2b.stats = result;
        });


    var _3b = players['paul molitor'];
    var _3bStatsRequest = {
        playerId: _3b.id
    };
    statsService
        .loadStats(_3bStatsRequest)
        .then(function (result){
            _3b.stats = result;
        });
}
```
]

.rightest[
```javascript
function loadPlayerStats(players) {

    var _1b = players['cecil cooper'];
    loadStatsForPlayer(_1b);

    var _2b = players['jim gantner'];
    loadStatsForPlayer(_2b);

    var _3b = players['paul molitor'];
    loadStatsForPlayer(_3b);
};
```

```javascript
function loadStatsForPlayer(player) {

    var statsRequest = {
        playerId: player.id
    };
    statsService
        .loadStats(statsRequest)
        .then(function (result){
            player.stats = result;
        });

};
```
]

???

On the right, I’ve extracted the duplication into one method, improving readability.

And now I've only got one place that I need to change, instead of many.

---
class: title

## Stay DRY
# But not too DRY

???

Dry is a recommendation, not a hard rule.

When I first learned about DRY, everything seemed like duplication.

Now I find myself asking **"is this duplication"?**

What's the **reason for these things changing?**

If it seems like they would all change together, then yes, it's repetition.

If they might change for different reasons,

they might just be similar code.


This is an important distinction because you don't want to end up with the wrong abstraction.


---

background-image: url(images/kids/lila_happy3.jpg)
class: bg-cover

# Stop repeating yourself

???

So again, I said to my wife,

I get it. I can stop repeating myself.

No matter how many times I tell you about the amazing dream I had last night....

you don't care.

And she said exactly.

And we moved on to the next lesson.

---
