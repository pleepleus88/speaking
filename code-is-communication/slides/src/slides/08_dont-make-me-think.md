class: new-section

# Don't Make Me Think

???

Above all else,
Don't make me think!

---
background-image: url(images/reactions/hi-five.jpg)
class: bg-cover, square

???

And she said "exactly!"

...

And this is the most important thing for code, too.

Code should not make the reader think.

---
name: development-is-hard
background-image: url(images/legos_thumb.gif)
animated-background: images/legos_single.gif
class: bg-cover, inverse-text

# Development Is Hard Enough


???

When you're writing or reading code,

 you're building a mental model in your head.

 **a lego spaceship** in your head

 It takes a lot of thought and effort to build this model up

 , and even more to keep from losing it.

 ...

 The more we have to think about the code we're reading,

 the less effort we are able to put toward maintaining that model.

...

Development is already Hard

Don't Make it harder on yourself, and your teammates,

with code that communicates poorly.

---
