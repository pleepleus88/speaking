class: new-section

# Be Clear
## Say exactly what you mean

???

...

And I realized, I totally get this

I've got kids.

I want to introduce you to one of them.

You've been seeing photos of my daughters Lila and Olivia.

But I want to talk specifically about...

---
background-image: url(images/supercat.jpg)
class: bg-cover

???
Olivia.

This is Supercat. (sidetrack)

Olivia will do anything for a laugh.

But she's not great at making decisions.

and so I have this conversation daily.

---
class: title, conversation

.alt-brand[
> ## Daaad! I'm hungry!
]

--

> ## Okay, Liv. Want some yogurt?

--

.alt-brand[
> ## No! I hate yogurt!
]

--

> ## Okay. How about an apple?

--

.alt-brand[
> ## Dad I said I wanted yogurt!!!!
]

???

Olivia has trouble communicating clearly what she wants

So I understand the importance of clarity.

And I feel pretty strongly about the **importance of being clear in my code**

...

especially when it comes to naming things.

---
class: sub-section

# Be Clear
## Name things clearly

???

Naming is the hardest thing in development.

Here are some rules I like to follow.

---
class: double-wide, tertiary, medium

# Be Clear
## Name Clearly
### Name With Intention
--

.leftest[
```javascript
function calculateOnBasePercentage(
    a, b, c, d, e){

    var top = a + b + c;
    var bottom = b + c + d + e;

    return top / bottom
}
```
]

???

On the left, you see code where the variables don’t really mean anything.

And in fact, in order to know what the variables mean,

I need to know the formula for obp.

And this is the opposite of how it should be.


...

--

.rightest[
```javascript
function calculateOnBasePercentage(
    hits,
    walks,
    hitByPitch,
    atBats,
    sacrificeFlies){

    var top = hits + walks + hitByPitch;
    var bottom = walks + hitByPitch
        + atBats + sacrificeFlies;

    return top / bottom
}
```
]

???

On the right, the variable names mean something,

making it easier to tell what the function is doing.

When you don't name with intention,

you force readers to have to think.

---
class: double-wide, tertiary

# Be Clear
## Name Clearly
### Name Thoroughly

???
If the function does multiple things,
all of those things should be identified in the name.

If there are side effects, the name should mention that.

--

```csharp
public void _____(ShoppingCart cart)
{

    this.ShoppingCartRepository.Add(cart);

    this.ClearCart();

}
```

--

.leftest[
```csharp
public void SaveCart(...)
// Doesn't tell the whole story
```
]

--

.rightest[
```csharp
public void SaveAndClearCart(...)
// Tells the whole story
```
]

???

...

And you'll notice that the function name got a little bit longer,

but...

---
class: double-wide, tertiary

# Be Clear
## Name Clearly
### Use Long Names

???

As developers, we spend a lot of time trying to make things small.

The length of a name should not be one of those things.

...

--

.leftest[
```javascript
//ambiguous name

function process(request) {

}

```
]

???

Short names are generally ambiguous.

...

--

.rightest[
```javascript
//unambiguous name

function saveUserProfile(request) {

}

```
]

???

Longer names are generally more clear.

...

---
class: double-wide, tertiary

# Be Clear
## Name Clearly
### Avoid Encodings

???

Encoded names are also unclear.

As a general rule, if you can’t pronounce the name out loud, it is probably encoded.

...

--

.leftest[
```javascript
//encoded names

//hungarian notation
var dblPrice;

//abbreviations
var flNmWoExt;

//scoping
var m_Title;
```
]

???

Some examples of encoding are

Hungarian notation,

abbreviations,

and scoping prefixes.

...

--


.rightest[
```javascript
//not encoded


var price;


var fileNameWithoutExtension;


var title;
```
]

???

Encoding forces the reader to

__decipher code__ as it’s read.

---
class: double-wide, tertiary

# Be Clear
## Name Clearly
### Use Constants

???

Sometimes we forget to name things altogether.

**magic numbers and strings** are easier to understand

when they are constants with a descriptive name

--

.leftest[
```javascript
return seconds / 86400;
```
]

???

...

who can tell me what this number represents?

--

.rightest[
```javascript
const secondsInADay = 86400;

return seconds / secondsInADay;
```
]

???

...

who can tell me now?

...

There's something else I think about when I think about being clear with my code.

---
class: sub-section, secondary

# Be Clear
## Comments

???

and that's comments.

---
background-image: url(images/comments-lie.jpg)
class: bg-cover, square

???

This tweet summarizes a lot of what I think about comments.


---
class: title

# Comments Lie.

???

Your IDE doesn’t keep comments up to date like it does variable names.

And chances are, you don’t either.

Once you understand the code, you stop reading the comments.

---
class: double-wide, tertiary

# Be Clear
## Comments
### Lie

```javascript
function isHallOfFameWorthy(player){

    //a player is considered HOF worthy if
    // he played for 18 seasons             //<------- 18

    return player.seasonsPlayed > 20;       //<------- 20
}
```
???

In this example, you can see that a comment says one thing, but the code says another.

---
class: double-wide, small, tertiary

# Be Clear
## Comments
### Don't Excuse Confusing Code

???

Comments don’t excuse confusing code,
which is too often how we use them.

...

--

.leftest[
```javascript
function inductPlayer(player) {

    //induct a player if he is awesome.
    // a player is considered awesome if
    // he played for the brewers,
    // won a world series,
    // or was named steve.

    if (player.teams['brewers']
        || player.worldSeriesTitleCount > 0
        || player.firstName == 'Steve') {

        hallOfFame.induct(player);
    }
}
```
]

???

We write code that isn’t clear,
and then we write lengthy comments summarizing the code.

...

--

.rightest[
```javascript
function inductPlayer(player) {

    if (isAwesome(player)) {
        hallOfFame.induct(player);
    }
}
```
]

???

But instead of 5 lines of comments and 3 lines of code,
wouldn’t you rather see one line of code?

---
class: double-wide, tertiary

# Be Clear
## Comments
### Apologize

```javascript
//                  ________________
//                 |                |_____    __
//                 |    I'm Sorry   |     |__|  |_________
//                 |________________|     |::|  |        /
//    /\**/\       |                \.____|::|__|      <
//   ( o_o  )_     |                      \::/  \._______\
//    (u--u   \_)  |
//     (||___   )==\
//   ,dP"/b/=( /P"/b\
//   |8 || 8\=== || 8
//   `b,  ,P  `b,  ,P
//     """`     """`
```

???

Comments are often an apology

For not being able to express ourselves in the code clearly.

If you are apologizing, you'll want to include the "i'm sorry" kitten on a bicycle

so i'll leave that here for you.


---
background-image: url(images/comments-redundant.jpg)
class: bg-cover, square

???

Here's another thing I think about comments

---
class: double-wide, tertiary

# Be Clear
## Comments
### Can Be Redundant

???

I remember being taught in college to comment every line.

--

.leftest[
```csharp
public void SavePlayer()
{
    //open the file
    this.OpenFile();

    var player = {

        //the name of the player
        Name = "Jim Gantner",

        //the player's position
        Position = "2B"

    }

    //save the player to file
    this.SavePlayerToFile(player);
}
```
]
???

These comments just echo the code.

They are just noise.

If I'm writing code that is clear, I don't need them.

---
class: double-wide, tertiary

# Be Clear
## Comments
### Can Be Misused

???

as in unused code,

a scenario which is better served by source control.

--

```javascript
function isAwesome(player) {

    //return player.yearsExperience > 8
    //return player.yearsExperience > 15
    //return player.yearsExperience > 12
    return player.yearsExperience > 10
        && player.hits > 3000
        && player.ops > .850;

}
```

???


---
class: sub-section
# Comments
## Can also be good

???

This isn't to say all comments are bad.

The right comment in the right situation can be helpful

---
class: double-wide, medium, tertiary

# Be Clear
## Comments
### Can Explain Intention

--
.leftest[
```javascript
function mapPlayer(players) {

    //in:
    // [
    //     {
    //         playerId: 1,
    //         name: 'Jim Gantner',
    //         position: '2B'
    //     },
    //     ...
    // ]
    //out:
    // [
    //     'Jim Gantner'
    // ]

    return _.chain(players)
        .where({ position: '2B' })
        .pluck('name')
        .value();
}
```
]
???

In this case, telling me the expected input and output of this function.

These types of comments are especially useful when doing

functional-style filter, map, reduce operations

where you're mapping things from one shape to another

---
class: double-wide, tertiary

# Be Clear
## Comments
### Can Simplify

???

Comments can also simplify

something like a regex

...

--

```javascript
function validateTime(time) {

    //valid formats: HH:MM,
    // where HH is in 24 hour format
    var regex = new RegExp(
        "^([0-1][0-9]|[2][0-3]):([0-5][0-9])$");

    return regex.test(time);

}
```

???

It takes time to decipher a regular expression.

A comment can help you understand the code faster.

With the caveat that if you aren't keeping it up to date,

it's not doing any good.

---
class: double-wide, tertiary

# Be Clear
## Comments
### Can Warn The Reader

???

advise the reader why an unexpected line needs to be there.

--

```javascript
function getMiddleInfielders(players) {

    return _.filter(players, function(player) {
        return player.position == '2B'
            || player.position == 'SS'
            // we have to include third basemen,
            //  because sometimes the shift is on.
            || player.position == '3B'
    });

}
```

???

...

So after all of this thinking about how to make my code more clear,

I start to get excited

---

background-image: url(images/kids/liv_happy1.jpg)
class: bg-cover

# Be Clear

???


and I ramble a whole bunch about
names and comments and regular expressions and she says

whoa whoa whoa, slow down.

Next rule for good communication.

---
