class: new-section

# Be direct.

## Stop leaving room for misinterpretation.

???

And at first I got mad

---
background-image: url(images/kids/liv_sad2.jpg)
class: bg-cover

???

Because I tend to take this kind of feedback personally.

But then I started to think about it.

When I write code, I try to be direct.

---
class: sub-section

# Be Direct
## Single Responsibility Principle (SRP)

???

I try to follow the SRP

I try to follow it with classes

And I try to follow it with functions

---
class: double-wide, secondary
# Be Direct
## SRP

.leftest.small[
```javascript
function loadSomeData(){

    me.loading = true;

    var dataRequest = {};
    dataRequest.ids = _.pluck(me.items, 'id');
    dataRequest.startDate =
        formatDate(me.startDate);
    dataRequest.endDate =
        formatDate(me.endDate);

    dataService.loadData(
        dataRequest,
        function (result) {
            me.fieldA = result.fieldA;
            me.fieldB = result.fieldB;

            notify('Data loaded!');
        }
    );
}
```
]

???

If my function here does several things,

I'll try to identify them

(identify!)

---
class: double-wide, secondary
# Be Direct
## SRP

.leftest.small.dim-1.dim-2.dim-4.dim-5.dim-6.dim-7.dim-8.dim-9.dim-10.dim-11.dim-12.dim-13.dim-14.dim-15.dim-16.dim-17.dim-18.dim-19.dim-20.dim-21[
```javascript
function loadSomeData(){

    me.loading = true;

    var dataRequest = {};
    dataRequest.ids = _.pluck(me.items, 'id');
    dataRequest.startDate =
        formatDate(me.startDate);
    dataRequest.endDate =
        formatDate(me.endDate);

    dataService.loadData(
        dataRequest,
        function (result) {
            me.fieldA = result.fieldA;
            me.fieldB = result.fieldB;

            notify('Data loaded!');
        }
    );
}
```
]

---
class: double-wide, secondary
# Be Direct
## SRP

.leftest.small.dim-1.dim-2.dim-3.dim-4.dim-12.dim-13.dim-14.dim-15.dim-16.dim-17.dim-18.dim-19.dim-20.dim-21[
```javascript
function loadSomeData(){

    me.loading = true;

    var dataRequest = {};
    dataRequest.ids = _.pluck(me.items, 'id');
    dataRequest.startDate =
        formatDate(me.startDate);
    dataRequest.endDate =
        formatDate(me.endDate);

    dataService.loadData(
        dataRequest,
        function (result) {
            me.fieldA = result.fieldA;
            me.fieldB = result.fieldB;

            notify('Data loaded!');
        }
    );
}
```
]

---
class: double-wide, secondary
# Be Direct
## SRP

.leftest.small.dim-1.dim-2.dim-3.dim-4.dim-5.dim-6.dim-7.dim-8.dim-9.dim-10.dim-11.dim-21[
```javascript
function loadSomeData(){

    me.loading = true;

    var dataRequest = {};
    dataRequest.ids = _.pluck(me.items, 'id');
    dataRequest.startDate =
        formatDate(me.startDate);
    dataRequest.endDate =
        formatDate(me.endDate);

    dataService.loadData(
        dataRequest,
        function (result) {
            me.fieldA = result.fieldA;
            me.fieldB = result.fieldB;

            notify('Data loaded!');
        }
    );
}
```
]

---
class: double-wide, secondary
# Be Direct
## SRP

.leftest.small[
```javascript
function loadSomeData(){

    me.loading = true;

    var dataRequest = {};
    dataRequest.ids = _.pluck(me.items, 'id');
    dataRequest.startDate =
        formatDate(me.startDate);
    dataRequest.endDate =
        formatDate(me.endDate);

    dataService.loadData(
        dataRequest,
        function (result) {
            me.fieldA = result.fieldA;
            me.fieldB = result.fieldB;

            notify('Data loaded!');
        }
    );
}
```
]

.rightest.small[
```javascript
function loadSomeData(){

    setLoadingState();

    var dataRequest = buildDataRequest();

    dataService.loadData(
        dataRequest,
        handleResponse
    );
}
```
]

.rightest.small[
```javascript
function setLoadingState(){
    me.loading = true;
}

function buildDataRequest(){
    //...
}

function handleResponse(data){
    //...
}
```
]

???

 and extract them to more focused methods,

So I can read through them one abstraction at a time.

---
class: sub-section

# Be Direct
## Cohesion

???

I try to write cohesive code

---
class: double-wide, tertiary

# Be Direct
## Cohesion
### Group Related Elements

???

I try to group related elements.

...

--

```csharp
public Player GetPlayer()

public void AddPlayer(Player player)

public void UpdatePlayer(Player player)



public Team GetTeam()

public void AddTeam(Team team)

public void UpdateTeam(Team team)

```

???

This helps me identify patterns and repetition easily

And find areas of code that might be suitable for extracting methods or classes.

---
class: double-wide, medium, tertiary

# Be Direct
## Cohesion
### Organize By Feature

???

I prefer to organize my code by feature, rather than type.

...

--

.leftest[
```json
app/
    App.js
    actions/
        calendar.js
        speakers.js
        user-profile.js
    containers/
        CalendarContainer.js
        SpeakerListContainer.js
        UserProfileContainer.js
    components/
        Calendar.js
        Session.js
        Speaker.js
        SpeakerList.js
        UserProfile.js
    reducers/
        root.js
        calendar.js
        speakers.js
        user-profile.js
```
]

???

On the left, is a pretty standard react app.

I have my code organized by object type.

The problem with this approach

is that when my system changes, it's not all the containers that change.

---
class: double-wide, medium, tertiary

# Be Direct
## Cohesion
### Organize By Feature

.leftest.dim-1.dim-2.dim-3.dim-4.dim-6.dim-7.dim-8.dim-10.dim-11.dim-12.dim-13.dim-16.dim-17.dim-18.dim-19.dim-21[
```json
app/
    App.js
    actions/
        calendar.js
        speakers.js
        user-profile.js
    containers/
        CalendarContainer.js
        SpeakerListContainer.js
        UserProfileContainer.js
    components/
        Calendar.js
        Session.js
        Speaker.js
        SpeakerList.js
        UserProfile.js
    reducers/
        root.js
        calendar.js
        speakers.js
        user-profile.js
```
]

???

It's "something to do with speakers".

I need to bounce around all these folders to find all of the speaker files.


---
class: double-wide, medium, tertiary

# Be Direct
## Cohesion
### Organize By Feature

.leftest[
```json
app/
    App.js
    actions/
        calendar.js
        speakers.js
        user-profile.js
    containers/
        CalendarContainer.js
        SpeakerListContainer.js
        UserProfileContainer.js
    components/
        Calendar.js
        Session.js
        Speaker.js
        SpeakerList.js
        UserProfile.js
    reducers/
        root.js
        calendar.js
        speakers.js
        user-profile.js
```
]

.rightest[
```json
app/
    App.js
    calendar/
        calendar.actions.js
        calendar.reducer.js
        Calendar.container.js
        Calendar.component.js
        Session.component.js
    speakers/
        speakers.actions.js
        speakers.reducer.js
        SpeakerList.container.js
        SpeakerList.component.js
        Speaker.component.js
    attendees/
        user-profile.actions.js
        user-profile.reducer.js
        UserProfile.container.js
        UserProfile.component.js
```
]
???

When I organize by feature,

All of my speaker things are in one folder.

This structure, to me, is more cohesive.

---
class: bg-cover
background-image:url(images/horizontal-slices.jpg)

???

we have a history of putting things in buckets that are based on horizontal slices

---
class: bg-cover
background-image: url(images/vertical-slices.jpg)

???

but it's the vertical slices - the features - that put "code that changes together" in the same place.

this is applicable on both server side and client


there are variations of this slide that have been used to argue for technologies like css-in-js

but it is appropriate everywhere.

...

It's kind of like packing your clothes.

Underwear, pants, shirts, all separated, it's kind of a mess when you want to get dressed the next day.

I've never tried it...but what if we all just started packing by outfits instead?

That way we just have to grab a whole outfit and go. No digging.

(if we changed our clothes, anyway)

---
class: sub-section

# Be Direct
## Be Explicit

???

Another way to be direct with code

is to be explicit, rather than implicit.

---
class: double-stack, tertiary

# Be Direct
## Be Explicit
### Use Parentheses

???

When I have a mathematical expression,

I am explicit with parentheses,

so that the reader doesn't have to remember the order of operations.

...

--

```csharp
var overallIndex = pageSize * pageNumber + indexOnPage;
```

???

If I look at this statement for a bit, I can

deduce the order of operations.

...

--

```csharp
var overallIndex = (pageSize * pageNumber) + indexOnPage;
```

???

Or, I can throw parentheses in so that

I don't have to spend any time at all identifying the OOO.

...

So I look back at my wife and I say

---

background-image: url(images/kids/lila_happy2.jpg)
class: bg-cover

# Be Direct

???

You're right. I can be more direct.

What else you got for me?

And she says

---
