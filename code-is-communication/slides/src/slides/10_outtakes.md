---
class: double-wide, secondary

# Don't Make Me Think
## Avoid Hidden Meanings

???

Developers are always trying to save keystrokes.

Sometimes it seems like using an already existing variable

for a second purpose

is going to save you effort somehow.

Like there's some universal limit on variables. (like guids)

...

--

.leftest[
```csharp
if (Request.SelectedTeamId == -1)
{
    return SearchMinorLeaguers(
        Request.LastName);
}
else
{
    return SearchMajorLeaguers(
        Request.LastName,
        Request.SelectedTeamId);
}
```
]

???

This is another case of developers optimizing for write time,

instead of read time.

You're not saving anything by using one variable,

just making things more confusing.

--

.rightest[
```csharp
if (Request.SearchMinorLeaguers)
{
    return SearchMinorLeaguers(
        Request.LastName);
}
else
{
    return SearchMajorLeaguers(
        Request.LastName,
        Request.SelectedTeamId);
}
```

]


???





---
class: double-wide, medium, tertiary

# Don't repeat yourself
## Stay DRY
### Switch Statements

???

Sometimes duplication shows itself in different ways,
like repeated switch statements.

...

--

.leftest[

```csharp
void DoThings(char code){
    switch (code){
        case 'A':
            DoFirstThingA();
            break;
        case 'B':
            DoFirstThingB();
            break;
        default:
            DoFirstThingC();
    }

    //repeated switch statement!
    switch (code){
        case 'A':
            DoSecondThingA();
            break;
        case 'B':
            DoSecondThingB();
            break;
        default;
            DoSecondThingC();
        }
    }

    //and so on...
}

```
]

--

.rightest[
```csharp
void DoThings(char code){
    var strategy = ChooseStrategy(code);

    strategy.DoFirstThing();
    strategy.DoSecondThing();
    strategy.DoThirdThing();
}
```

```csharp
IStrategy ChooseStrategy(char code){
    //one decision point
    switch (code){
        case 'A':
            return new AStrategy();
        case 'B':
            return new BStrategy();
        default:
            return new DefaultStrategy();
    }
}
```
]

???

In these cases, the strategy pattern can be used to defeat the repetition.

The strategy pattern helps you turn

multiple repeated decision points

into one decision, with multiple strategies.




---
class: double-wide, tertiary

# Be Direct
## Be Explicit
### Specify Optional Args

???


When there is potential confusion about params,

I name them instead of relying on order.

...

--

```csharp
public void GreetUser(
    int userId,
    string firstName = null,
    string lastName = null,
    string nickname = null)
{

}
```

--

.leftest[
```csharp
GreetUser(123,
    null,
    null,
    "Steverino");
```
]

???

Sure, I can just pass them all in the right order,

But that leaves me unclear as to what I'm actually passing.

...

--

.rightest[
```csharp
GreetUser(123,
    nickname: "Steverino");
```
]

???

If I pass my parameters named, instead,
I have no trouble identifying immediately which param I'm passing.
