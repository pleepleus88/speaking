class: title

# Code Is Communication

.col.col-4.about.left.centered[
## Steven Hicks
### <i class="el el-twitter"></i>  [@pepopowitz](http://twitter.com/pepopowitz)
### <i class="el el-envelope"></i>  steven.hicks@centare.com
### <i class="el el-globe"></i>  [bit.ly/code-is-communication](http://bit.ly/code-is-communication)
]

???

**Contact Info**
**Thanks:**

* conference
* organizers
* you!

---
class: bg-cover
background-image: url(images/dev-up-sponsors.png)

???

* sponsors

---
class: middle

.col.col-5.profile[
![Me](images/steve-by-lila-square.jpg)
]


.col.col-5[
# Steven Hicks
]

---
class: middle

.col.col-5.profile[
![Me](images/steve-by-lila-square.jpg)
]

.col.col-5[
# Steven Hicks

.centare-logo[
![Centare](images/centare.png)
]
]

???

* Agile coaching
* Consulting
* Product development consulting

---
class: middle

.col.col-5.profile[
![Me](images/steve-by-lila-square.jpg)
]

.col.col-5[
# Steven Hicks

.centare-logo[
![Centare](images/centare.png)
]
## Spreadsheets -> Web Apps

]

---

class: middle

.col.col-5.profile[
![Me](images/steve-by-lila-square.jpg)
]

.col.col-5[
# Steven Hicks

.centare-logo[
![Centare](images/centare.png)
]

## Spreadsheets -> Web Apps
## Web developer for ~20 years

]

???

I've seen a lot of code.

---

```javascript
var o=n=>n+(!/1.$/.test(--n)&&'snr'[n%=10]+'tdd'[n]||'th')
```

???

I want to start with some code.

I'll giveyou all about 1:00.

Raise your hand when you think you understand what this code does.

---
class: small

```javascript
function getOrdinalForNumber(number) {
    var suffix = getSuffix(number);

    return `${number}${suffix}`;
}

function getSuffix(number) {
    if (numberEndsInElevenTwelveOrThirteen(number)) {
        return "th";
    }

    var lastDigit = number % 10;

    switch (lastDigit) {
        case 1:
            return "st";
        case 2:
            return "nd";
        case 3:
            return "rd";
        default:
            return "th";
    }
}

function numberEndsInElevenTwelveOrThirteen(number) {
    var asString = number.toString();
    return (
        asString.endsWith("11") ||
        asString.endsWith("12") ||
        asString.endsWith("13")
    );
}
```

???

Do it again.

---
```javascript
getOrdinalForNumber(3);
```

```
3rd
```

.footnote[
https://codegolf.stackexchange.com/questions/4707/outputting-ordinal-numbers-1st-2nd-3rd
]
???

Both of them do the same thing.

...

But we aren't here to play code golf.

We're here to talk about ....

---
class: title

# Communication

???

Which I will say in front of you all, is pretty funny,

because as someone who is extremely uncomfortable in social situations,

I would say that my general advice about communication with others is

---
class: title, small

# Avoid it and go for a bike ride instead.

???

I am a pretty active guy,

I usually prefer to go for a run or bike ride by myself than to talk to people.

...

but I know a person.

And so here's another introduction slide.

---
class: middle

.col.col-5.profile[
![Me](images/stephanie-hicks-1.jpg)
]

.col.col-5[
# Stephanie Hicks
]

---
class: middle

.col.col-5.profile[
![Me](images/stephanie-hicks-1.jpg)
]

.col.col-5[
# Stephanie Hicks

## Thinks I'm neat

]

---
class: middle

.col.col-5.profile[
![Me](images/stephanie-hicks-1.jpg)
]

.col.col-5[
# Stephanie Hicks

## Thinks I'm neat

## Teaches English & creative writing

]

---

class: middle

.col.col-5.profile[
![Me](images/stephanie-hicks-1.jpg)
]

.col.col-5[
# Stephanie Hicks

## Thinks I'm neat

## Teaches English & creative writing

## Helps me feel comfortable speaking to others

]

???

I lean on her a lot in social situations

because she's great at communicating.

So I had the idea for this talk

And I said Steph,

I have this idea, but I kind of stink at communication.

---
class: title

# How can I be better at communication?

???

And she said

for starters...

---
