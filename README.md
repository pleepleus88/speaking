# Speaking

This is a collection of artifacts for talks I'm working on.

## To install a google font into css -

Package:

[https://www.npmjs.com/package/webfont-dl](https://www.npmjs.com/package/webfont-dl)

Command:

`webfont-dl "http://fonts.googleapis.com/css?family=...." -o font.css`

> Note: it will replace the contents of font.css, not append. So put it in a new file & copy to append to the existing one.



## Checklist

1. Slides
1. Presenter mode
1. clicker
1. camera
1. venue mic
1. my mic
1. clap hands!
1. record screen
