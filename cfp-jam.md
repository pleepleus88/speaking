Introductions
    Tony Gemoll
    Steven Hicks
        NM in december
        2017: 6 talks at 5 conferences
            3 code camps
        MKE DOT NET 
        Motivation

Questions you might have

  Why should I speak?
    Giving back to dev community
    Free conference ticket (and sometimes travel)
    Career development
    Branding (self)
    Branding (NM)
        (Tony)
  How do I get over the impostor syndrome/self-doubt that prevents me from speaking?
    "I'm not an expert"
        Who cares? 
        Curse of knowledge
        Just know more than the people one step behind you
    "I'm not a good speaker"   
        How could you expect to be good at something you practice so infrequently?
        Practice

    "Speaking is just scary"
        Yes! And fun.
        The audience wants you to succeed
            They came to learn from you
        We can help you lessen the fear by helping you prepare


  How do I come up with ideas to talk about?
    Things you know really well
        You don't need to be an expert in "JavaScript", but maybe you have a lot of experience in writing tests for Angular apps
        That's more than others have
    Things you recently learned
        Many others haven't learned that thing yet
    Things you want to learn but haven't yet
        Talk-driven learning
        Pick a topic you're interested in, but don't know much about 
        Force yourself to learn it over the next few months
    Your experiences with a technology/technologies
        i.e. we had this problem and this is how we solved it
    Personal development
        i.e. overcoming fear, imposter syndrome, ...
  How do I get to speak at a conference?
    CFP process
        Conference asks for submissions
        Name, bio, Job Title
        Talk title, abstract (elevator pitch)
        Convince them why you're the right person
  How do I write a good abstract?
    Know who you're writing it for
        * Short-term: the conference organizers.
        * Long-term: the attendees
    Be clear & express the intention of the session
    Use proper grammar & punctuation
        You need to convince organizers that you can communicate to the attendees
    Get to the point
        Organizers don't have time to read your novella
    Tell attendees what they'll get out of the session
        Describe their problem & offer a solution
  I didn't get accepted. Now what?
    Acceptance rate is low. Keep trying!
  Oh shoot, I got accepted! Now how do I write my talk?
    Focus on content first, then slides
    Why > How > What
        You're looking to inspire them, not to recite something they could find on the internet.
    Tell a story
        Find the best narrative for your talk. You aren't just presenting facts, you're presenting a story.
    Focus on the audience, not the tech being demonstrated
        What will be easy for them? What will be difficult?
  Slide design
      Fewer words
      Fewer bullet points
      Large text
      High visibility/contrast
  Do I need a live demo?
    No!
  How do I prepare for my talk?
    Practice a ton
        alone
        family
        stuffed animals
    Give it at work!
        Your team
        FED Summit
    Give it at a meetup!
    Get feedback from people
  What do I need to know before I start talking?
    Things will go wrong. Don't depend on internet, ...
    Do a tech-check
    Everyone gets nervous.
      Remember: they want you to succeed.
  How do I handle audience questions?
    You choose! During? After? Never? In a small group afterward?
        You can do any of these. Whichever you pick, stick to your guns. 
    Don't be afraid to say "I don't know".
        This doesn't discredit you.
  How do I handle annoying attendees?
    There aren't very many of them.
    If they do happen, ask them to talk after.

Open CFPs
How Do I Find conferences?
What's next?
  CFP Jam/Abstract Party?


  

Why?
  Why would a person want to speak at a conference?
    Feels good to give back to the dev community
    Free conference ticket
      & often travel
      So you can learn stuff!
    Career development
    Branding (self)
    Branding (NM)
      NM Support
        talk about different levels of conference 

  Why wouldn't a person? 
    I'm not an expert
      Who cares? 
      Curse of knowledge
      Just know more than the people one step behind you
    I'm not a good speaker
      Practice
    Speaking is scary
      Yes! And fun.
      The audience wants you to succeed
        They came to learn from you
      We can help you lessen the fear by helping you prepare




How?
  How do I come up with ideas of things to talk about? 

  Stages of a talk
    Idea generation - 
      * "I'm not an expert"
        Don't need to be 
      * Things you know really well
        You don't need to be an expert in "JavaScript", but maybe you have a lot of experience in writing tests for Angular apps
          That's more than others have
      * Things you recently learned
        Many others haven't learned that thing yet
      * Your experiences
        i.e. we had this problem and this is how we solved it
      * Personal development
        i.e. overcoming fear, imposter syndrome, ...
      * Talk-driven learning
        Pick a topic you're interested in, but don't know much about 
        Force yourself to learn it over the next few months
    Abstract
      * What's an abstract?
      * Who are they for?
        * Long-term: the attendees
        * Short-term: the conference organizers.
        * You need to express to the conf organizers that you can communicate clearly to attendees
      * Good ones
        * Have names that are clear, & express the intention of the session
          They can be funny, but they don't have to be. Clarity is more important.
        * Have proper grammar & spelling
          You need to convince organizers that you can communicate to the attendees
        * Get to the point quickly 
          and stay on task 
            Organizers don't have time to read your novella
        * Tell the attendees what they'll get out of the session
          * Describe their problem & offer a solution
        * Knowing the audience
      * Don't wait until the last minute
        * Give yourself time to edit
        * Send it to friends/coworkers for feedback!
          * Find someone else who wants to also speak. Review each other's!
    Submit
      * You can submit more than one talk
        * But please no more than 4
      * Standard questions
        * Name, Job Title
        * Bio & headshot
          This will get presented to attendees.
        * Talk Title
        * Talk Abstract
          Various lengths
      * Sometimes
        * Do you need travel assistance?
        * Speaking experience
          * Links to videos/slides
          * Have you presented it before
            * If so, did people like it
        * Notes about the session
          * Brief outline/description of how the talk flows
    Wait
      * It will definitely take a few weeks before you hear back
        * And could take a couple months
      * This is a good time to start writing the talk!
        Because you can also submit it to more CFP's
      * How to find CFP's
        * Papercall.io
        * Twitter
        * Lists
      * Some open ones:
        List them
    Write
      * Live demos
        You don't need em.
        Use animated gifs or step through code via slides
      * Focus on the content before slides
      * Focus on "why", and maybe "how" - not just what. 

    Speak
      Respect your audience 
        CoC, profanity, ...





Why?
  Why would a person want to speak at a conference?
    Feels good to give back to the dev community
    Free conference ticket
      & often travel
      So you can learn stuff!
    Branding (NM)
    Branding (self)
  Why wouldn't a person? 
    I'm not an expert
      Who cares? 
      Curse of knowledge
      Just know more than the people one step behind you
    I'm not a good speaker
      Practice
    Speaking is scary
      Yes! And fun.
      The audience wants you to succeed
      We can help you lessen the fear by helping you prepare
How?
  Stages of a talk
    Idea generation - 
      * "I'm not an expert"
        Don't need to be 
      * Things you know really well
        You don't need to be an expert in "JavaScript", but maybe you have a lot of experience in writing tests for Angular apps
          That's more than others have
      * Things you recently learned
        Many others haven't learned that thing yet
      * Your experiences
        i.e. we had this problem and this is how we solved it
      * Personal development
        i.e. overcoming fear, imposter syndrome, ...
      * Talk-driven learning
        Pick a topic you're interested in, but don't know much about 
        Force yourself to learn it over the next few months
    Abstract
      * What's an abstract?
      * Who are they for?
        * Long-term: the attendees
        * Short-term: the conference organizers.
        * You need to express to the conf organizers that you can communicate clearly to attendees
      * Good ones
        * Have names that are clear, & express the intention of the session
          They can be funny, but they don't have to be. Clarity is more important.
        * Have proper grammar & spelling
          You need to convince organizers that you can communicate to the attendees
        * Get to the point quickly 
          and stay on task 
            Organizers don't have time to read your novella
        * Tell the attendees what they'll get out of the session
          * Describe their problem & offer a solution
      * Bad ones
        * opposite of those things ^^^
      * Don't wait until the last minute
        * Give yourself time to edit
        * Send it to friends/coworkers for feedback!
          * Find someone else who wants to also speak. Review each other's!
    Submit
      * You can submit more than one talk
        * But please no more than 4
      * Standard questions
        * Name, Job Title
        * Bio & headshot
          This will get presented to attendees.
        * Talk Title
        * Talk Abstract
          Various lengths
      * Sometimes
        * Do you need travel assistance?
        * Speaking experience
          * Links to videos/slides
          * Have you presented it before
            * If so, did people like it
        * Notes about the session
          * Brief outline/description of how the talk flows
      * It will definitely take a few weeks before you hear back
        * And could take a couple months
      * How to find CFP's
        * Papercall.io
        * Twitter
        * Lists
    Write the talk
      * Content first
        * Outline?
        * Don't just tell them "what". Explain "why" they should be excited about it, and maybe "how" to get started. 
          You're looking to inspire them, not to recite something they could find on the internet.
        * Story arc
          Find the best narrative for your talk. You aren't just presenting facts, you're presenting a story.
        * Focus on the audience, not the tech you're demonstrating
          How will this help them? 
          What will be easy for them? What will be difficult?
        * Script/no script
          Figure out where you are most comfortable. 
          If you do script, make sure you don't read it like a robot.
      * Slides
        * Only after you've written the content!
        * Fewer words!
        * Spread bullet points across multiple slides
          But be pragmatic. Like html tables - usually it's the wrong construct, but sometimes it really is a table.
        * Large text
        * High visibility/contrast
          90% of projectors are garbage
    Prepare
      * Get to know your content
        * You'll feel more confident if you know all the things you're going to talk about.
      * Practice!
        * Alone
          This is like unit tests. You'll find a lot of things, but it's not "real life".
        * With an audience
          This is like integration tests. You'll learn which parts you don't know very well.
          * Your family
          * Your team
          * FED Summit
          * Meetups!!!!!
          * Request feedback from them!
        * Don't memorize unless you have the time to get it down 100%
          You can use your slides to guide you. Images/words that cue you, or Notes.
      * Don't expect anything to work
        * Conference WIFI usually sucks
        * Projectors sometimes fail
        * Demos don't go as planned
        * Sound is a crapshoot
          Just have a plan for when these things happen.
    Deliver
      * Set up early to make sure everything works
        * Sometimes you can do a tech-check the day before
      * Yes, you'll be nervous
        * Find a way to calm yourself before.
        * Remember:
          * They want you to succeed
          * You were selected to speak at a conference.
          * You know your content
      * Slow pace
        * Drink water
        * Give attendees time to process what you just said
      * Speak clearly
        * Don't mumble
        * Look at your audience, not the screen
        * Use a microphone if it's there
      * Questions
        * Choose how you'll handle them
          * During your talk? At the end? In a small group afterward?
            You can do any of these. Whichever you pick, stick to your guns. 
        * Pesky attendees/know-it-alls
          * Ask them to talk 1on1 afterward
          * Remind them that you have XX minutes and want to get through it
        * What if I'm wrong?
          * Don't be afraid to admit it.
          * You also just learned something! 
        * What if you don't know the answer?
          * Don't dig a hole.
          * "I don't know, but if you hang around for a bit we can research it together!"
          * Ask if anyone else in the room knows the answer. 

          