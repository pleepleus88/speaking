Native mobile apps are on the decline. ???


Progressive web apps are an intriguing mix of the mobile web and native apps. They are the face of the internet soon. In this session, we'll see what it takes to build your first progressive web app. We will discuss the principles behind progressive web apps and progressive enhancement. We'll take a look at the tools available to help you get started, and discuss the challenges with building a progressive web app today. 






The past, present, and future of Progressive Web apps

Native mobile apps are on the decline. By some estimates, 49% of smartphone users in the US download zero apps in a typical month. 

With progressive web apps, the boundary between native and web is blurring. New features of the HTML spec offer blah blah blah to replace the functionality of native apps?


We'll look at the past, present, and future of PWAs. Where did they come from? What can you do with them currently? And what is coming down the pipeline to (put a bow on it)?