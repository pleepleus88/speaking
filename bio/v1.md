Steven Hicks is a full-stack web developer with nearly 20 years experience. He believes in clean, readable, and maintainable code. Steve likes to use the right tool for the job, especially if the right tool is JavaScript. He strongly believes that if you ain't falling, you ain't learning.

Steve embraces continuous improvement and believes that a developer's job is to solve problems, not just write code.

When he isn't crushing 1s and 0s or playing with his kids, you can find Steve at a triathlon, on his mountain bike, or in a climbing gym.