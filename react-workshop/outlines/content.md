React fundamentals
The 'Component' mindset
JSX, a strange-looking hybrid between JavaScript and HTML
Verifying component inputs
Styling React components
Routing
State management
Automated testing
Best practices


intro & setup
  about me
    skip it?
      just show art from kids?
    javascript engineer
  nm?
    skip it?
  About This Workshop
    Collaborative
      You + me
      You + your neighbor
        pair up on the exercises!
          raise your hand if you're interested in pairing
        teaching is one of the best ways to learn
          and pairing is a great way to teach AND learn
    Schedule
    Feedback
      Green = good
      Yellow = okay
      Red = bad
      Comments = :100:
    Meet your neighbors!
      name
  About You
    New to JS?
    New to JS UI frameworks?
    New to React?
    Developers?
    UX/Designers/????
  
React Fundamentals
  What Is React?
    // from getting started with react
    A JavaScript library for building user interfaces
      from their site
      dumb - of course that's what it is
      but there's meaning here - it is ONLY a UI framework
    The V in MVC (maybe VC)
      And nothing more
      Angular: monolith :: React : microlibraries
      Unix principle
        "Choosing React is like building your own computer, vs buying one off the shelf"
    React Is Declarative
      Imperative code is abstracted
      "Declarative views make your code more predictable and easier to debug."
    React Is Component-Based
      The 'Component' Mindset
        example
      Components implement a render() method
        example
          React.createElement (single element)
          nested elements
          show what the examples translate to
        from docs: 
          The render() function should be pure, meaning that it does not modify component state, it returns the same result each time it’s invoked, and it does not directly interact with the browser. If you need to interact with the browser, perform your work in componentDidMount() or the other lifecycle methods instead. Keeping render() pure makes components easier to think about.

        also from docs:
          https://reactjs.org/docs/components-and-props.html
          Conceptually, components are like JavaScript functions. They accept arbitrary inputs (called “props”) and return React elements describing what should appear on the screen.

exercise: Components
  break listy into components (on paper)
  take-aways
    Components are about ISOLATION, more than they are about reuse
      So find the isolated pieces/seams & make them components
    Decompose your components


      Components receive input data (props, state) describing the current state
      Components can be thought of as pure functions
      
      Components are re-rendered as their inputs change (props, state)
        one-way data flow
        Reconciliation
          Updating the DOM is slow
          Virtual DOM


      Components can be written with JSX
        JSX is optional
        Separation of concerns
        Browsers don't understand it
          
      exercise: JSX fundamentals
        https://reactjs.org/docs/introducing-jsx.html


  React basics
    extends React.Component
      render()
    stateless functional
      reasons to use stateless functional
      compare to React.Component
        this.props.X vs {X}
        no life-cycle events
      favor stateless functional
      exercise: convert a react.component to a stateless functional
    {}
    className
    props
      can't be modified!
        destructuring in ES6 (to pass props)
    conditional rendering
      https://reactjs.org/docs/conditional-rendering.html
    rendering a list {list.map(item => {...})}
      key property
      https://reactjs.org/docs/lists-and-keys.html
    fragments, arrays
    handling events
      https://reactjs.org/docs/handling-events.html
  take-aways
    Note: Always start component names with a capital letter.

render
  https://reactjs.org/docs/react-component.html#render
  When called, it should examine this.props and this.state and return one of the following types:
  React elements. Typically created via JSX. For example, <div /> and <MyComponent /> are React elements that instruct React to render a DOM node, or another user-defined component, respectively.
  Arrays and fragments. Let you return multiple elements from render. See the documentation on fragments for more details.
  Portals. Let you render children into a different DOM subtree. See the documentation on portals for more details.
  String and numbers. These are rendered as text nodes in the DOM.
  Booleans or null. Render nothing. (Mostly exists to support return test && <Child /> pattern, where test is boolean.)

  exercise: implement render several times, returning each of those things ^^^. maybe as a type-along?

  after: introduce stateless functional components as a way to save keystrokes from react classes.

  talk about props here

  common errors:
    Uncaught Error: Component(...): Nothing was returned from render. This usually means a return statement is missing. Or, to render nothing, return null.
      forgot to return something from render
      return <...> on multiple lines needs to be wrapped with ()!
    


css
  traditional
    className
    import './styles.css';
  mid-level: css-modules
    use a separate project/app for exercise (because CRA doesn't have it w/o ejecting)
  next: css-in-js
    styled components, etc
  conclusion
    but you don't HAVE to do css-in-js
    the important thing is that you have component-based css
      via imported css, imported scss, css modules, css-in-js....
      The main takeaway is that styles are not a separate concern from the rest of the component.
        show "the slide"
        talk about suitcases


Routing
  talk about organizing suitcases here?
  exercise:
    add links to our column headers & row headers, to take us to a "friend detail" or "list detail" page




state
  state vs props
    state
      https://reactjs.org/docs/state-and-lifecycle.html
    props
      defaultProps
      props.children
  lifecycle methods
    constructor
      initialize state
      bind event handlers to an instance
    componentDidMount
      make API requests from here!
      subscribe to things here (and unsubscribe in componentWillUnmount)
      
    componentDidUpdate
      check to see if the props have changed, to avoid performance problems!
    componentWillUnmount

    maybe skip:
      shouldComponentUpdate
        PureComponent - basically does this check for you.
          PureComponent performs a *shallow* comparison of props and state, and reduces the chance that you’ll skip a necessary update.
      getDerivedStateFromProps
      getSnapshotBeforeUpdate
      componentDidCatch
        for defining Error Boundaries

  setState
    immutability
    setState({})
    setState(prevState => {})
    return only the state you want to update
  Context
  exercise:
    load actual data into grid, from api
    load list/friend from api endpoint in detail pages!

    maybe two parts - 1, load something from an api endpoint. 2, hook up events.

  next: redux, mobx
  conclusion
    use setState for local state
    Context for "branch" state
    redux/mobx for app state


handling events????
  attaching event handlers in a stateless functional
  attaching event handlers in a Component
  passing event handlers to children
  binding to this
    .bind(this) in constructor
    fat arrow functions
    class fields
  is this better before state?

  exercise
    handleListMemberChanged, removeFriendFromList, addFriendToList
      set them up with unit tests ahead of time?????
      this might be a whole nother exercise????
    bonus: show some sort of indicator that the grid is saving, and then it is done saving
    bonus: friend detail view/edit
      see list detail view/edit

testing

thinking in react
  https://reactjs.org/docs/thinking-in-react.html
  components
  building an app
    sketch out components
    build out components via stateless functional components
    add state
  this makes sense to talk about after doing the component, jsx, and state exercises!
    "The order of these exercises was intentional! this is a recommended approach to building a react app."

type checking
  proptypes

  next: flow, typescript

forms
  https://reactjs.org/docs/forms.html
  controlled components vs uncontrolled

debugging
  react dev tools
  hot module reloading

props as children???
  find a way to work this in.
  <SecondaryPage>

error boundaries?



potential exercises
  return loading message when friends haven't loaded yet
  bonus exercises: build out friend detail pages?
  
TODO - figure out when to introduce create-react-app, and how to cover it. Maybe a super early getting started slide, & just show what it looks like????
  if it gets a "next", that's ejecting & learning about webpack
