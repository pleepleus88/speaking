8:00-11:45, 1:00-5:00
7:45 total
3h45m morning, 4h afternoon
225m morning, 240m afternoon

Intro & setup
20min
  including getting started

Intro to React
20min

Exercise: Components
20min

Intro to JSX
20min

-- 1h20m (80m)

Break
10min

Exercise: JSX
20min

Rendering a React Component
20min

Exercise: Rendering React Components
30min

-- 1h20m (80m)

Break
15min

Composing Components
15min
  props

Exercise: Composing React Components
35min
  do some children-props stuff here?

-- 1h5m (65m)

LUNCH

Styling Components
15min

Exercise: Styling Components
30min

Intro: Component State
5min

Managing Simple Component State
10min

Exercise: Managing Simple Component State
20min

-- 1h20m (80m)

Break
10min

Managing Deeper Component State
10min

Exercise: Managing Deeper Component State (Context)
30min

Managing Application State
10min

Managing Application Routes
15min

-- 1h15m (75m), 2h35m (155) so far

Break
15min

Exercise: React Router
15min

Testing React
15min

Exercise: Testing React
30min

Best Practices & Wrap-up
10m

-- 1h25m (85m), 4h (240) so far




Does this fit?????

Validating COmponent Inputs
15min

Exercise: PropTypes
15min


