class: title

# Getting Started With React

.col.col-7.about.left.centered[
## Steven Hicks
### <i class="el el-twitter"></i>  [@pepopowitz](http://twitter.com/pepopowitz)
### <i class="el el-globe-alt"></i>  [stevenhicks.me](https://stevenhicks.me)
### <i class="el el-envelope"></i>  steven.hicks@centare.com
### <i class="el el-slideshare"></i>  [bit.ly/what-why-how-react](http://bit.ly/what-why-how-react)
]

???

**Thanks:**

* Chicago Code Camp
* Sponsors?
* You all for coming

**Contact Info**
* My name is Steve
* @pepopowitz
* slides online

---
class: middle

.col.col-6.profile[
![Me](images/steven-hicks.jpg)
]


.col.col-4[
# Steven Hicks
]

???

That's me according to my 6 year old daughter Olivia
---
class: middle

.col.col-6.profile[
![Me](images/steven-hicks.jpg)
]

.col.col-4[
# Steven Hicks

## Has wings instead of arms
]

???

...but more pertinent...

---

class: middle

.col.col-6.profile[
![Me](images/steven-hicks.jpg)
]

.col.col-4[
# Steven Hicks

## Has wings instead of arms

.centare-logo[
![Centare](images/centare.png)
]

]

???

I'm a full-stack developer at Centare

Based in Milwaukee

---

class: middle

.col.col-6.profile[
![Me](images/steven-hicks.jpg)
]

.col.col-4[
# Steven Hicks

## Has wings instead of arms

.centare-logo[
![Centare](images/centare.png)
]

## Web developer for nearly 20 years

]

???

And in that time, I have built the front-end of web apps in many styles.

---
class: content

## Ways that I have built front-ends

# Plain JavaScript

???

This was hard.

Lots of cross-browser issues

---
class: content

## Ways that I have built front-ends

# JQuery

???

Changed my life

But hard to maintain

---
class: content

## Ways that I have built front-ends

# Knockout

???

Introduced me to MVVM

& Templates

But I quickly moved to

---
class: content

## Ways that I have built front-ends

# Angular 1

???

Powerful

But Complicated

---
class: content

## Ways that I have built front-ends

# React

???

I'm currently doing almost all my development with React

and nothing has me as excited as building with React

I'm excited for all of you to come out here today to

hear my **sales pitch** on

why you should be excited about React too.