---
class: content
## Create-react-app

# Create-react-app is a launching pad

???

Create-react-app exists to make it easy for you to **learn React**

It **abstracts complexity** so you can **focus on the framework**

Instead of learning build tools

When you're ready, you can eject, and learn those build tools.


So I encourage you to go ahead and install create-react-app

And **start building something**

---
class: title

# THANK YOU!

.col.col-7.about.left.centered[
## Steven Hicks
### <i class="el el-twitter"></i>  [@pepopowitz](http://twitter.com/pepopowitz)
### <i class="el el-globe-alt"></i>  [stevenhicks.me](https://stevenhicks.me)
### <i class="el el-envelope"></i>  steven.hicks@centare.com
### <i class="el el-slideshare"></i>  [bit.ly/what-why-how-react](http://bit.ly/what-why-how-react)
]

---
class: content, top
## Resources

.col.col-10.left.hero[
* [Create-react-app](https://github.com/facebookincubator/create-react-app)
* [Cory House's Pluralsight Course](https://www.pluralsight.com/courses/react-redux-react-router-es6)
* [Enabling hot-reloading in Create-react-app](https://medium.com/@sheepsteak/adding-hot-module-reloading-to-create-react-app-e053fadf569d#.d7gwj6bud)
]