
---
class: title

# Why would I use React?

---
class: content

## Why React?

# It is fast!

???

And you can say that for most modern javascript frameworks

What makes React fast?

---
class: content
## React is fast!

# Updating the DOM is slow

???

It is often the bottleneck in your UI

React updates the DOM efficiently

---
class: content
## React is fast!

# Virtual DOM

???

by using a Virtual DOM

A lightweight representation of the actual browser's DOM.

When a React component receives new props,

React re-renders it to the virtual DOM

---
class: content
## React is fast!

# Reconciliation (Virtual DOM diffing)

???

And then React uses a process called Virtual DOM diffing

To **identify the differences** between the DOM and the virtual DOM

And it is able to update only the DOM elements **that have changed.**

This process makes React fast!

---
class: content
## Why React?

# Compile-time checking & props validation

.full-width[
![Props Failure](images/props.png)
]

???

When I think about how many hours of my life I've spent thanks to a property in my view

being named differently than a property in my controller...

It makes me pretty happy.

---
class: content
## Why React?

# It's easy to understand

---
class: content
## It's easy to understand

> Components work best as pure functions

.empty[]

> State is managed with unidirectional flow

???

It's easy to look at a component and understand what it is doing.

There are many little things about React

that are like **little nuggets of functional programming**

---
class: content
## Why React?

# It's easy to test

???

Like previous point -

I can write a **test** that passes specific props into my component

and **reliably** see what happens.

There are also some really great **testing tools** that

make testing in React not only easy, but enjoyable.

---
class: content
## Why React?

# It's easy to collaborate

???

because you're **breaking up** your UI into **tiny components**

**devs and ux designers**/"front-end" developers

can easily **work around each other**

it is similar to how "design by contract" allows you to define how pieces will work together

then individuals can run off and build the pieces.

---
class: content
## Why React?

# It's mostly just JavaScript

---
class: content, double-stack
## It's mostly just JavaScript

```html
<ul>
    <li *ngFor="#thing of things">
        <span class="badge">{{thing.name}}</span>
    </li>
</ul>
```

???

Here's some Angular code

First off - React is hardly the first framework to ask you to put HTML & JS together

But with Angular, there's more than just HTML and JS

There's Angular syntax too

--


```html
<ul>
    {
        things.map(thing => (
            <li key={thing.id}>{thing.name}</li>
        ))
    }
</ul>
```
???

...

When I write this with React,

I'm using actual JS.

things.map is just a JS function on an array.

...

As a result of this....

---
class: content
## Why React?

# It has a lower learning curve

???

If you're writing components in Angular 1 and you're going to Angular 2,

that may not be true.

---
class: title

# Why wouldn't I use React?

???

Two sides to every story

---
class: content
## Why not React?

# You aren't building a SPA

???

If you're not building a full Single Page App

React isn't going to be the best option.

If you want to do "buckets of SPAs", where pages are rendered by server and hook in SP functionality

Or "sprinkles of JavaScript", where it's even less JavaScript functionality

There are better options for you.

---
class: content
## Why not React?

# ~~React changes quickly~~

???

**Funny story** - a week ago, I modified this slide to have a strikethrough

In the past year, React has **stabilized** quite a bit

Then **Friday**, they announced they were moving PropTypes to a **standalone package**

But for the most part React is **no more volatile** at this point than anything else.

---
class: content
## Why not React?

# JavaScript Fatigue

???

(poll)

JS Fatigue is a real thing

But we are feeling it because there is so much **innovation**

However - React requires you to **make many decisions** about your app

---
class: content
## JavaScript Fatigue

# Angular : Monolith :: React : Microlibraries

???

When you build with React

You are saying "yes" to the microlibrary approach to development.

If you have trouble getting libraries approved for your use, this could be a challenge.

I have seen it done in a larger, more traditional organization

---
class: content
## JavaScript Fatigue

> Choosing between Angular and React is like choosing between buying an off-the-shelf computer and building your own with off-the-shelf parts.

.footnote[
    [Cory House](https://medium.freecodecamp.com/angular-2-versus-react-there-will-be-blood-66595faafd51)
]

???

look at screen & pause