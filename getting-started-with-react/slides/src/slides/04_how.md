
---
class: title

# How do I learn React?

???

The short answer is...it depends

Different strokes for different folks

---
class: content, top
## How do I learn React?

# Choose your own adventure

---
class: content, top
## How do I learn React?

# Choose your own adventure

.col.col-4.left.centered[
## Tutorials
]

???

Cory House's pluralsight course

---
class: content, top
## How do I learn React?

# Choose your own adventure

.col.col-4.left.centered[
## Tutorials
## Read the source
]

???

Heard it on a podcast

These people are robots

---
class: content, top
## How do I learn React?

# Choose your own adventure

.col.col-4.left.centered[
## Tutorials
## Read the source
## **Start building stuff**
]

???

And for my money, the easiest way to get from zero to "building stuff"

is with a boilerplate or starter kit.

...

When I learned, I had to search for a starter kit.

But I have great news if you want to learn from a starter kit -

---
class: content
## Getting Started

# Create-react-app

.footnote[
[Create-react-app](https://github.com/facebookincubator/create-react-app)
]
???

As of about 9 months ago, there is an official one, put out by Facebook

Create-react-app is a **CLI** that gets you from

**0 to "functioning React app" in one command**.

---
class: content
## Create-react-app

# 0. Install Node

.footnote[
[NodeJS.org](https://nodejs.org/en/)
]
???

There is one requirement for using Create-react-app.

You must install Node.

---
class: content
## Create-react-app

# 1. Install!

.huge[
`npm install -g create-react-app`
]

---
class: content
## Create-react-app

# 2. Create!

.huge[
`create-react-app app-name`
]

---
class: content
## Create-react-app

# 3. Run!

.huge[
`npm start`
]

---
class: content
## Create-react-app

# 4. Test!

.huge[
`npm test`
]

---
class: content
## Create-react-app

# 5. Build!

.huge[
`npm run build`
]

???

This is going to build an optimized version of your app, ready for deployment.

---
class: content
## Create-react-app

# Demo!

???

1. Browse the app
2. Run the app
3. Make changes (show site updating)
    a. Change style
    b. Change component
    c. Add "footer"

---
class: content, top
## Create-react-app

# What's Included?

.col.col-7.centered[
* React
]

---
class: content, top
## Create-react-app

# What's Included?

.col.col-7.centered[
* React
* ES2015+ (via Babel)
]

---
class: content, top
## Create-react-app

# What's Included?

.col.col-7.centered[
* React
* ES2015+ (via Babel)
* Linting (via ESLint)
]

---
class: content, top
## Create-react-app

# What's Included?

.col.col-7.centered[
* React
* ES2015+ (via Babel)
* Linting (via ESLint)
* Unit testing (via Jest)
]

---
class: content, top
## Create-react-app

# What's Included?

.col.col-7.centered[
* React
* ES2015+ (via Babel)
* Linting (via ESLint)
* Unit testing (via Jest)
* Bundling (via WebPack)
]

---
class: content, top
## Create-react-app

# What's Included?

.col.col-7.centered[
* React
* ES2015+ (via Babel)
* Linting (via ESLint)
* Unit testing (via Jest)
* Bundling (via WebPack)
* Live reloading (full page refresh)
]

---
class: content, top
## Create-react-app

# What's Included?

.col.col-7.centered[
* React
* ES2015+ (via Babel)
* Linting (via ESLint)
* Unit testing (via Jest)
* Bundling (via WebPack)
* Live reloading (full page refresh)
* A build task (optimized for deployment)
]

---
class: content, top
## Create-react-app

# What can be added (easily)?

.col.col-7.centered[
* React-Router
* Flux/Redux/Mobx
]

---
class: content, top
## Create-react-app

# What can be added (easily)?

.col.col-7.centered[
* React-Router
* Flux/Redux/Mobx
* Lodash
* Bootstrap
]

---
class: content, top
## Create-react-app

# What can be added (easily)?

.col.col-7.centered[
* React-Router
* Flux/Redux/Mobx
* Lodash
* Bootstrap
* Enzyme
* Code coverage
]

---
class: content, top
## Create-react-app

# What can be added (easily)?

.col.col-7.centered[
* React-Router
* Flux/Redux/Mobx
* Lodash
* Bootstrap
* Enzyme
* Code coverage
* Hot reloading (no page refresh)
]

---
class: content, top
## Create-react-app

# What can be added (easily)?

.col.col-7.centered[
* React-Router
* Flux/Redux/Mobx
* Lodash
* Bootstrap
* Enzyme
* Code coverage
* Hot reloading (no page refresh)
* LESS/SCSS/CSS Preprocessors **new!**
]

---
class: content, top
## Create-react-app

# What's not included?

.col.col-7.centered[
* Server rendering
]

---
class: content, top
## Create-react-app

# What's not included?

.col.col-7.centered[
* Server rendering
* CSS modules
]

---
class: content, top
## Create-react-app

# What's not included?

.col.col-7.centered[
* Server rendering
* CSS modules
* Customizations to the build pipeline
]

---
class: content
## Create-react-app

# What if I want to use something that's not included?

.huge[
`npm run eject`
]

???

By doing so, you are taking things into your own hands.

You can't undo an 'eject'

The black box is unboxed.

The big difference - you see all the details about how the build pipeline.

