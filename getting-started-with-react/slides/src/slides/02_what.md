
---
class: title

# What is React?

???

Before we talk about what React is,

(poll)

is anyone here using it?

---
class: content

## What is React?

# A library for building user interfaces

.footnote[
https://facebook.github.io/react/
]

???

So this sounds completely stupid

Of course it is a library for building user interfaces

That's why we're using it

But there is **significance** in the **simplicity** of this statement

React is nothing more than a **UI framework**.

---
class: content

## What is React?

# The V in MVC (maybe VC)
## And nothing more

???

When you think about most modern JS frameworks,

the large majority implement an **MVC** type of pattern

Some are **MVVM**, some can't be labelled (**MV-star**)

But for the most part, you get **all three parts** - the M, the V, and the C (or whatever).

React is **unapologetic** about not being all three - **the V only**

Typically **bundled** with another framework for the M/state management

---
class: content
## What is React?

# React is Declarative

???

What does that mean?

Well, it means we write code that says **WHAT** to display

**not HOW** to display it

...

Obviously some code, at some point,

has to be written on HOW to display

---
class: content
## React is Declarative

# Imperative code is abstracted
## So we can call it declaratively

???

But we abstract the imperative code

So our app can call it declaratively.

**Example**: how to find my house.

**Imperative**: directions

**Declarative**: address

---
class: content
## React is Declarative

> Declarative views make your code **more predictable** and **easier to debug**.

.footnote[
https://facebook.github.io/react/
]

???

Why do we care?

---
class: content
## What is React?

# React is Component-Based

???

Components represent small pieces

And you assemble many small components into an app

---
class: content
## What does this look like?

.hero.centered.col.col-7[
![images/todomvc-1a.jpg](images/todomvc-1a.jpg)
]

???

It helps to look at an example of this.

---
class: content
## What does this look like?

.hero.centered.col.col-7[
![images/todomvc-2a.jpg](images/todomvc-2a.jpg)
]
---
class: content
## What does this look like?

.hero.centered.col.col-7[
![images/todomvc-3a.jpg](images/todomvc-3a.jpg)
]
---
class: content
## What does this look like?

.hero.centered.col.col-7[
![images/todomvc-4a.jpg](images/todomvc-4a.jpg)
]
---
class: content
## What does this look like?

.hero.centered.col.col-7[
![images/todomvc-5a.jpg](images/todomvc-5a.jpg)
]
---
class: content
## What does this look like?

.hero.centered.col.col-7[
![images/todomvc-6a.jpg](images/todomvc-6a.jpg)
]
---
class: content
## What does this look like?

.hero.centered.col.col-7[
![images/todomvc-7a.jpg](images/todomvc-7a.jpg)
]
---
class: content
## What does this look like?

.hero.centered.col.col-7[
![images/todomvc-8a.jpg](images/todomvc-8a.jpg)
]

???

The **deconstruction** of UI's into small components is crucial to successful React development.

---
class: content
## React is Component-Based

# Components implement a **render()** method

???

And this **render** method just **describes** what gets displayed

---
class: content
## Components implement a **render()** method

.dim-1.dim-10[
```javascript
class HelloMessage extends React.Component {
*    render() {
        return React.createElement(
            "div",
            null,
            "Hello ",
            this.props.name
            );
*    }
}
```
]

???

(Describe code)

React provides a React.createElement function

to allow us to compose our components.

We can build an entire app this way.

But there is a **better way**...

---
class: content

## React is Component-Based

# Components can be written with **JSX**

???

About here is where people who have heard a little bit about React

get turned off.

Poll: has anyone heard about JSX?

---
class: content

## JSX

# Is optional

???

**Not required**

But most people use it

And there are some **signficant advantages** to using it.

So what is it?

---
class: content

## JSX

> JSX is a faster, safer, easier JavaScript

.footnote[
http://jsx.github.io/
]

???

Not very helpful

Main takeaway: JSX is a **superset** of JS

---
class: content, double-wide

## JSX

# What does it look like?

.leftest[
```javascript
return React.createElement(
    "div",
    null,
    "Hello ",
    this.props.name
);
```
]

--

.rightest[
```javascript
return (
    <div>
        Hello {this.props.name}
    </div>
);
```
]

???

Anyone want to describe what they see?

It looks like you got HTML in your JS

**Detour**

---
class: content

## JSX

# Separation of Concerns

???

What is SOC?

---
class: content

## Separation of Concerns

> A design principle for separating a computer program into distinct sections, such that each section addresses a separate concern

.footnote[
    http://wikipedia.org/wiki/Separation_of_concerns
]
???

This is the definition of SOC

Defines the term using the term

So I'll rewrite it

---
class: content

## Separation of Concerns

> Different pieces of your app should mind their own business.

.footnote[
    Me
]

???

My definition

...

And many people who build web apps often define it as this...

---
class: content

## Separation of Concerns

> Keep your JS separate from your HTML

.footnote[
    People who build web apps
]

???

---
class: content

## Separation of Concerns

```javascript
class HelloMessage extends React.Component {
    render() {
        return (
            <div>
                Hello {this.props.name}
            </div>
        );
    }
}
```

???
So looking back at our example

We definitely have some HTML in our JS

But the **JS and HTML** are very much the **same concern**

They're displaying a hello message

At first glance, JSX looks like it violates SOC but it absolutely does not.

If anything, it **embraces** it.

Because now we can put everything about this component in one place.

---
class: content

## Separation of Concerns

> Keeping your HTML out of your JS is a separation of **technologies**.

???

My argument to you is that ...

...

It may go without saying, but JSX,

---
class: content
## JSX

# Browsers don't understand it

???

So it is **transpiled**

from JSX to plain-old JS

Transpilation seems like a **hassle**

But it comes with several **benefits**

---
class: content
## Benefits Of Transpiling JSX

# Universal support of newer versions of JavaScript/ECMAScript

???

i.e. Class syntax from ES2015

We don't care which browsers can understand it

because we're transpiling down to plain old JavaScript

---
class: content
## Benefits Of Transpiling JSX

# Ability to enforce linting

???

Which improves **code quality**

If you care strongly about **semicolons or no semicolons**

or **tabs vs spaces**

you can enforce that everyone on your team is following the rules

---
class: content
## Benefits Of Transpiling JSX

# Compile-time validation of our source

???

Which is really great for catching **spelling errors**

and places where you **double-declared** variables

or **didn't declare**

---
class: content

## JSX

# Is optional
## But awesome

???

JSX can be **weird** at first

But it **significantly simplifies** your code.

I'm going to use JSX in this talk,

and I think you should try it too.


---
class: content
## What is React?

# React is Component-Based

???

**Back on track**

We were talking about React components

---
class: content
## React is Component-Based

# Components receive input data (**props**) describing the current state

???

And your component decides what is shown based on the input data

If we look back at our previous example,

---

class: content
## Components receive input data (**props**)

.dim-1.dim-2.dim-3.dim-4.dim-6.dim-7.dim-8.dim-9[
```javascript
class HelloMessage extends React.Component {
    render() {
        return (
            <div>
                Hello {this.props.name}
            </div>
        );
    }
}
```
]

???

We see that there is a name passed in through the props.

Now, the nice thing about props is that

there is **support** for **prop validation** through

...

---
class: content
## React is Component-Based

# Props can be validated through **PropTypes**

???

And propTypes allow you to **define a contract** for your component

---
class: content
## Props can be validated through **PropTypes**

.dim-1.dim-2.dim-3.dim-4.dim-5.dim-6.dim-7.dim-8.dim-9[
```javascript
class HelloMessage extends React.Component {
    render() {
        return (
            <div>
                Hello {this.props.name}
            </div>
        );
    }
}

HelloMessage.propTypes = {
    name: PropTypes.string.isRequired
};
```
]

???

Here's an example.

...

These PropTypes are **validated** for you **by the framework**,

and you will get an **explicit error** when you are in **violation** of the contract.

---
class: content

## React is Component-Based

# Components can manage their own state, instead of taking it via props

???

It is up to you to decide which state belongs to the **component**, and which to the **app**

---
class: content
## Components can manage their own state

.medium.dim-1.dim-6.dim-7.dim-8.dim-9.dim-10.dim-11.dim-12[
```javascript
class Counter extends React.Component {
    // 1. initialize state
    constructor(props) {
        this.state = {count: 0};
    }

    // 2. render state

    // 3. update state
}
```
]

---
class: content
## Components can manage their own state

.medium.dim-1.dim-2.dim-3.dim-13.dim-14.dim-15.dim-16.dim-17[
```javascript
class Counter extends React.Component {
    // 1. initialize state

    // 2. render state
    render() {
        return (
            <div>
                <div>Total: {this.state.count}</div>
                <div><button onClick={this.increment}>Increment</button></div>
            </div>
        );
    }

    // 3. update state
}
```
]

---
class: content
## Components can manage their own state

.medium.dim-1.dim-2.dim-3.dim-4.dim-5.dim-14[
```javascript
class Counter extends React.Component {
    // 1. initialize state

    // 2. render state

    // 3. update state
    increment() {
        this.setState((prevState) => {
            return {
                count: prevState.count + 1
            }
        });
    }
}
```
]

---
class: content

## React is Component-Based

# Components work best as pure functions

???

In other words, given the **same inputs**, you'd get the **same outputs**.

They make your code **easier to follow**.

When a component is managing state, it's not pure

This doesn't mean you shouldn't manage state

It means you should **isolate the state management**

So that you can write **more pure functional components**

---
class: title

# What isn't React?

---
class: content
## What Isn't React?

# Application-level state management

???

Typically handled by another framework

---
class: content
## State Management

# Flux, Redux, Mobx, etc

???

And I don't want to get too much into this conversation

Because it is a talk of its own

But there is one important thing about the state management options for React

---
class: content
## State Management

# Unidirectional flow

???

I don't have a definition

But you can guess how ridiculous it would be

---
class: content
## Unidirectional flow

.unidirectional-flow[

![Unidirectional flow](images/unidirectionalflow.jpg)

]

???

Blue = React. Green = state mgmt

This is decidedly different from an approach like Angular,

where there is two-way binding.

...

Why do we care?

---
class: content
## Unidirectional flow

# Leads to a more predictable system

???

The **simplicity** and **predictability** of one-way data flow makes it much easier to figure out what's going on.
